### building from source ###

For running all the tests, an empty soda4LCA instance running at

http://localhost:8090/Node/

is required.

Run `mvn verify -Plocal` to test against a local instance at http://localhost:8090/Node/.

Run `mvn verify` or `mvn verify -Pdocker` to test against a dockerized instance that will spin up just for testing (NOTE: requires `docker-compose` command).

