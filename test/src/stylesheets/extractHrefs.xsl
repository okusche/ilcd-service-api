<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0">

    <xsl:output method="text"/>

    <xsl:template match="/">
        <xsl:apply-templates select="*"/>
    </xsl:template>


    <xsl:template match="/*/*/@xlink:href">
        <xsl:value-of select="."/>
        <xsl:text>,</xsl:text>
    </xsl:template>


    <xsl:template match="*">
        <xsl:apply-templates select="*|@*"/>
    </xsl:template>


    <xsl:template match="@*"/>


    <xsl:template match="text()"/>


</xsl:stylesheet>
