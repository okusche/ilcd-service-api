<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:process="http://www.ilcd-network.org/ILCD/ServiceAPI/Process"
    xmlns:flow="http://www.ilcd-network.org/ILCD/ServiceAPI/Flow" xmlns:flowProperty="http://www.ilcd-network.org/ILCD/ServiceAPI/FlowProperty"
    xmlns:unitGroup="http://www.ilcd-network.org/ILCD/ServiceAPI/UnitGroup" xmlns:lciamethod="http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod"
    xmlns:source="http://www.ilcd-network.org/ILCD/ServiceAPI/Source" xmlns:contact="http://www.ilcd-network.org/ILCD/ServiceAPI/Contact"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://docbook.org/ns/docbook" xmlns:xi="http://www.w3.org/2001/XInclude">

    <xsl:output indent="yes"/>

    <xsl:template match="/">
<!--        <xsl:processing-instruction name="xml-model">href="http://www.oasis-open.org/docbook/xml/5.0/rng/docbook.rng"
            schematypens="http://relaxng.org/ns/structure/1.0"</xsl:processing-instruction>
        <article xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" version="5.0">
            <info>
                <title>soda4LCA release <xsl:processing-instruction name="eval">${project.version}</xsl:processing-instruction> Service API - <xsl:call-template
                        name="getDatasetType"/> Response Elements</title>
                <subtitle>
                    <inlinemediaobject>
                        <imageobject>
                            <imagedata fileref="images/soda4LCA_logo.png" width="200px" align="center"/>
                        </imageobject>
                    </inlinemediaobject>
                </subtitle>
            </info>
--><!--            <xsl:element name="xi:include">
                <xsl:attribute name="href">namespace_URIs.xml</xsl:attribute>
            </xsl:element>

        -->
            <xsl:element name="sect2">
                <xsl:attribute name="version">5.0</xsl:attribute>
                <xsl:attribute name="xml:id">Response_Elements_<xsl:call-template name="getDatasetType"/></xsl:attribute>
                <title><xsl:call-template name="getDatasetType"/> Response Elements</title>
                <para>
                    <table pgwide="1">
                        <title/>
                        <tgroup cols="2">
                            <colspec colnum="1" colname="col1" colwidth="1*"/>
                            <colspec colnum="2" colname="col2" colwidth="3.5*"/>
                            <thead>
                                <row>
                                    <entry>Name</entry>
                                    <entry>Description</entry>
                                </row>
                            </thead>
                            <tbody>
                                <xsl:apply-templates select="*|@*"/>
                            </tbody>
                        </tgroup>
                    </table>
                </para>
            </xsl:element>
<!--        </article>-->
    </xsl:template>

    <xsl:template match="*[not(name(preceding-sibling::*[1])=name())]|@*[not(local-name()='lang') and not(local-name()='schemaLocation')]">
        <row>
            <entry>
                <para>
                    <emphasis>
                        <xsl:if test="count(. | ../@*) = count(../@*)">
                            <xsl:text>@</xsl:text>
                        </xsl:if>
                        <xsl:value-of select="local-name()"/>
                    </emphasis>
                </para>
                <xsl:if test="not(namespace-uri(.)='http://www.ilcd-network.org/ILCD/ServiceAPI') and not(namespace-uri(.)='')">
                    <para>(<xsl:call-template name="getNSPrefix"/>)</para>
                </xsl:if>
            </entry>
            <entry>
                <para> &lt;description here&gt;</para>
                <para>Type: String<xsl:if test="@xml:lang"><xsl:text> Multilang</xsl:text></xsl:if></para>

                <xsl:if test="name(following-sibling::*[1])=name()">
                    <para>may occur multiple times</para>
                </xsl:if>

                <xsl:element name="para">
                    <xsl:text>Ancestors: </xsl:text>
                    <xsl:choose>
                        <xsl:when test="count(ancestor::*)=0">
                            <xsl:text>None</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>

                            <xsl:for-each select="ancestor::*">
                                <xsl:value-of select="local-name()"/>
                                <xsl:if test="not(position()=last())">
                                    <xsl:text>.</xsl:text>
                                </xsl:if>
                            </xsl:for-each>

                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>

            </entry>
        </row>

        <xsl:apply-templates select="*|@*"/>

    </xsl:template>



    <xsl:template name="getNSPrefix">
        <xsl:choose>
            <xsl:when test="contains(namespace-uri(),'Process')">process</xsl:when>
            <xsl:when test="contains(namespace-uri(),'Flow')">flow</xsl:when>
            <xsl:when test="contains(namespace-uri(),'FlowProperty')">flowproperty</xsl:when>
            <xsl:when test="contains(namespace-uri(),'UnitGroup')">unitgroup</xsl:when>
            <xsl:when test="contains(namespace-uri(),'Source')">source</xsl:when>
            <xsl:when test="contains(namespace-uri(),'Contact')">contact</xsl:when>
            <xsl:when test="contains(namespace-uri(),'LCIAMethod')">lciamethod</xsl:when>
        </xsl:choose>
    </xsl:template>


    <xsl:template name="getDatasetType">
        <xsl:choose>
            <xsl:when test="local-name(/*)='dataSetList'">
                <xsl:text>DatasetList</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring-after(namespace-uri(/*),'http://www.ilcd-network.org/ILCD/ServiceAPI/')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="text()"/>
    <xsl:template match="@xml:lang|@xsi:schemaLocation"/>

</xsl:stylesheet>
