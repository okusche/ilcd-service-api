<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    
    <xsl:template match="/">
        <xsl:apply-templates select="xs:schema"/>
    </xsl:template>
    
    <xsl:template match="xs:schema">
        <xsl:apply-templates select="xs:element[not(//xs:element[./@ref=@name])]"/>
    </xsl:template>
    
    <xsl:template match="xs:element">
        -<xsl:value-of select="@name"/>-
        
<!--        <xsl:apply-templates select="xs:element"/>-->
    </xsl:template>
    
    
</xsl:stylesheet>