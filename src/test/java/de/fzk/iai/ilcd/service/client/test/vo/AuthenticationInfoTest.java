/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.AuthenticationInfo;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockVO;
import de.fzk.iai.ilcd.service.model.IAuthenticationInfo;

public class AuthenticationInfoTest extends VOTest {

	@Test
	public void marshallingTest() throws FileNotFoundException, JAXBException {

		AuthenticationInfo authInfo = new AuthenticationInfo();
		authInfo.setUserName( ADMIN_EMAIL );
		authInfo.setAuthenticated( true );
		authInfo.getRoles().add( "ADMIN" );
		
		
		DataStockVO stock1 = new DataStockVO();
		stock1.setUuid( "00000000-0000-0000-0000-000000000000" );
		stock1.setShortName( "STOCK1" );
		stock1.getRoles().add( "READ" );
		stock1.getRoles().add( "EXPORT" );
		authInfo.getDataStockRoles().add( stock1 );
	
		ServiceDAO dao = new ServiceDAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(authInfo, System.out);
		dao.marshal(authInfo, new FileOutputStream(new File(TEST_FILENAME_OUT)));

	}

	@Test
	public void unmarshallingTest() throws FileNotFoundException, JAXBException {

		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME));

		ServiceDAO dao = new ServiceDAO();

		IAuthenticationInfo authInfo = (IAuthenticationInfo) dao.unmarshal(stream);

		assertEquals(authInfo.getUserName(), ADMIN_EMAIL);
		assertTrue(authInfo.getRoles().contains( "ADMIN" ));

		assertEquals( authInfo.getDataStockRoles().get( 0 ).getUuid(), "00000000-0000-0000-0000-000000000000" );
		assertEquals( authInfo.getDataStockRoles().get( 0 ).getShortName(), "STOCK1" );
		assertTrue( authInfo.getDataStockRoles().get( 0 ).getRoles().contains( "READ" ));
		assertTrue( authInfo.getDataStockRoles().get( 0 ).getRoles().contains( "EXPORT" ));
		
	}

	public static final String TEST_FILENAME = PATH_PREFIX + "service_api_auth.xml";

	public static final String TEST_FILENAME_OUT = PATH_PREFIX_OUT + "service_api_auth.xml";

	public static final String BASE_URL = "http://localhost:8090/Node/resource/";

	public static final String NODE_ID = "ACME";

	public static final String NODE_NAME = "ACME products LCI database";

	public static final String OPERATOR = "ACME, Inc.";

	public static final String ADMIN_NAME = "Dr. Bar";

	public static final String ADMIN_EMAIL = "bar@acme.com";

	public static final String ADMIN_PHONE = "+49";

	public static final String ADMIN_WWW = "http://www.acme.com/bar";

	public static final String DESCRIPTION = "LCI database that contains reviewed datasets for all ACME products";

	public static final String DESCRIPTION_DE = "Sachbilanzdatenbank für alle von ACME hergestellten Produkte";
}
