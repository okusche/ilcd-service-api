package de.fzk.iai.ilcd.service.client.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import org.junit.Before;

import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.service.client.impl.ILCDNetworkClient;
import org.junit.BeforeClass;

public class AbstractILCDNetworkClientTestIT {

	public static final String BASE_URL = "http://localhost:8090/resource/";
	public static final String PROCESS_UUID = "fd6e82e6-1aa5-4c0a-86d3-4ca0b0ddb974";
	public static final String PROCESS_VERSION = "00.03.000";
	public static final String SOURCE_UUID = "e4d50832-85c5-4207-b5c4-c8543e16fa3c";
	public static final String CONTACT_UUID = "4ae407df-4514-45fb-989e-795d4f91fb41";
	public static final String CONTACT_NAME = "German ILCD registration authority";
	public static final String NON_EXISTENT_UUID = "f00baf00-f00b-f00b-f00b-f00baf00baf0";
	public static final String NON_EXISTENT_VERSION = "99.99.999";
	public static final String SOURCE_FILE = "src/test/xml/ILCD/sources/Flowchart_Power_grid_mix_DE_1998_e4d50832-85c5-4207-b5c4-c8543e16fa3c.xml";
	public static final String CONTACT_FILE = "src/test/xml/ILCD/contacts/4ae407df-4514-45fb-989e-795d4f91fb41.xml";
	public static final String PROCESS_FILE = "src/test/xml/ILCD/processes/Power_grid_mix_AC_consumption_mix_at_consumer_220V_fd6e82e6-1aa5-4c0a-86d3-4ca0b0ddb974.xml";

	protected static ILCDNetworkClient client = null;

	public AbstractILCDNetworkClientTestIT() {
		super();
	}
	
	protected static String getBaseURL() {
		return System.getProperty("soda.base.url", BASE_URL);
	}

	@BeforeClass
	public static void setup() throws Exception {
		// this.client = new ILCDNetworkClient(BASE_URL);
		client = new ILCDNetworkClient( getBaseURL(), "admin", "default" );

		InputStream fis = new FileInputStream( new File(SOURCE_FILE) );
		client.putDataSetAsStream(SourceDataSet.class, fis);

		fis = new FileInputStream( new File(CONTACT_FILE) );
		client.putDataSetAsStream(ContactDataSet.class, fis);

		fis = new FileInputStream( new File(PROCESS_FILE) );
		client.putDataSetAsStream(ProcessDataSet.class, fis);

	}

}