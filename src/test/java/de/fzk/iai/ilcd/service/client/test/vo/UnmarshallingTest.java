/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.epd.ProcessSubType;
import de.fzk.iai.ilcd.service.model.IDataSetListVO;

public class UnmarshallingTest extends de.fzk.iai.ilcd.service.client.test.vo.VOTest {

	@Before
	public void setup() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void simpleUnmarshallingTest() throws FileNotFoundException, JAXBException {

		FileInputStream stream = new FileInputStream(new File(PATH_PREFIX + "service_api_single_process_dataset.xml"));

		DatasetVODAO dao = new DatasetVODAO();

		IDataSetListVO dataset = (IDataSetListVO) dao.unmarshal(stream);

		ProcessDataSetVO process = (ProcessDataSetVO) dataset;

		System.out.println(dataset.toString());
		System.out.println(process.isAccessRestricted());
		System.out.println("node name: " + dataset.getSourceId());

	}

	@Test
	public void extensionsUnmarshallingTest() throws FileNotFoundException, JAXBException {

		FileInputStream stream = new FileInputStream(new File(PATH_PREFIX + "service_api_single_process_dataset_with_extensions.xml"));

		DatasetVODAO dao = new DatasetVODAO();

		IDataSetListVO dataset = (IDataSetListVO) dao.unmarshal(stream);

		ProcessDataSetVO process = (ProcessDataSetVO) dataset;
		
		System.out.println(dataset.toString());
		System.out.println(process.isAccessRestricted());
		System.out.println("node name: " + dataset.getSourceId());
		System.out.println(process.getSubType());
		
		Assert.assertEquals( ProcessSubType.AVERAGE_DATASET, process.getSubType() );

	}

	@Test
	public void multiUnmarshallingTest() throws Exception {

		FileInputStream stream = new FileInputStream(new File(PATH_PREFIX + "service_api_dataset_list.xml"));

		DatasetVODAO dao = new DatasetVODAO();

		DataSetList list = (DataSetList) dao.unmarshal(stream);

		System.out.println(list);

		System.out.println("node name: " + list.getSourceId());

		for (IDataSetListVO ds : list.getDataSet()) {
			System.out.println(ds.getClass().getName() + " " + ds);
			if (ds instanceof FlowDataSetVO) {
				FlowDataSetVO flow = (FlowDataSetVO) ds;

				System.out.println(flow.getFlowCategorization().getCategory(0));
				System.out.println(flow.getFlowCategorization().getCategory(1));
				System.out.println(flow.getFlowCategorization().getCategory(2));
			} else if (ds instanceof ProcessDataSetVO) {
				ProcessDataSetVO process = (ProcessDataSetVO) ds;
				System.out.println("access restricted: " + process.isAccessRestricted());
				if (process.getUuidAsString().equals("10000000-0000-0000-0000-000000000000"))
					assertEquals(ProcessSubType.SPECIFIC_DATASET, process.getSubType());
			}
		}

	}

//	<T> JAXBElement<T> wrap(String ns, String tag, T o) {
//		QName qtag = new QName(ns, tag);
//		Class<?> clazz = o.getClass();
//		@SuppressWarnings("unchecked")
//		JAXBElement<T> jbe = new JAXBElement(qtag, clazz, o);
//		return jbe;
//	}
}
