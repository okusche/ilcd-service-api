/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for
 * Applied Computer Science (IAI).
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ContactDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.model.*;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.binding.helper.DatasetDAO;
import de.fzk.iai.ilcd.service.client.DatasetNotFoundException;
import de.fzk.iai.ilcd.service.client.ILCDServiceClientException;
import de.fzk.iai.ilcd.service.client.impl.ILCDClientResponse;
import de.fzk.iai.ilcd.service.client.impl.ILCDNetworkClient;
import de.fzk.iai.ilcd.service.client.impl.vo.Result;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;

public class ILCDNetworkClientTestIT extends AbstractILCDNetworkClientTestIT {

	private final int MAX_LOOPS = 3;

	protected final Logger log = org.apache.logging.log4j.LogManager.getLogger( this.getClass() );

	@Test
	public void testGetNodeInfo() {
		log.info( "testing GET nodeinfo" );

		INodeInfo nodeInfo = client.getNodeInfo();

		log.debug( "got node info from: " + nodeInfo.getNodeID() + " at " + nodeInfo.getBaseURL() );

	}
	
	@Test
	public void testDataSetExists() {
		log.info( "testing existsDataset()" );
		boolean exists = client.existsDataSet(ProcessDataSet.class, PROCESS_UUID, PROCESS_VERSION);
		assertTrue(exists);
		
		boolean exists2 = client.existsDataSet(ProcessDataSet.class, PROCESS_UUID, null);
		assertTrue(exists2);

		boolean notExists = client.existsDataSet(ProcessDataSet.class, PROCESS_UUID, "00.33.000");
		assertTrue(!notExists);
		
		boolean notExists2 = client.existsDataSet(ProcessDataSet.class, "aaae82e6-1aa5-4c0a-86d3-4ca0b0ddb974", null);
		assertTrue(!notExists2);
	}
	
	@Test
	public void testGetProcess() throws ILCDServiceClientException {

		log.info( "testing GET process" );
		ProcessDataSet process;
		try {
			process = client.getDataSet( ProcessDataSet.class, PROCESS_UUID );
			log.info( "process UUID: " + process.getProcessInformation().getDataSetInformation().getUUID() );
		}
		catch ( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetProcessWithVersion () throws ILCDServiceClientException {

		log.info( "testing GET process" );
		ProcessDataSet process;
		try {
			process = client.getDataSet( ProcessDataSet.class, PROCESS_UUID, PROCESS_VERSION );
			log.info( "process UUID: " + process.getProcessInformation().getDataSetInformation().getUUID() );
		}
		catch ( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetSources() throws IOException {
		log.info( "testing GET source" );
		Result<ISourceListVO> result = client.getDataSets( ISourceListVO.class );
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i = 0;
		for ( ISourceListVO source : result.getDataSets() ) {
			log.info( source.getDefaultName() );
			i++;
			if ( i > MAX_LOOPS )
				break;
		}
	}

	@Test
	public void testGetFlows() throws IOException {
		log.info( "testing GET flow" );
		Result<IFlowListVO> result = client.getDataSets( IFlowListVO.class );
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i = 0;
		for ( IFlowListVO flow : result.getDataSets() ) {
			log.info( flow.getDefaultName() );
			i++;
			if ( i > MAX_LOOPS )
				break;
		}
	}

	@Test
	public void testGetLCIAMethods() throws IOException {
		log.info( "testing GET flow" );
		Result<ILCIAMethodListVO> result = client.getDataSets( ILCIAMethodListVO.class );
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i = 0;
		for ( ILCIAMethodListVO method : result.getDataSets() ) {
			log.info( method.getDefaultName() );
			i++;
			if ( i > MAX_LOOPS )
				break;
		}
	}

	@Test
	public void testGetFlowProperties() throws IOException {
		log.info( "testing GET flowproperty" );
		Result<IFlowPropertyListVO> result = client.getDataSets( IFlowPropertyListVO.class );
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i = 0;
		for ( IFlowPropertyListVO flowprop : result.getDataSets() ) {
			log.info( flowprop.getDefaultName() );
			i++;
			if ( i > MAX_LOOPS )
				break;
		}
	}

	@Test
	public void testGetUnitGroups() throws IOException {
		log.info( "testing GET unitgroup" );
		Result<IUnitGroupListVO> result = client.getDataSets( IUnitGroupListVO.class );
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i = 0;
		for ( IUnitGroupListVO dataset : result.getDataSets() ) {
			log.info( dataset.getDefaultName() );
			i++;
			if ( i > MAX_LOOPS )
				break;
		}
	}

	@Test
	public void testGetContacts() throws IOException {
		log.info( "testing GET contact" );
		Result<IContactListVO> result = client.getDataSets( IContactListVO.class );
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i = 0;
		for ( IContactListVO dataset : result.getDataSets() ) {
			log.info( dataset.getDefaultName() );
			i++;
			if ( i > MAX_LOOPS )
				break;
		}
	}

	@Test
	public void testGetProcesses() throws IOException, ILCDServiceClientException {
		log.info( "testing GET process" );
		Result<IProcessListVO> result = client.getDataSets( IProcessListVO.class );
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i = 0;
		for ( IProcessListVO process : result.getDataSets() ) {
			log.info( "VO simpleName: " + process.getDefaultName() );
			log.info( "VO UUID:       " + process.getUuidAsString() );
			ProcessDataSet fullProcess = client.getDataSet( ProcessDataSet.class, process.getUuidAsString() );
			try {
				log.info( "full Process baseName: " + fullProcess.getProcessInformation().getDataSetInformation().getName().getBaseName().get( 0 ).getValue() );
			}
			catch ( NullPointerException e ) {
				log.error( "full Process baseName appears to be null" );
			}
			log.info( "full Process UUID:     " + fullProcess.getProcessInformation().getDataSetInformation().getUUID() );
			assertEquals( process.getUuidAsString(), fullProcess.getProcessInformation().getDataSetInformation().getUUID() );
			i++;
			if ( i > MAX_LOOPS )
				break;
		}
	}

	@Test
	public void testPostProcess() {
		log.info( "testing POST process" );

		Result<IProcessListVO> result;
		try {
			result = client.getDataSets( IProcessListVO.class );
			log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
			for ( IProcessListVO process : result.getDataSets() ) {
				log.info( "VO simpleName: " + process.getDefaultName() );
				log.info( "VO UUID:       " + process.getUuidAsString() );
				ProcessDataSet fullProcess = client.getDataSet( ProcessDataSet.class, process.getUuidAsString() );
				try {
					log.info( "full Process baseName: "
							+ fullProcess.getProcessInformation().getDataSetInformation().getName().getBaseName().get( 0 ).getValue() );
				}
				catch ( NullPointerException e ) {
					log.error( "full Process baseName appears to be null" );
				}
				log.info( "full Process UUID:     " + fullProcess.getProcessInformation().getDataSetInformation().getUUID() );

				String newUUID = modUUID( fullProcess.getProcessInformation().getDataSetInformation().getUUID() );
				log.info( "setting new UUID " + newUUID );

				fullProcess.getProcessInformation().getDataSetInformation().setUUID( newUUID );

				client.putDataSet( fullProcess );

				// wait a little for the server to complete the operation
				log.info( "waiting..." );
				Thread.sleep( 1000 );

				// let's see if the dataset has made it to the database
				assertTrue( client.getDataSet( ProcessDataSet.class, newUUID ).getProcessInformation().getDataSetInformation().getUUID().equals( newUUID ) );

				break;
			}
		}
		catch ( Exception e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Test
	public void testPostProcessInStock() {
		log.info( "testing POST process" );

		Result<IProcessListVO> result;
		try {

			result = client.getDataSets( IProcessListVO.class );
			log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
			for ( IProcessListVO process : result.getDataSets() ) {
				log.info( "VO simpleName: " + process.getDefaultName() );
				log.info( "VO UUID:       " + process.getUuidAsString() );
				ProcessDataSet fullProcess = client.getDataSet( ProcessDataSet.class, process.getUuidAsString() );
				try {
					log.info( "full Process baseName: "
							+ fullProcess.getProcessInformation().getDataSetInformation().getName().getBaseName().get( 0 ).getValue() );
				}
				catch ( NullPointerException e ) {
					log.error( "full Process baseName appears to be null" );
				}
				log.info( "full Process UUID:     " + fullProcess.getProcessInformation().getDataSetInformation().getUUID() );

				String newUUID = modUUID( fullProcess.getProcessInformation().getDataSetInformation().getUUID() );
				log.info( "setting new UUID " + newUUID );

				fullProcess.getProcessInformation().getDataSetInformation().setUUID( newUUID );

				client.putDataSet( fullProcess, "7c6fdb08-902e-48de-bb94-9600bf317331" );

				// wait a little for the server to complete the operation
				log.info( "waiting..." );
				Thread.sleep( 1000 );

				// let's see if the dataset has made it to the database
				assertTrue( client.getDataSet( ProcessDataSet.class, newUUID ).getProcessInformation().getDataSetInformation().getUUID().equals( newUUID ) );

				break;
			}
		}
		catch ( Exception e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Test
	public void testPostContact() {
		log.info( "testing POST contact" );

		Result<IContactListVO> result;
		try {
			result = client.getDataSets( IContactListVO.class );
			log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
			for ( IContactListVO contact : result.getDataSets() ) {
				log.info( "VO simpleName: " + contact.getDefaultName() );
				log.info( "VO UUID:       " + contact.getUuidAsString() );
				ContactDataSet fullContact = client.getDataSet( ContactDataSet.class, contact.getUuidAsString() );
				try {
					log.info( "contact name: "
							+ fullContact.getContactInformation().getDataSetInformation().getName().get( 0 ).getValue() );
				}
				catch ( NullPointerException e ) {
					log.error( "contact name appears to be null" );
				}
				
				fullContact.getContactInformation().getDataSetInformation().getName().get( 0 ).setValue( "ACME & Co." );
				
				log.info( "full contact UUID:     " + fullContact.getContactInformation().getDataSetInformation().getUUID() );

				String newUUID = modUUID( fullContact.getContactInformation().getDataSetInformation().getUUID() );
				log.info( "setting new UUID " + newUUID );

				fullContact.getContactInformation().getDataSetInformation().setUUID( newUUID );

				client.putDataSet( fullContact );

				// wait a little for the server to complete the operation
				log.info( "waiting..." );
				Thread.sleep( 1000 );

				// let's see if the dataset has made it to the database
				assertTrue( client.getDataSet( ContactDataSet.class, newUUID ).getContactInformation().getDataSetInformation().getUUID().equals( newUUID ) );

				break;
			}
		}
		catch ( Exception e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Test
	public void testAuthenticationInfo() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL(), "admin", "default" );

		IAuthenticationInfo ai = client.getAuthenticationStatus();

		log.debug( "authenticated: " + (ai.isAuthenticated()) );

		assertTrue( ai.isAuthenticated() );
		assertEquals( "admin", ai.getUserName() );

		log.debug( "roles:" );
		for ( String role : ai.getRoles() ) {
			log.debug( role );
		}

	}

	// broken in soda4LCA, needs fix first
//	@Test
	public void testGetProcessVO() throws ILCDServiceClientException {

		log.info( "testing GET process" );
		Result<IProcessVO> result;
		try {
			result = client.getDataSets( IProcessVO.class );
			log.info( result.getDataSets().size() + " found" );
			int i = 0;
			for ( IProcessVO process : result.getDataSets() ) {
				log.info( "VO simpleName: " + process.getDefaultName() );
				log.info( "VO UUID:       " + process.getUuidAsString() );
				IProcessVO processVO = client.getDataSetVO( IProcessVO.class, process.getUuidAsString() );
				assertEquals( process.getUuidAsString(), processVO.getUuidAsString() );
				i++;
				if ( i > MAX_LOOPS )
					break;
			}

		}
		catch ( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testAuthentication() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL(), "admin", "default" );

		Result<IProcessListVO> datasets = client.getDataSets( IProcessListVO.class );
		log.debug( datasets.getDataSets().size() + " datasets found" );
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testHTTP403() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL() );

		Result<IProcessListVO> datasets = client.getDataSets( IProcessListVO.class );

		ProcessDataSetVO datasetVO = (ProcessDataSetVO) datasets.getDataSets().toArray()[0];

		String UUID = datasetVO.getUuidAsString();

		ProcessDataSet dataset = client.getDataSet( ProcessDataSet.class, UUID );
		dataset.getProcessInformation().getDataSetInformation().setUUID( modUUID( dataset.getProcessInformation().getDataSetInformation().getUUID() ) );
		ILCDClientResponse response = client.putDataSet( dataset );

		log.info( response.getClientResponse().getStatus() + " " + response.getBody() );

		assertEquals( response.getStatus(), 403 );
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testHTTP403AsStream() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL() );

		Result<IProcessListVO> datasets = client.getDataSets( IProcessListVO.class );

		ProcessDataSetVO datasetVO = (ProcessDataSetVO) datasets.getDataSets().toArray()[0];

		String UUID = datasetVO.getUuidAsString();

		ProcessDataSet dataset = client.getDataSet( ProcessDataSet.class, UUID );
		String newUUID = modUUID( dataset.getProcessInformation().getDataSetInformation().getUUID() );
		dataset.getProcessInformation().getDataSetInformation().setUUID( newUUID );

		// save the ds to disk
		File tmpFile = File.createTempFile( newUUID, ".xml" );
		tmpFile.deleteOnExit();
		String fileName = tmpFile.getAbsolutePath();

		DatasetDAO dao = new DatasetDAO();
		dao.saveDataset( dataset, fileName );

		// set up an InputStream from that file
		InputStream fis = new FileInputStream( tmpFile );

		ILCDClientResponse response = client.putDataSetAsStream( ProcessDataSet.class, fis );

		log.info( response.getClientResponse().getStatus() + " " + response.getBody() );

		assertEquals( 403, response.getClientResponse().getStatus(), 403 );

	}

	@Test
	public void getDataSetHTTP404() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL() );
		try {
			client.getDataSet( ProcessDataSet.class, NON_EXISTENT_UUID );
			assertTrue( false );
		}
		catch ( DatasetNotFoundException e ) {
			log.info( e );
			assertTrue( true );
		}
		catch ( Exception e ) {
			log.error( e );
			assertTrue( false );
		}
	}

	@Test
	public void getDataSetVOHTTP404() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL() );
		try {
			client.getDataSetVO( ProcessDataSetVO.class, NON_EXISTENT_UUID );
			assertTrue( false );
		}
		catch ( DatasetNotFoundException e ) {
			log.info( e );
			assertTrue( true );
		}
		catch ( Exception e ) {
			log.error( e );
			assertTrue( false );
		}
	}

	@Test
	public void getDataSetVOAsStreamHTTP404() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL() );
		try {
			client.getDataSetVOAsStream( ProcessDataSetVO.class, NON_EXISTENT_UUID );
			assertTrue( false );
		}
		catch ( DatasetNotFoundException e ) {
			log.info( e );
			assertTrue( true );
		}
		catch ( Exception e ) {
			log.error( e );
			assertTrue( false );
		}
	}

	@Test
	public void getDataSetAsStreamHTTP404() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL() );
		try {
			client.getDataSetAsStream( ProcessDataSet.class, NON_EXISTENT_UUID );
			assertTrue( false );
		}
		catch ( DatasetNotFoundException e ) {
			log.info( e );
			assertTrue( true );
		}
		catch ( Exception e ) {
			log.error( e );
			assertTrue( false );
		}
	}

	@Test
	public void getFullDataSetHTTP404() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL() );
		try {
			client.getFullDataset( getBaseURL() + "processes/" + NON_EXISTENT_UUID );
			assertTrue( false );
		}
		catch ( DatasetNotFoundException e ) {
			log.info( e );
			assertTrue( true );
		}
		catch ( Exception e ) {
			log.error( e );
			assertTrue( false );
		}
	}

	@Test
	public void getFullDataSetAsStreamHTTP404() throws Exception {
		ILCDNetworkClient client = new ILCDNetworkClient( getBaseURL() );
		try {
			client.getFullDatasetAsStream( getBaseURL() + "processes/" + NON_EXISTENT_UUID );
			assertTrue( false );
		}
		catch ( DatasetNotFoundException e ) {
			log.info( e );
			assertTrue( true );
		}
		catch ( Exception e ) {
			log.error( e );
			assertTrue( false );
		}
	}

	public String modUUID( String originalUUID ) {

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat( "ddHHmmss" );

		String newUUID = sdf.format( cal.getTime() ) + originalUUID.substring( originalUUID.indexOf( "-" ) );

		return newUUID;
	}

	@Test
	public void testGetExchanges() throws IOException, DatasetNotFoundException {
		log.info( "testing GET exchanges" );
		Result<IProcessListVO> result = client.getDataSets( IProcessListVO.class );
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i = 0;
		for ( IProcessListVO process : result.getDataSets() ) {
			log.info( "process simpleName: " + process.getDefaultName() );
			log.info( "process UUID:       " + process.getUuidAsString() );

			Result<IFlowListVO> r = client.getExchanges( process.getUuidAsString(), ILCDNetworkClient.EXCHANGES_DIRECTION_OUT, null );

			log.info( r.getTotalSize() + " output flows" );

			assertEquals( r.getTotalSize(), r.getDataSets().size() );

			assertTrue( r.getTotalSize() > 0 );

			i++;
			if ( i > MAX_LOOPS )
				break;
		}
	}

	@Test
	public void testGetRegistrationAuthorities() throws IOException, DatasetNotFoundException {
		log.info( "testing GET registrationAuthorities" );
		Result<IContactListVO> result = client.getRegistrationAuthorities();
		log.debug( result.getDataSets().size() + " found on node " + client.getNodeName() );
		int i=0;
		for (IContactListVO contact : result.getDataSets() ) {
			log.info( "contact simpleName: " + contact.getDefaultName() );
			log.info( "contact UUID:       " + contact.getUuidAsString() );
			if (i==0) {
				assertEquals(CONTACT_UUID, contact.getUuidAsString());
				assertEquals(CONTACT_NAME, contact.getDefaultName());
			}
			i++;
		}
	}
}
