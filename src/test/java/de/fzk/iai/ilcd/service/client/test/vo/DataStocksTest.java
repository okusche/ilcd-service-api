/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockList;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockVO;
import de.fzk.iai.ilcd.service.model.IDataStockListVO;
import de.fzk.iai.ilcd.service.model.IDataStockVO;

public class DataStocksTest extends VOTest {

	@Test
	public void marshallingTest() throws FileNotFoundException, JAXBException {

		DataStockVO stock = new DataStockVO();
		stock.setUuid(UUID1);
		stock.setRoot(true);
		stock.setName(NAME1);
		stock.setShortName(SHORT_NAME1);
		stock.setDescription(DESCRIPTION);
		stock.setDescription("de", DESCRIPTION_DE);

		DataStockVO stock2 = new DataStockVO();
		stock2.setUuid(UUID2);
		stock2.setName(NAME2);
		stock2.setShortName(SHORT_NAME2);

		DataStockList list = new DataStockList();
		list.getDataStocks().add(stock);
		list.getDataStocks().add(stock2);

		ServiceDAO dao = new ServiceDAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(list, System.out);
		dao.marshal(list, new FileOutputStream(new File(TEST_FILENAME_OUT)));

	}

	@Test
	public void unmarshallingTest() throws FileNotFoundException, JAXBException {

		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME));

		ServiceDAO dao = new ServiceDAO();

		IDataStockListVO list = (IDataStockListVO) dao.unmarshal(stream);

		IDataStockVO stock1 = list.getDataStocks().get(0);
		IDataStockVO stock2 = list.getDataStocks().get(1);

		assertEquals(stock1.getUuid(), UUID1);
		assertEquals(stock2.getUuid(), UUID2);
		
		assertEquals(stock1.getShortName(), SHORT_NAME1);
		assertEquals(stock2.getShortName(), SHORT_NAME2);

		assertEquals(stock1.getName().getValue("en"), NAME1);
		assertEquals(stock2.getName().getValue("en"), NAME2);

		assertEquals(stock1.getDescription().getValue("en"), DESCRIPTION);
		assertEquals(stock1.getDescription().getValue("de"), DESCRIPTION_DE);

		assertEquals(stock1.isRoot(), true);
		assertEquals(stock2.isRoot(), false);

	}

	public static final String UUID1 = "00000000-0000-0000-0000-000000000000";
	
	public static final String UUID2 = "00000000-0000-0000-0000-000000000001";
	
	public static final String TEST_FILENAME = PATH_PREFIX + "service_api_datastocks.xml";

	public static final String TEST_FILENAME_OUT = PATH_PREFIX_OUT + "service_api_datastocks.xml";

	public static final String SHORT_NAME1 = "default";

	public static final String SHORT_NAME2 = "other";

	public static final String NAME1 = "Default Root Data Stock";

	public static final String NAME2 = "Other, Non-Root Data Stock";

	public static final String DESCRIPTION = "description";

	public static final String DESCRIPTION_DE = "deutsche Beschreibung";
}
