package de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.test;

import de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.NodeInfo;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NodeInfoTest extends NodeInfo {

    @Test
    public void testAddTrailingSlash() {
        assertEquals(null, addTrailingSlash(null));
        assertEquals("/", addTrailingSlash(""));
        assertEquals("foo/", addTrailingSlash("foo"));
        assertEquals("foo/", addTrailingSlash("foo/"));
        assertEquals("/foo/", addTrailingSlash("/foo"));
        assertEquals("/foo/", addTrailingSlash("/foo/"));
    }

    @Test
    public void testNoTrailingSlash() {
        assertEquals(null, noTrailingSlash(null));
        assertEquals("", noTrailingSlash(""));
        assertEquals("", noTrailingSlash("/"));
        assertEquals("foo", noTrailingSlash("foo"));
        assertEquals("foo", noTrailingSlash("foo/"));
        assertEquals("/foo", noTrailingSlash("/foo"));
        assertEquals("/foo", noTrailingSlash("/foo/"));
    }
}
