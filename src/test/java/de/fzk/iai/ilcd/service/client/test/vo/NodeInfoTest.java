/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.NodeInfo;
import de.fzk.iai.ilcd.service.model.INodeInfo;

public class NodeInfoTest extends VOTest {

	@Test
	public void marshallingTest() throws FileNotFoundException, JAXBException {

		NodeInfo nodeInfo = new NodeInfo();
		nodeInfo.setBaseURL(BASE_URL);
		nodeInfo.setNodeID(NODE_ID);
		nodeInfo.setName(NODE_NAME);
		nodeInfo.setOperator(OPERATOR);
		nodeInfo.setAdminName(ADMIN_NAME);
		nodeInfo.setAdminEMail(ADMIN_EMAIL);
		nodeInfo.setAdminPhone(ADMIN_PHONE);
		nodeInfo.setAdminWWW(ADMIN_WWW);
		nodeInfo.setDescription(DESCRIPTION);
		nodeInfo.setDescription("de", DESCRIPTION_DE);

		ServiceDAO dao = new ServiceDAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(nodeInfo, System.out);
		dao.marshal(nodeInfo, new FileOutputStream(new File(TEST_FILENAME)));
	}

	@Test
	public void unmarshallingTest() throws FileNotFoundException, JAXBException {

		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME));

		ServiceDAO dao = new ServiceDAO();

		INodeInfo nodeInfo = (INodeInfo) dao.unmarshal(stream);

		assertEquals(nodeInfo.getBaseURL(), BASE_URL);
		assertEquals(nodeInfo.getNodeID(), NODE_ID);
		assertEquals(nodeInfo.getName(), NODE_NAME);
		assertEquals(nodeInfo.getOperator(), OPERATOR);
		assertEquals(nodeInfo.getDescription().getValue(), DESCRIPTION);
		assertEquals(nodeInfo.getDescription().getValue("de"), DESCRIPTION_DE);
		assertEquals(nodeInfo.getAdminName(), ADMIN_NAME);
		assertEquals(nodeInfo.getAdminEMail(), ADMIN_EMAIL);
		assertEquals(nodeInfo.getAdminPhone(), ADMIN_PHONE);
		assertEquals(nodeInfo.getAdminWWW(), ADMIN_WWW);
	}

	@Test
	public void testWithResourcePrefix() {
		NodeInfo nodeInfo = new NodeInfo();
		nodeInfo.setBaseURL(BASE_URL);

		assertEquals(BASE_URL, nodeInfo.getBaseURLwithResource());
		testNodeInfoWithResource(BASE_URL_RESOURCE_VARIANT1, BASE_URL);
		testNodeInfoWithResource(BASE_URL_RESOURCE_VARIANT2, BASE_URL);

		assertEquals(BASE_URL_NORESOURCE, nodeInfo.getBaseURLnoResource());
		testNodeInfoNoResource(BASE_URL_NORESOURCE_VARIANT1, BASE_URL_NORESOURCE);
		testNodeInfoNoResource(BASE_URL_NORESOURCE_VARIANT2, BASE_URL_NORESOURCE);
	}

	private void testNodeInfoWithResource(String baseUrl, String expected) {
		NodeInfo nodeInfo = new NodeInfo();
		nodeInfo.setBaseURL(baseUrl);
		assertEquals(expected, nodeInfo.getBaseURLwithResource());
	}

	private void testNodeInfoNoResource(String baseUrl, String expected) {
		NodeInfo nodeInfo = new NodeInfo();
		nodeInfo.setBaseURL(baseUrl);
		assertEquals(expected, nodeInfo.getBaseURLnoResource());
	}

	public static final String TEST_FILENAME = PATH_PREFIX + "service_api_nodeInfo.xml";

	public static final String TEST_FILENAME_OUT = PATH_PREFIX_OUT + "service_api_nodeInfo.xml";

	public static final String BASE_URL = "http://localhost:8090/Node/resource/";

	public static final String BASE_URL_NORESOURCE = "http://localhost:8090/Node/";

	public static final String BASE_URL_RESOURCE_VARIANT1 = "http://localhost:8090/Node/resource/";

	public static final String BASE_URL_RESOURCE_VARIANT2 = "http://localhost:8090/Node/resource";

	public static final String BASE_URL_NORESOURCE_VARIANT1 = "http://localhost:8090/Node/";

	public static final String BASE_URL_NORESOURCE_VARIANT2 = "http://localhost:8090/Node";

	public static final String NODE_ID = "ACME";

	public static final String NODE_NAME = "ACME products LCI database";

	public static final String OPERATOR = "ACME, Inc.";

	public static final String ADMIN_NAME = "Dr. Bar";

	public static final String ADMIN_EMAIL = "bar@acme.com";

	public static final String ADMIN_PHONE = "+49";

	public static final String ADMIN_WWW = "http://www.acme.com/bar";

	public static final String DESCRIPTION = "LCI database that contains reviewed datasets for all ACME products";

	public static final String DESCRIPTION_DE = "Sachbilanzdatenbank für alle von ACME hergestellten Produkte";
}
