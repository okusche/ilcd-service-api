/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.epd.ProcessSubType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.CategoryType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassificationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ComplianceSystemType;
import de.fzk.iai.ilcd.service.model.enums.TypeOfProcessValue;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;

public class MarshallingTest extends de.fzk.iai.ilcd.service.client.test.vo.VOTest {

	private ObjectFactory of = null;

	@Before
	public void setup() {
		this.of = new ObjectFactory();
	}

	@After
	public void tearDown() {
	}

	@Test
	public void listMarshallingTest() throws Exception {
		ProcessDataSetVO dataset = of.processDataSetVO();
		dataset.setAccessRestricted(true);
		dataset.setHref("http://localhost/samples/service_api_single_process_dataset.xml");
		dataset.setUuid("10000000-0000-0000-0000-000000000000");
		dataset.setName("Foo unit process");
		dataset.setPermanentUri("http://db.ilcd-network.org/data/00F42");
		dataset.setType(TypeOfProcessValue.UNIT_PROCESS_SINGLE_OPERATION);
		dataset.setSubType(ProcessSubType.SPECIFIC_DATASET);
		dataset.setParameterized(true);

		ClassificationType classification = new ClassificationType();
		classification.setName( "ILCD" );
		classification.add( new ClassType( 0, "foo"));
		classification.add( new ClassType( 1, "bar"));
		dataset.setClassification( classification );

		dataset.getSynonyms().setValue("Foobar");
		dataset.getSynonyms().setValue("de", "Bar");
		// dataset.getSynonyms().add(new LString("Foobar"));
		// dataset.getSynonyms().add(new LString("Bar", "de"));
		
		ComplianceSystemType cFoo = new ComplianceSystemType();
		cFoo.setName( "Foo Compliance" );
		
		ComplianceSystemType cBar = new ComplianceSystemType();
		cBar.setName( "BAR Compliance" );
		
		dataset.addComplianceSystem(cFoo  );
		dataset.addComplianceSystem(cBar  );

		ProcessDataSetVO dataset2 = of.processDataSetVO();
		dataset2.setUuid("00000000-0000-0000-0000-000000000000");
		dataset2.setName("Bar unit process");
		dataset2.setPermanentUri("http://db.ilcd-network.org/data/00A23");
		dataset2.setGeneralComment("foo bar blah");

		DataSetVO dataset3 = of.sourceDataSetVO();
		dataset3.setName("Excellent publication");

		FlowDataSetVO dataset4 = new FlowDataSetVO();
		dataset4.setName("Excellent flow");
		dataset4.getFlowCategorization().add(new CategoryType(0, "Emissions"));
		dataset4.getFlowCategorization().add(new CategoryType(1, "Emissions to water"));
		dataset4.getFlowCategorization().add(new CategoryType(2, "Emissions to cold water"));

		DataSetVO dataset5 = of.processDataSetVO();
		dataset5.setUuid("30000000-0000-0000-0000-000000000000");
		dataset5.setName("another process");
		((ProcessDataSetVO) dataset5).setSubType( ProcessSubType.REPRESENTATIVE_DATASET );

		DataSetList list = of.dataSetList();
		list.getDataSet().add(dataset);
		list.getDataSet().add(dataset2);
		list.getDataSet().add(dataset3);
		list.getDataSet().add(dataset4);
		list.getDataSet().add(dataset5);

		list.setSourceId(":LOCAL");

		list.setTotalSize(3);
		list.setPageSize(500);
		list.setStartIndex(0);

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(list, System.out);
		dao.marshal(list, new FileOutputStream(new File(PATH_PREFIX_OUT + "service_api_dataset_list.xml")));

	}

	<T> JAXBElement<T> wrap(String ns, String tag, T o) {
		QName qtag = new QName(ns, tag);
		java.lang.Class<?> clazz = o.getClass();
		@SuppressWarnings("unchecked")
		JAXBElement<T> jbe = new JAXBElement(qtag, clazz, o);
		return jbe;
	}
}
