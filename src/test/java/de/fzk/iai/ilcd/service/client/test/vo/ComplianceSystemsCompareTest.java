/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ComplianceSystemType;

public class ComplianceSystemsCompareTest {

	@Test
	public void testComplianceCompare() {
		ComplianceSystemType ilcdELCompliance = new ComplianceSystemType();
		ilcdELCompliance.setName(COMPLIANCE_ILCD_ENTRY_LEVEL);

		ComplianceSystemType efCompliance = new ComplianceSystemType();
		efCompliance.setName(COMPLIANCE_ENVIRONMENTAL_FOOTPRINT_3_0);

		ComplianceSystemType aCompliance = new ComplianceSystemType();
		aCompliance.setName(COMPLIANCE_ANOTHER);

		assertTrue(ilcdELCompliance.compareTo(efCompliance) > 0);

		assertTrue(efCompliance.compareTo(ilcdELCompliance) < 0);

		assertTrue(ilcdELCompliance.compareTo(aCompliance) > 0);

		assertTrue(aCompliance.compareTo(ilcdELCompliance) < 0);

		assertTrue(aCompliance.compareTo(efCompliance) < 0);

	}

	public static final String COMPLIANCE_ANOTHER = "Another Compliance";

	public static final String COMPLIANCE_ENVIRONMENTAL_FOOTPRINT_3_0 = "Environmental Footprint 3.0 Compliance";

	public static final String COMPLIANCE_ILCD_ENTRY_LEVEL = "ILCD Compliance - entry level";

}
