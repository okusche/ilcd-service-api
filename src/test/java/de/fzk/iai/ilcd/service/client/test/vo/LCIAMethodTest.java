/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.LCIAMethodDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassificationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.lciamethod.TimeInformationType;
import de.fzk.iai.ilcd.service.model.enums.AreaOfProtectionValue;
import de.fzk.iai.ilcd.service.model.enums.LCIAImpactCategoryValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfLCIAMethodValue;

public class LCIAMethodTest extends de.fzk.iai.ilcd.service.client.test.vo.VOTest {
	private ObjectFactory of = null;

	@Before
	public void setup() {
		this.of = new ObjectFactory();
	}

	@Test
	public void simpleLCIAMethodMarshallingTest() throws Exception {
		LCIAMethodDataSetVO dataset = of.LCIAMethodDataSetVO();
		dataset.setUuid(LCIAMethodTest.UUID);
		dataset.setName(NAME);
		// dataset.getName().add(new LString(NAME));
		dataset.setPermanentUri(PERMANENT_URI);
		dataset.setGeneralComment(GENERAL_COMMENT);

		ClassificationType classification = new ClassificationType();
		classification.add(new ClassType(0, CLASSIFICATION_1));
		classification.add(new ClassType(1, CLASSIFICATION_2));
		classification.add(new ClassType(2, CLASSIFICATION_3));
		dataset.setClassification(classification);

		dataset.setType(TypeOfLCIAMethodValue.AREA_OF_PROTECTION_DAMAGE_INDICATOR);

		dataset.getAreaOfProtection().add(AREA_OF_PROTECTION);

		dataset.getMethodology().add(METHODOLOGY_1);
		dataset.getMethodology().add(METHODOLOGY_2);

		dataset.getImpactCategory().add(IMPACT_CATEGORY);

		dataset.setImpactIndicator(IMPACT_INDICATOR);

		dataset.setDataSetVersion(DATASET_VERSION);

		TimeInformationType time = new TimeInformationType();
		time.getDuration().setValue(DURATION);
		time.getReferenceYear().setValue(REFERENCE_YEAR);
		dataset.setTimeInformation(time);

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(TEST_FILENAME_OUT)));

	}

	@Test
	public void simpleUnmarshallingTest() throws Exception {

		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME));

		DatasetVODAO dao = new DatasetVODAO();

		LCIAMethodDataSetVO dataset = (LCIAMethodDataSetVO) dao.unmarshal(stream);

		assertEquals(dataset.getUuidAsString(), UUID);

		assertEquals(dataset.getDataSetVersion(), DATASET_VERSION);
		assertEquals(dataset.getPermanentUri(), PERMANENT_URI);
		assertEquals(dataset.getDefaultName(), NAME);
		assertEquals(dataset.getDescription().getValue(), GENERAL_COMMENT);
		assertEquals(dataset.getTopCategory(), CLASSIFICATION_1);
		assertEquals(dataset.getSubCategory(), CLASSIFICATION_2);
		assertEquals(dataset.getClassification().getClass(2).getName(), CLASSIFICATION_3);
		assertEquals(dataset.getTimeInformation().getDuration().getValue(), DURATION);
		assertEquals(dataset.getTimeInformation().getReferenceYear().getValue(), REFERENCE_YEAR);
		assertEquals(dataset.getAreaOfProtection().get(0), AREA_OF_PROTECTION);
		assertTrue(dataset.getMethodology().get(0).equals(METHODOLOGY_1)
				|| dataset.getMethodology().get(0).equals(METHODOLOGY_2));
		assertTrue(dataset.getMethodology().get(1).equals(METHODOLOGY_1)
				|| dataset.getMethodology().get(1).equals(METHODOLOGY_2));
		assertEquals(dataset.getImpactIndicator(), IMPACT_INDICATOR);
		assertEquals(dataset.getImpactCategory().get(0), IMPACT_CATEGORY);

	}

	public static final String TEST_FILENAME = PATH_PREFIX + "service_api_single_lciamethod_dataset.xml";

	public static final String TEST_FILENAME_OUT = PATH_PREFIX_OUT + "service_api_single_lciamethod_dataset.xml";

	public static final String UUID = "00000000-0000-0000-0000-000000000000";

	public static final String DATASET_VERSION = "01.00.000";

	public static final String PERMANENT_URI = "http://db.ilcd-network.org/data/lciamethods/lciamethodtest";

	public static final String NAME = "Acidification endpoint";

	public static final String GENERAL_COMMENT = "Interim recommended method (need of further improvements).";

	public static final String CLASSIFICATION_1 = "Endpoint/damage level LCIA methods";

	public static final String CLASSIFICATION_2 = "Natural environment";

	public static final String CLASSIFICATION_3 = "Acidification";

	public static final String DURATION = "indefinite";

	public static final String REFERENCE_YEAR = "time independent";

	public static final String METHODOLOGY_1 = "ILCD2011";

	public static final String METHODOLOGY_2 = "ReCiPe2008";

	public static final LCIAImpactCategoryValue IMPACT_CATEGORY = LCIAImpactCategoryValue.ACIDIFICATION;

	public static final AreaOfProtectionValue AREA_OF_PROTECTION = AreaOfProtectionValue.NATURAL_ENVIRONMENT;

	public static final String IMPACT_INDICATOR = "Change in potentially not occuring fraction of plant species per change in base saturation (dimensionless)";
}
