/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.flow.FlowDataSet;
import de.fzk.iai.ilcd.api.app.flowproperty.FlowPropertyDataSet;
import de.fzk.iai.ilcd.api.app.lciamethod.LCIAMethodDataSet;
import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.api.app.unitgroup.UnitGroupDataSet;
import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.service.client.impl.ILCDNetworkClient;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ContactDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowPropertyDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.LCIAMethodDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.SourceDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.UnitGroupDataSetVO;
import de.fzk.iai.ilcd.service.model.IContactListVO;
import de.fzk.iai.ilcd.service.model.IContactVO;
import de.fzk.iai.ilcd.service.model.IFlowListVO;
import de.fzk.iai.ilcd.service.model.IFlowPropertyListVO;
import de.fzk.iai.ilcd.service.model.IFlowPropertyVO;
import de.fzk.iai.ilcd.service.model.IFlowVO;
import de.fzk.iai.ilcd.service.model.ILCIAMethodListVO;
import de.fzk.iai.ilcd.service.model.ILCIAMethodVO;
import de.fzk.iai.ilcd.service.model.IProcessListVO;
import de.fzk.iai.ilcd.service.model.IProcessVO;
import de.fzk.iai.ilcd.service.model.ISourceListVO;
import de.fzk.iai.ilcd.service.model.ISourceVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupListVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupVO;

public class ILCDNetworkClientURLSuffixTest extends ILCDNetworkClient {

	@Test
	public void testGetURLSuffix() {
		assertEquals(super.getURLSuffix(ProcessDataSetVO.class), DatasetTypes.PROCESSES.getValue());
		assertEquals(super.getURLSuffix(IProcessVO.class), DatasetTypes.PROCESSES.getValue());
		assertEquals(super.getURLSuffix(IProcessListVO.class), DatasetTypes.PROCESSES.getValue());
		assertEquals(super.getURLSuffix(ProcessDataSet.class), DatasetTypes.PROCESSES.getValue());

		assertEquals(super.getURLSuffix(FlowDataSetVO.class), DatasetTypes.FLOWS.getValue());
		assertEquals(super.getURLSuffix(IFlowVO.class), DatasetTypes.FLOWS.getValue());
		assertEquals(super.getURLSuffix(IFlowListVO.class), DatasetTypes.FLOWS.getValue());
		assertEquals(super.getURLSuffix(FlowDataSet.class), DatasetTypes.FLOWS.getValue());

		assertEquals(super.getURLSuffix(FlowPropertyDataSetVO.class), DatasetTypes.FLOWPROPERTIES.getValue());
		assertEquals(super.getURLSuffix(IFlowPropertyVO.class), DatasetTypes.FLOWPROPERTIES.getValue());
		assertEquals(super.getURLSuffix(IFlowPropertyListVO.class), DatasetTypes.FLOWPROPERTIES.getValue());
		assertEquals(super.getURLSuffix(FlowPropertyDataSet.class), DatasetTypes.FLOWPROPERTIES.getValue());

		assertEquals(super.getURLSuffix(UnitGroupDataSetVO.class), DatasetTypes.UNITGROUPS.getValue());
		assertEquals(super.getURLSuffix(IUnitGroupVO.class), DatasetTypes.UNITGROUPS.getValue());
		assertEquals(super.getURLSuffix(IUnitGroupListVO.class), DatasetTypes.UNITGROUPS.getValue());
		assertEquals(super.getURLSuffix(UnitGroupDataSet.class), DatasetTypes.UNITGROUPS.getValue());

		assertEquals(super.getURLSuffix(LCIAMethodDataSetVO.class), DatasetTypes.LCIAMETHODS.getValue());
		assertEquals(super.getURLSuffix(ILCIAMethodVO.class), DatasetTypes.LCIAMETHODS.getValue());
		assertEquals(super.getURLSuffix(ILCIAMethodListVO.class), DatasetTypes.LCIAMETHODS.getValue());
		assertEquals(super.getURLSuffix(LCIAMethodDataSet.class), DatasetTypes.LCIAMETHODS.getValue());

		assertEquals(super.getURLSuffix(SourceDataSetVO.class), DatasetTypes.SOURCES.getValue());
		assertEquals(super.getURLSuffix(ISourceVO.class), DatasetTypes.SOURCES.getValue());
		assertEquals(super.getURLSuffix(ISourceListVO.class), DatasetTypes.SOURCES.getValue());
		assertEquals(super.getURLSuffix(SourceDataSet.class), DatasetTypes.SOURCES.getValue());

		assertEquals(super.getURLSuffix(ContactDataSetVO.class), DatasetTypes.CONTACTS.getValue());
		assertEquals(super.getURLSuffix(IContactVO.class), DatasetTypes.CONTACTS.getValue());
		assertEquals(super.getURLSuffix(IContactListVO.class), DatasetTypes.CONTACTS.getValue());
		assertEquals(super.getURLSuffix(ContactDataSet.class), DatasetTypes.CONTACTS.getValue());
	}

}
