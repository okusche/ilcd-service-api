package de.fzk.iai.ilcd.service.client.impl.vo.networknode;

import static java.time.Instant.now;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.NodeInfo;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.xml.bind.JAXBException;
import org.junit.Test;

public class NetworkNodeVOTest {

  private static NetworkNodeStatusVO createNetworkNodeVO(boolean alive) {
    return new NetworkNodeStatusVO(createNodeInfo(UUID.randomUUID().toString()),
        createHeartbeatVOList(new Random().nextInt(11) + 2, alive));
  }

  private static NodeInfo createNodeInfo(String nodeId) {
    NodeInfo nodeInfo = new NodeInfo();
    nodeInfo.setNodeID(nodeId);
    nodeInfo.setName("name_of_" + nodeId);
    nodeInfo.setDescription("description_of_" + nodeId);
    nodeInfo.setBaseURL("base_url_of_" + nodeId);
    nodeInfo.setAdminName("admin_name_of_" + nodeId);
    nodeInfo.setAdminEMail("admin_email_of_" + nodeId);
    nodeInfo.setAdminPhone("admin_phone_of_" + nodeId);
    nodeInfo.setAdminWWW("admin_www_of_" + nodeId);
    nodeInfo.setOperator("operator_of_" + nodeId);
    return nodeInfo;
  }

  private static List<HeartbeatVO> createHeartbeatVOList(int size, boolean alive) {
    List<HeartbeatVO> heartbeats = new ArrayList<>();
    final Duration interval = Duration.ofMinutes(30);
    Instant t = now().minus(Duration.ofHours(Math.floorDiv(size, 2)));
    for (int i = 0; i < size; i++) {
      heartbeats.add(new HeartbeatVO(t, i == size - 1 ? alive
          : i % 2 == 0)); // the last will be given by parameter, elsewhere alternating
      t = t.plus(interval);
    }
    return heartbeats;
  }

  private static String toString(Object object, ServiceDAO marshaller) {
    //noinspection SpellCheckingInspection
    try (final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      marshaller.marshal(object, baos);
      return baos.toString();
    } catch (IOException | JAXBException e) {
      throw new RuntimeException(e);
    }
  }

  private static <T> T fromString(String xml, ServiceDAO marshaller, Class<T> clazz) {
    //noinspection SpellCheckingInspection
    try (final ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes())) {
      final Object obj = marshaller.unmarshal(bais);
      return clazz.cast(obj);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static NetworkNodeStatusListVO createNetworkNodeListVO(NetworkNodeStatusVO... nodes) {
    return new NetworkNodeStatusListVO(new ArrayList<>(Arrays.asList(nodes)));
  }

  @Test
  public void assert_heartbeat_setter_is_sorting_latest_first() {
    HeartbeatVO oldest = new HeartbeatVO(Instant.parse("2023-01-01T10:00:00Z"), true);
    HeartbeatVO middle = new HeartbeatVO(Instant.parse("2023-01-01T12:00:00Z"), true);
    HeartbeatVO latest = new HeartbeatVO(Instant.parse("2023-01-01T15:00:00Z"), true);

    NetworkNodeStatusVO node = new NetworkNodeStatusVO();
    node.setHeartbeats(Arrays.asList(middle, oldest, latest));

    assertEquals("Heartbeats should be ordered with latest first",
        Arrays.asList(latest, middle, oldest), node.getHeartbeats());
  }

  @Test
  public void assert_last_seen_is_calculated_correctly() {
    // ARRANGE
    Instant alive1Instant = Instant.parse("2023-01-01T10:00:00Z");
    Instant dead1Instant = Instant.parse("2023-01-01T11:00:00Z");
    Instant dead2Instant = Instant.parse("2023-01-01T12:00:00Z");
    Instant alive2Instant = Instant.parse("2023-01-01T12:00:00Z");

    HeartbeatVO alive1 = new HeartbeatVO(alive1Instant, true);
    HeartbeatVO dead1 = new HeartbeatVO(dead1Instant, false);
    HeartbeatVO dead2 = new HeartbeatVO(dead2Instant, false);
    HeartbeatVO alive2 = new HeartbeatVO(alive2Instant, true);

    NetworkNodeStatusVO nodeNeverHealthStatus = new NetworkNodeStatusVO();

    NetworkNodeStatusVO nodeDeadNow = new NetworkNodeStatusVO();
    nodeDeadNow.setHeartbeats(Arrays.asList(alive1, dead1));

    NetworkNodeStatusVO nodeAlwaysDead = new NetworkNodeStatusVO();
    nodeAlwaysDead.setHeartbeats(Arrays.asList(dead1, dead2));

    NetworkNodeStatusVO nodeAliveNow = new NetworkNodeStatusVO();
    nodeAliveNow.setHeartbeats(Arrays.asList(alive1, dead1, alive2));

    // ACT

    // ASSERT
    // assertEquals("", "", "");
    assertNull("Unexpected is-alive state on node: " + nodeNeverHealthStatus,
        nodeNeverHealthStatus.isAlive());
    assertNull("Unexpected last-seen state on node: " + nodeNeverHealthStatus,
        nodeNeverHealthStatus.getLastSeen());

    assertEquals("Unexpected is-alive state on node: " + nodeDeadNow, false, nodeDeadNow.isAlive());
    assertEquals("Unexpected last-seen state on node: " + nodeDeadNow, alive1Instant,
        nodeDeadNow.getLastSeen());

    assertEquals("Unexpected is-alive state on node: " + nodeAlwaysDead, false,
        nodeAlwaysDead.isAlive());
    assertNull("Unexpected last-seen state on node: " + nodeAlwaysDead,
        nodeAlwaysDead.getLastSeen());

    assertEquals("Unexpected is-alive state on node: " + nodeAliveNow, true,
        nodeAliveNow.isAlive());
    assertEquals("Unexpected last-seen state on node: " + nodeAliveNow, alive2Instant,
        nodeAliveNow.getLastSeen());
  }

  @Test
  public void assert_marshalling_roundtrips_are_fine() {
    // ARRANGE
    final HeartbeatVO heartbeat = new HeartbeatVO(now(), true);
    final NetworkNodeStatusVO livingNode = createNetworkNodeVO(true);
    final NetworkNodeStatusVO deadNode = createNetworkNodeVO(false);
    final NetworkNodeStatusListVO listVO = createNetworkNodeListVO(livingNode, deadNode);
    final ServiceDAO serviceDAO = new ServiceDAO();

    // ACT
    final HeartbeatVO heartbeat2 = fromString(toString(heartbeat, serviceDAO), serviceDAO,
        HeartbeatVO.class);
    final NetworkNodeStatusVO livingNode2 = fromString(toString(livingNode, serviceDAO), serviceDAO,
        NetworkNodeStatusVO.class);
    final NetworkNodeStatusVO deadNode2 = fromString(toString(deadNode, serviceDAO), serviceDAO,
        NetworkNodeStatusVO.class);
    final NetworkNodeStatusListVO listVO2 = fromString(toString(listVO, serviceDAO), serviceDAO,
        NetworkNodeStatusListVO.class);

    // ASSERT
    assertEquals("Unequal object after roundtrip", heartbeat, heartbeat2);
    assertEquals("Unequal object after roundtrip", livingNode, livingNode2);
    assertEquals("Unequal object after roundtrip", deadNode, deadNode2);
    assertEquals("Unequal object after roundtrip", listVO, listVO2);
  }

}