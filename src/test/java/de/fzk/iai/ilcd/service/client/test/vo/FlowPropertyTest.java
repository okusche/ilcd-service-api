/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowPropertyDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.flowproperty.UnitGroupType;
import de.fzk.iai.ilcd.service.model.IFlowPropertyVO;
import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;

public class FlowPropertyTest extends de.fzk.iai.ilcd.service.client.test.vo.VOTest {
	private ObjectFactory of = null;

	@Before
	public void setup() {
		this.of = new ObjectFactory();
	}

	@Test
	public void simpleFlowPropertyMarshallingTest() throws Exception {
		FlowPropertyDataSetVO dataset = of.flowPropertyDataSetVO();
		dataset.setUuid("00000000-0000-0000-0000-000000000000");
		dataset.setName("Foo flow property");
		// dataset.getName().add(new LString("Foo flow property"));
		dataset.setPermanentUri("http://db.ilcd-network.org/data/flowproperties/flowpropertytest");
		dataset.setGeneralComment("foo with the flow property");

		dataset.setDataSetVersion("01.00.000");

		dataset.getSynonyms().setValue("this, that");
		dataset.getSynonyms().setValue("de", "dies, das");
		// dataset.getSynonyms().add(new LString("this"));
		// dataset.getSynonyms().add(new LString("that"));

		UnitGroupType unitGroupDetails = new UnitGroupType();
		unitGroupDetails.setName("Units of Mass");
		unitGroupDetails.setDefaultUnit("kg");
		unitGroupDetails.setReference(new GlobalReferenceType(GlobalReferenceTypeValue.UNIT_GROUP_DATA_SET,
				"00000000-0000-0000-0000-000000000000", "reference unit group"));

		dataset.setUnitGroupDetails(unitGroupDetails);

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset,
				new FileOutputStream(new File(PATH_PREFIX_OUT + "service_api_single_flowproperty_dataset.xml")));

	}

	@Test
	@SuppressWarnings("unused")
	public void simpleUnmarshallingTest() throws FileNotFoundException, JAXBException {

		FileInputStream stream = new FileInputStream(new File(PATH_PREFIX
				+ "service_api_single_flowproperty_dataset.xml"));

		DatasetVODAO dao = new DatasetVODAO();

		IFlowPropertyVO dataset = (IFlowPropertyVO) dao.unmarshal(stream);

	}

}
