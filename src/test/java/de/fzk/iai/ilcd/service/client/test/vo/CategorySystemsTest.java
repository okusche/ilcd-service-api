package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.CategorySystemList;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.CategorySystem;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;

public class CategorySystemsTest extends VOTest {

	@Test
	public void marshallingTest() throws FileNotFoundException, JAXBException {

		CategorySystemList cats = new CategorySystemList();
		 
		CategorySystem cs1 = new CategorySystem();
		cs1.setName("ILCD");
		cs1.setReferenceToSource(new GlobalReferenceType( GlobalReferenceTypeValue.SOURCE_DATA_SET, "0000", "JRC"));

		CategorySystem cs2 = new CategorySystem();
		cs2.setName("OEKOBAU.DAT");
		cs2.setReferenceToSource(new GlobalReferenceType( GlobalReferenceTypeValue.SOURCE_DATA_SET, "DDDD", "BBSR"));

		cats.getCategorySystems().add(cs1);
		cats.getCategorySystems().add(cs2);
		
		ServiceDAO dao = new ServiceDAO();
		dao.setRenderSchemaLocation(false);
		dao.marshal(cats, System.out);
		dao.marshal(cats, new FileOutputStream(new File(TEST_FILENAME_OUT)));

	}

	@Test
	public void unmarshallingTest() throws FileNotFoundException, JAXBException {

		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME));

		ServiceDAO dao = new ServiceDAO();

		CategorySystemList csList = (CategorySystemList) dao.unmarshal(stream);

		assertEquals(csList.getCategorySystems().get(0).getName(), "ILCD");
		assertEquals(csList.getCategorySystems().get(0).getReferenceToSource().getShortDescription().getDefaultValue(), "JRC");
		assertEquals(csList.getCategorySystems().get(0).getReferenceToSource().getRefObjectId(), "0000");
		assertEquals(csList.getCategorySystems().get(0).getReferenceToSource().getType(), GlobalReferenceTypeValue.SOURCE_DATA_SET);

		
		assertEquals(csList.getCategorySystems().get(1).getName(), "OEKOBAU.DAT");
		assertEquals(csList.getCategorySystems().get(1).getReferenceToSource().getShortDescription().getDefaultValue(), "BBSR");
		assertEquals(csList.getCategorySystems().get(1).getReferenceToSource().getRefObjectId(), "DDDD");
		assertEquals(csList.getCategorySystems().get(1).getReferenceToSource().getType(), GlobalReferenceTypeValue.SOURCE_DATA_SET);
	}

	public static final String TEST_FILENAME = PATH_PREFIX + "service_api_categorysystems.xml";

	public static final String TEST_FILENAME_OUT = PATH_PREFIX_OUT + "service_api_categorysystems.xml";
}
