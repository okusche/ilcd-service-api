/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ContactDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassificationType;
import de.fzk.iai.ilcd.service.model.IContactVO;

public class ContactTest extends VOTest {
	private ObjectFactory of = null;

	@Before
	public void setup() {
		this.of = new ObjectFactory();
	}

	@Test
	public void simpleContactMarshallingTest() throws Exception {
		ContactDataSetVO dataset = of.contactDataSetVO();

		dataset.setSourceId("SIRIM");
		dataset.setUuid("00000000-0000-0000-0000-000000000000");
		dataset.setName("Interstellar Center of Excellence");
		// dataset.getName().add(new
		// LString("Interstellar Center of Excellence"));
		dataset.setShortName("ICE");
		dataset.setPermanentUri("http://db.ilcd-network.org/data/contacts/contacttest");
		dataset.setGeneralComment("nuff said");

		dataset.setDataSetVersion("01.00.000");

		ClassificationType classification = new ClassificationType();
		classification.setName( "ILCD" );
		classification.add( new ClassType( 0, "Organizations"));
		classification.add( new ClassType( 1, "Most important"));
		dataset.setClassification( classification );

		dataset.setShortName("KIT");
		dataset.setEmail("devon@kit.edu");
		dataset.setFax("+497247");
		dataset.setPhone("+497247");
		dataset.setWww("www.kit.edu");
		dataset.setCentralContactPoint("Zentrale");

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(PATH_PREFIX_OUT + "service_api_single_contact_dataset.xml")));

	}

	@Test
	@SuppressWarnings("unused")
	public void simpleUnmarshallingTest() throws JAXBException, FileNotFoundException {

		FileInputStream stream = new FileInputStream(new File(PATH_PREFIX + "service_api_single_contact_dataset.xml"));

		DatasetVODAO dao = new DatasetVODAO();

		IContactVO dataset = (IContactVO) dao.unmarshal(stream);

	}

}
