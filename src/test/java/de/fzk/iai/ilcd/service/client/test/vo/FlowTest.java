/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.CategoryType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.flow.FlowCategorizationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.flow.ReferenceFlowPropertyType;
import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfFlowValue;

public class FlowTest extends de.fzk.iai.ilcd.service.client.test.vo.VOTest {
	private ObjectFactory of = null;

	@Before
	public void setup() {
		this.of = new ObjectFactory();
	}

	@Test
	public void simpleFlowMarshallingTest() throws Exception {
		FlowDataSetVO dataset = of.flowDataSetVO();
		dataset.setUuid("00000000-0000-0000-0000-000000000000");
		dataset.setName("Foo flow");
		// dataset.getName().add(new LString("Foo flow"));
		dataset.setPermanentUri("http://db.ilcd-network.org/data/flows/flowtest");
		dataset.setGeneralComment("foo with the flow");

		FlowCategorizationType flowCat = new FlowCategorizationType(new CategoryType(0, "Emissions"));
		flowCat.add(new CategoryType(1, "Emissions to water"));
		flowCat.add(new CategoryType(2, "Emissions to cold water"));

		dataset.setFlowCategorization(flowCat);

		dataset.setType(TypeOfFlowValue.ELEMENTARY_FLOW);

		dataset.setDataSetVersion("01.00.000");

		dataset.getSynonyms().setValue("this, that");
		dataset.getSynonyms().setValue("de", "dies, das");
		// dataset.getSynonyms().add(new LString("this"));
		// dataset.getSynonyms().add(new LString("that"));

		dataset.setCasNumber("000404-86-4");
		dataset.setSumFormula("H2SO4");

		ReferenceFlowPropertyType flowPropRef = new ReferenceFlowPropertyType();
		flowPropRef.setName("Mass");
		flowPropRef.setDefaultUnit("kg");
		flowPropRef.setHref("http://....");

		flowPropRef.setReference(new GlobalReferenceType(GlobalReferenceTypeValue.FLOW_PROPERTY_DATA_SET,
				"00000000-0000-0000-0000-000000000000", "reference flow property"));

		dataset.setReferenceFlowProperty(flowPropRef);

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(PATH_PREFIX_OUT + "service_api_single_flow_dataset.xml")));

	}

	@Test
	public void simpleUnmarshallingTest() throws Exception {

		FileInputStream stream = new FileInputStream(new File(PATH_PREFIX + "service_api_single_flow_dataset.xml"));

		DatasetVODAO dao = new DatasetVODAO();

		FlowDataSetVO dataset = (FlowDataSetVO) dao.unmarshal(stream);

		assertEquals(dataset.getFlowCategorization().getCategory(0), "Emissions");
		assertEquals(dataset.getFlowCategorization().getCategory(1), "Emissions to water");
		assertEquals(dataset.getFlowCategorization().getCategory(2), "Emissions to cold water");

	}

}
