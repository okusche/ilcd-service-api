/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.SourceDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.model.ISourceVO;
import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;
import de.fzk.iai.ilcd.service.model.enums.PublicationTypeValue;

public class SourceTest extends de.fzk.iai.ilcd.service.client.test.vo.VOTest {
	private ObjectFactory of = null;

	@Before
	public void setup() {
		this.of = new ObjectFactory();
	}

	@Test
	public void simpleSourceMarshallingTest() throws Exception {
		SourceDataSetVO dataset = of.sourceDataSetVO();
		dataset.setUuid("00000000-0000-0000-0000-000000000000");
		dataset.setName("My KIT");
		dataset.setName("de", "Mein KIT");
		// dataset.getName().add(new LString("Mein KIT"));
		dataset.setPermanentUri("http://db.ilcd-network.org/data/sources/sourcetest");
		dataset.setGeneralComment("nuff said");

		dataset.setDataSetVersion("01.00.000");

		dataset.setCitation("zitat");
		GlobalReferenceType fileRef = new GlobalReferenceType(GlobalReferenceTypeValue.OTHER_EXTERNAL_FILE, null, null,
				"http://acme.org/foo.pdf");
		dataset.addFileReference(fileRef);
		// dataset.getFile().setHref("http://....");
		dataset.addBelongsTo(new GlobalReferenceType(GlobalReferenceTypeValue.CONTACT_DATA_SET,
				"00000000-0000-0000-0000-000000000000", "Autor"));
		dataset.addBelongsTo(new GlobalReferenceType(GlobalReferenceTypeValue.CONTACT_DATA_SET,
				"11111111-0000-0000-0000-000000000000", "Someone"));
		dataset.setPublicationType(PublicationTypeValue.MONOGRAPH);

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(PATH_PREFIX_OUT + "service_api_single_source_dataset.xml")));

	}

	@Test
	@SuppressWarnings("unused")
	public void simpleUnmarshallingTest() throws Exception {

		FileInputStream stream = new FileInputStream(new File(PATH_PREFIX + "service_api_single_source_dataset.xml"));

		DatasetVODAO dao = new DatasetVODAO();

		ISourceVO dataset = (ISourceVO) dao.unmarshal(stream);

	}

}
