/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.xml.bind.JAXBException;

import de.fzk.iai.ilcd.service.model.process.IReferenceFlow;
import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.epd.ProcessSubType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassificationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.AccessInformationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ComplianceSystemType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.DataQualityIndicatorType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.DataQualityIndicatorsType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.LCIMethodInformationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ReferenceFlowType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ReviewType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ReviewType.Scope;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ReviewType.Scope.Method;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.TimeInformationType;
import de.fzk.iai.ilcd.service.model.IProcessListVO;
import de.fzk.iai.ilcd.service.model.enums.CompletenessValue;
import de.fzk.iai.ilcd.service.model.enums.ComplianceValue;
import de.fzk.iai.ilcd.service.model.enums.DataQualityIndicatorName;
import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;
import de.fzk.iai.ilcd.service.model.enums.LCIMethodApproachesValue;
import de.fzk.iai.ilcd.service.model.enums.LCIMethodPrincipleValue;
import de.fzk.iai.ilcd.service.model.enums.LicenseTypeValue;
import de.fzk.iai.ilcd.service.model.enums.MethodOfReviewValue;
import de.fzk.iai.ilcd.service.model.enums.QualityValue;
import de.fzk.iai.ilcd.service.model.enums.ScopeOfReviewValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfProcessValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfReviewValue;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;
import de.fzk.iai.ilcd.service.model.process.IReview;

public class ProcessTest extends VOTest {

	private ObjectFactory of = null;

	@Before
	public void setup() {
		this.of = new ObjectFactory();
	}

	@Test
	public void simpleProcessMarshallingTest() throws Exception {
		ProcessDataSetVO dataset = of.processDataSetVO();
		populateDataset( dataset );

		dataset.getSubType();
		
		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(TEST_FILENAME_SIMPLE_OUT)));

	}

	@Test
	public void simpleProcessWithReferenceFlowsMarshallingTest() throws Exception {
		ProcessDataSetVO dataset = of.processDataSetVO();
		populateDataset( dataset );

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(TEST_FILENAME_SIMPLE_OUT)));
	}

	@Test
	public void extensionsProcessMarshallingTest() throws Exception {
		ProcessDataSetVO dataset = of.processDataSetVO();
		populateDataset( dataset );
		
		dataset.setSubType( ProcessSubType.AVERAGE_DATASET );

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(TEST_FILENAME_EXTENSIONS_OUT)));

	}

	@Test
	public void extensionsProcessUnmarshallingTest() throws Exception {
		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME_EXTENSIONS));

		DatasetVODAO dao = new DatasetVODAO();

		ProcessDataSetVO dataset = (ProcessDataSetVO) dao.unmarshal(stream);

		assertTrue(dataset.isAccessRestricted());

		assertEquals(dataset.getUuidAsString(), UUID);

		assertEquals(dataset.getDataSetVersion(), DATASET_VERSION);
		assertEquals(dataset.getSubType(), ProcessSubType.AVERAGE_DATASET);
	}
	
	@Test
	public void extensionsProcessListMarshallingTest() throws Exception {
		DataSetList processList = of.dataSetList();

		ProcessDataSetVO dataset1 = of.processDataSetVO();
		populateDataset( dataset1 );
		dataset1.setSubType( ProcessSubType.AVERAGE_DATASET );
		
		ProcessDataSetVO dataset2 = of.processDataSetVO();
		populateDataset( dataset2 );
		dataset2.setSubType( ProcessSubType.GENERIC_DATASET );

		processList.getDataSet().add(dataset1);
		processList.getDataSet().add(dataset2);

		ReferenceFlowType flow1 = new ReferenceFlowType();
		flow1.setFlowName("butter, 80%");
		flow1.setReference(new GlobalReferenceType(GlobalReferenceTypeValue.FLOW_DATA_SET, "00000000-1111-0000-0000-000000000000", "butter, 80%"));

		ReferenceFlowType flow2 = new ReferenceFlowType();
		flow2.setFlowName("water");
		flow2.setReference(new GlobalReferenceType(GlobalReferenceTypeValue.FLOW_DATA_SET, "00000000-2222-0000-0000-000000000000", "water"));

		dataset1.getReferenceFlows().add(flow1);
		dataset1.getReferenceFlows().add(flow2);

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(processList, System.out);
		dao.marshal(processList, new FileOutputStream(new File(TEST_FILENAME_LIST_EXTENSIONS_OUT)));

	}

	@Test
	public void extensionsProcessListUnmarshallingTest() throws Exception {
		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME_LIST_EXTENSIONS));

		DatasetVODAO dao = new DatasetVODAO();

		DataSetList processList = (DataSetList) dao.unmarshal(stream);

		assertTrue(((IProcessListVO) processList.getDataSet().get(0)).isAccessRestricted());

		assertEquals(((IProcessListVO) processList.getDataSet().get(0)).getUuidAsString(), UUID);

		assertEquals(((IProcessListVO) processList.getDataSet().get(0)).getDataSetVersion(), DATASET_VERSION);
		assertEquals(((IProcessListVO) processList.getDataSet().get(0)).getSubType(), ProcessSubType.AVERAGE_DATASET);

		assertTrue(((IProcessListVO) processList.getDataSet().get(1)).isAccessRestricted());

		assertEquals(((IProcessListVO) processList.getDataSet().get(1)).getUuidAsString(), UUID);

		assertEquals(((IProcessListVO) processList.getDataSet().get(1)).getDataSetVersion(), DATASET_VERSION);
		assertEquals(((IProcessListVO) processList.getDataSet().get(1)).getSubType(), ProcessSubType.GENERIC_DATASET);

	}

	private void populateDataset( ProcessDataSetVO dataset ) {
		dataset.setAccessRestricted(true);
		dataset.setUuid(ProcessTest.UUID);
		dataset.setName(NAME);
		// dataset.getName().add(new LString(NAME));
		dataset.setPermanentUri(PERMANENT_URI);
		dataset.setGeneralComment(GENERAL_COMMENT);

		ClassificationType classification3 = new ClassificationType();
		classification3.setName( "ILCD" );
		classification3.add(new ClassType(0, "FOOBAR"));
		classification3.add(new ClassType(1, "BARFOO"));
		dataset.getClassifications().add(classification3);

		ClassificationType classification = new ClassificationType();
		classification.setName( "ILCD" );
		classification.add(new ClassType(0, CLASSIFICATION_1));
		classification.add(new ClassType(1, CLASSIFICATION_2));
		dataset.setClassification(classification);

		ClassificationType classification2 = new ClassificationType();
		classification2.setName( "other" );
		classification2.add(new ClassType(0, "foo"));
		classification2.add(new ClassType(1, "bar"));
		dataset.getClassifications().add(classification2);


		
		dataset.getSynonyms().setValue(SYNONYMS_EN);
		dataset.getSynonyms().setValue("de", SYNONYMS_DE);

		dataset.setType(TypeOfProcessValue.UNIT_PROCESS_SINGLE_OPERATION);

		dataset.setParameterized(PARAMETERIZED);

		dataset.getUseAdvice().setValue(USE_ADVICE);
		AccessInformationType accessInformation = new AccessInformationType();
		accessInformation.setCopyright(false);
		accessInformation.setLicenseType(LicenseTypeValue.fromValue(LICENSE_TYPE));
		accessInformation.setUseRestrictions(USE_RESTRICTIONS);
		dataset.setAccessInformation(accessInformation);

		dataset.setDataSetVersion(DATASET_VERSION);
		dataset.setLocation(LOCATION);

		TimeInformationType time = new TimeInformationType();
		time.setValidUntil(DATASET_VALID_UNTIL);
		time.setReferenceYear(REFERENCE_YEAR);
		dataset.setTimeInformation(time);

		dataset.setFormat("ILCD 1.0");
		dataset.setOwnerReference(new GlobalReferenceType(GlobalReferenceTypeValue.CONTACT_DATA_SET,
				"00000000-0000-0000-0000-000000000000", "JRC"));

		dataset.setOverallQuality("FOOOO");

		dataset.setResults(true);

		LCIMethodInformationType lciMethodInfo = new LCIMethodInformationType();
		lciMethodInfo.setMethodPrinciple(LCIMethodPrincipleValue.fromValue(LCI_METHOD_PRINCIPLE));

		lciMethodInfo.getApproaches().add(LCIMethodApproachesValue.fromValue(LCI_METHOD_APPROACHES_1));
		lciMethodInfo.getApproaches().add(LCIMethodApproachesValue.fromValue(LCI_METHOD_APPROACHES_2));

		dataset.setLCIMethodInformation(lciMethodInfo);

		dataset.setCompletenessProductModel(CompletenessValue.fromValue(COMPLETENESS));

		accessInformation = new AccessInformationType();
		accessInformation.setCopyright(true);
		accessInformation.setLicenseType(LicenseTypeValue.fromValue(LICENSE_TYPE));
		accessInformation.setUseRestrictions(USE_RESTRICTIONS);
		dataset.setAccessInformation(accessInformation);
		ComplianceSystemType compliance = new ComplianceSystemType();
		compliance.setName(COMPLIANCE_ILCD_ENTRY_LEVEL);
		compliance.setOverallCompliance(ComplianceValue.FULLY_COMPLIANT);
		dataset.addComplianceSystem(compliance);

		ComplianceSystemType efCompliance = new ComplianceSystemType();
		efCompliance.setName(COMPLIANCE_ENVIRONMENTAL_FOOTPRINT_3_0);
		efCompliance.setOverallCompliance(ComplianceValue.FULLY_COMPLIANT);
		dataset.addComplianceSystem(efCompliance);

		ComplianceSystemType aCompliance = new ComplianceSystemType();
		aCompliance.setName(COMPLIANCE_ANOTHER);
		aCompliance.setOverallCompliance(ComplianceValue.FULLY_COMPLIANT);
		dataset.addComplianceSystem(aCompliance);

		ReferenceFlowType refFlow = new ReferenceFlowType();
		// refFlow.setHref("http://....");
		refFlow.setReference(new GlobalReferenceType(GlobalReferenceTypeValue.FLOW_DATA_SET,
				"00000000-0000-0000-0000-000000000000", "foo flow"));
		refFlow.setFlowName(REFERENCE_FLOW_NAME);

		dataset.getQuantitativeReference().getReferenceFlow().add(refFlow);

		dataset.getQuantitativeReference().getFunctionalUnit().setValue(FUNCTIONAL_UNIT);

		ReviewType review = new ReviewType();
		review.getReviewDetails().setValue(REVIEW_DETAILS);
		review.setType(TypeOfReviewValue.fromValue(REVIEW_TYPE));
		Scope scope = new ReviewType.Scope();
		scope.setName(ScopeOfReviewValue.LCI_RESULTS_OR_PARTLY_TERMINATED_SYSTEM);
		Method method = new ReviewType.Scope.Method();
		method.setName(MethodOfReviewValue.ELEMENT_BALANCE);
		scope.getMethod().add(method);
		review.getScopes().add(scope);
		DataQualityIndicatorsType dqis = new DataQualityIndicatorsType();
		DataQualityIndicatorType dqi = new DataQualityIndicatorType();
		dqi.setName(DataQualityIndicatorName.COMPLETENESS);
		dqi.setValue(QualityValue.GOOD);
		dqis.getDataQualityIndicator().add(dqi);
		review.setDataQualityIndicators(dqis);
		dataset.addReview(review);
	}

	@Test
	public void simpleUnmarshallingTest() throws FileNotFoundException, JAXBException {

		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME_SIMPLE));

		DatasetVODAO dao = new DatasetVODAO();

		ProcessDataSetVO dataset = (ProcessDataSetVO) dao.unmarshal(stream);

		assertTrue(dataset.isAccessRestricted());

		assertEquals(dataset.getUuidAsString(), UUID);

		assertEquals(dataset.getDataSetVersion(), DATASET_VERSION);
		assertEquals(dataset.getPermanentUri(), PERMANENT_URI);
		assertEquals(dataset.getDefaultName(), NAME);
		assertEquals(dataset.getDescription().getValue(), GENERAL_COMMENT);
		assertEquals(dataset.getSynonyms().getValue(), SYNONYMS_EN);
		assertEquals(dataset.getSynonyms().getLStrings().size(), 2);
		assertEquals(dataset.getUseAdvice().getValue(), USE_ADVICE);
		assertEquals(dataset.getAccessInformation().getUseRestrictions().getValue(), USE_RESTRICTIONS);
	
		assertEquals(dataset.getClassification().getClass( 0 ).getName(), CLASSIFICATION_1);
		assertEquals(dataset.getClassification().getClass( 1 ).getName(), CLASSIFICATION_2);
		
		assertEquals(dataset.getClassification().size(), 2);
		assertEquals(dataset.getLocation(), LOCATION);
		assertEquals(dataset.getTimeInformation().getValidUntil(), DATASET_VALID_UNTIL);
		assertEquals(dataset.getTimeInformation().getReferenceYear(), REFERENCE_YEAR);
		assertEquals(dataset.getQuantitativeReference().getFunctionalUnit().getValue(), FUNCTIONAL_UNIT);
		assertEquals(dataset.getParameterized(), PARAMETERIZED);
		assertEquals(dataset.getHasResults(), HAS_RESULTS);
		assertEquals(dataset.getOverallQuality(), "FOOOO");
		for (IReview review : dataset.getReviews()) {
			assertEquals(review.getType(), TypeOfReviewValue.fromValue(REVIEW_TYPE));
			assertEquals(review.getReviewDetails().getValue(), REVIEW_DETAILS);
		}
		assertEquals(dataset.getLCIMethodInformation().getApproaches().size(), LCI_METHOD_APPROACHES_COUNT);
		assertTrue(dataset.getLCIMethodInformation().getApproaches()
				.contains(LCIMethodApproachesValue.fromValue(LCI_METHOD_APPROACHES_1)));
		assertTrue(dataset.getLCIMethodInformation().getApproaches()
				.contains(LCIMethodApproachesValue.fromValue(LCI_METHOD_APPROACHES_2)));
		assertEquals(dataset.getCompletenessProductModel(), CompletenessValue.fromValue(COMPLETENESS));
		assertEquals(dataset.getAccessInformation().getLicenseType(), LicenseTypeValue.fromValue(LICENSE_TYPE));
		assertEquals(dataset.getLCIMethodInformation().getMethodPrinciple(),
				LCIMethodPrincipleValue.fromValue(LCI_METHOD_PRINCIPLE));

		assertEquals(dataset.getQuantitativeReference().getReferenceFlow().get(0).getFlowName().getValue(),
				REFERENCE_FLOW_NAME);
		
		assertEquals(3, dataset.getComplianceSystems().size());
		
		// make sure the correct order isn't a coincidence
		for (int i=0; i<10; i++) {
			List<IComplianceSystem> cl = new ArrayList<IComplianceSystem>(dataset.getComplianceSystems());
			assertEquals(COMPLIANCE_ANOTHER, cl.get(0).getName());
			assertEquals(COMPLIANCE_ENVIRONMENTAL_FOOTPRINT_3_0, cl.get(1).getName());
			assertEquals(COMPLIANCE_ILCD_ENTRY_LEVEL, cl.get(2).getName());
			stream = new FileInputStream(new File(TEST_FILENAME_SIMPLE));
			dataset = (ProcessDataSetVO) dao.unmarshal(stream);
		}
	
	}
	
	@Test
	public void processWithDupesMarshallingTest() throws Exception {
		ProcessDataSetVO dataset = of.processDataSetVO();
		populateDataset( dataset );

		ProcessDataSetVO dataset1 = of.processDataSetVO();
		populateDataset( dataset1 );
		dataset1.setDataSetVersion("00.99.000");

		ProcessDataSetVO dataset2 = of.processDataSetVO();
		populateDataset( dataset2 );
		dataset2.setSourceId("FOO.BAR");
		
		dataset.getDuplicates().add(dataset1);
		dataset.getDuplicates().add(dataset2);
		
		dataset.getSubType();
		
		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(TEST_FILENAME_SIMPLE_OUT)));

	}

	public static final String TEST_FILENAME_SIMPLE = PATH_PREFIX + "service_api_single_process_dataset.xml";

	public static final String TEST_FILENAME_EXTENSIONS = PATH_PREFIX + "service_api_single_process_dataset_with_extensions.xml";

	public static final String TEST_FILENAME_LIST_EXTENSIONS = PATH_PREFIX + "service_api_process_list_with_extensions.xml";

	public static final String TEST_FILENAME_DUPES = PATH_PREFIX + "service_api_single_process_dataset_with_dupes.xml";

	public static final String TEST_FILENAME_SIMPLE_OUT = PATH_PREFIX_OUT + "service_api_single_process_dataset.xml";

	public static final String TEST_FILENAME_EXTENSIONS_OUT = PATH_PREFIX_OUT + "service_api_single_process_dataset_with_extensions.xml";

	public static final String TEST_FILENAME_LIST_EXTENSIONS_OUT = PATH_PREFIX_OUT + "service_api_process_list_with_extensions.xml";

	public static final String TEST_FILENAME_DUPES_OUT = PATH_PREFIX_OUT + "service_api_single_process_dataset_with_dupes.xml";

	public static final String UUID = "00000000-0000-0000-0000-000000000000";

	public static final String DATASET_VERSION = "01.00.000";

	public static final String PERMANENT_URI = "http://db.ilcd-network.org/data/processes/processtest";

	public static final String NAME = "Foo unit process";

	public static final String GENERAL_COMMENT = "foo bar";

	public static final String SYNONYMS_EN = "Foobar";

	public static final String SYNONYMS_DE = "Fubar";

	public static final String USE_ADVICE = "hear my advice";

	public static final String USE_RESTRICTIONS = "Rated R";

	public static final String CLASSIFICATION_1 = "Energy systems";

	public static final String CLASSIFICATION_2 = "Foo energy systems";

	public static final String LOCATION = "RER";

	public static final Integer DATASET_VALID_UNTIL = 2012;

	public static final Integer REFERENCE_YEAR = 2009;

	public static final String FUNCTIONAL_UNIT = "Foonctional Unit";

	public static final String REVIEW_TYPE = TypeOfReviewValue.INDEPENDENT_EXTERNAL_REVIEW.getValue();

	public static final String REVIEW_DETAILS = "details here";

	public static final boolean PARAMETERIZED = true;

	public static final String LCI_METHOD_APPROACHES_1 = LCIMethodApproachesValue.ALLOCATION_GROSS_CALORIFIC_VALUE
			.getValue();

	public static final String LCI_METHOD_APPROACHES_2 = LCIMethodApproachesValue.ALLOCATION_ELEMENT_CONTENT.getValue();

	public static final int LCI_METHOD_APPROACHES_COUNT = 2;

	public static final String COMPLETENESS = CompletenessValue.ALL_RELEVANT_FLOWS_QUANTIFIED.getValue();

	public static final String LICENSE_TYPE = LicenseTypeValue.FREE_OF_CHARGE_FOR_ALL_USERS_AND_USES.getValue();

	public static final String LCI_METHOD_PRINCIPLE = LCIMethodPrincipleValue.ATTRIBUTIONAL.getValue();

	public static final String REFERENCE_FLOW_NAME = "electricity mix";

	public static final boolean HAS_RESULTS = true;

	public static final String COMPLIANCE_ANOTHER = "Another Compliance";

	public static final String COMPLIANCE_ENVIRONMENTAL_FOOTPRINT_3_0 = "Environmental Footprint 3.0 Compliance";

	public static final String COMPLIANCE_ILCD_ENTRY_LEVEL = "ILCD Compliance - entry level";

}
