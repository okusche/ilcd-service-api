/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.CategoryList;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassType;

public class CategoriesTest extends VOTest {

	@Test
	public void marshallingTest() throws FileNotFoundException, JAXBException {

		CategoryList cats = new CategoryList();
		
		cats.addCategory(new ClassType(0, "FOO" ));
		cats.addCategory(new ClassType(0,  "BAR") );
		cats.addCategory(new ClassType(0,  "FOOBAR" , "007.001"));
				
		
		ServiceDAO dao = new ServiceDAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(cats, System.out);
		dao.marshal(cats, new FileOutputStream(new File(TEST_FILENAME_OUT)));

	}

//	@Test
//	public void unmarshallingTest() throws FileNotFoundException, JAXBException {
//
//		FileInputStream stream = new FileInputStream(new File(TEST_FILENAME));
//
//		ServiceDAO dao = new ServiceDAO();
//
//		INodeInfo nodeInfo = (INodeInfo) dao.unmarshal(stream);
//
//		assertEquals(nodeInfo.getBaseURL(), BASE_URL);
//		assertEquals(nodeInfo.getNodeID(), NODE_ID);
//		assertEquals(nodeInfo.getName(), NODE_NAME);
//		assertEquals(nodeInfo.getOperator(), OPERATOR);
//		assertEquals(nodeInfo.getDescription().getValue(), DESCRIPTION);
//		assertEquals(nodeInfo.getDescription().getValue("de"), DESCRIPTION_DE);
//		assertEquals(nodeInfo.getAdminName(), ADMIN_NAME);
//		assertEquals(nodeInfo.getAdminEMail(), ADMIN_EMAIL);
//		assertEquals(nodeInfo.getAdminPhone(), ADMIN_PHONE);
//		assertEquals(nodeInfo.getAdminWWW(), ADMIN_WWW);
//
//	}

	public static final String TEST_FILENAME = PATH_PREFIX + "service_api_categories.xml";

	public static final String TEST_FILENAME_OUT = PATH_PREFIX_OUT + "service_api_categories.xml";

}
