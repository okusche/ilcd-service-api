/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model.common;

import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;

/**
 * The Interface IGlobalReference.
 * 
 * This represents a reference to another object, identified by a UUID and,
 * optionally, version number.
 */
public interface IGlobalReference {

	/**
	 * Gets the short description.
	 * 
	 * @return the short description
	 */
	public abstract IMultiLangString getShortDescription();

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public abstract GlobalReferenceTypeValue getType();

	/**
	 * Sets the type.
	 * 
	 * @param value
	 *            the new type
	 */
	public abstract void setType(GlobalReferenceTypeValue value);

	/**
	 * Gets the UUID of the referenced object.
	 * 
	 * @return the UUID
	 */
	public abstract String getRefObjectId();

	/**
	 * Sets the UUID of the referenced object.
	 * 
	 * @param value
	 *            the new UUID
	 */
	public abstract void setRefObjectId(String value);

	/**
	 * Gets the version as string.
	 * 
	 * @return the version as string
	 */
	public abstract String getVersionAsString();

	/**
	 * Sets the version.
	 * 
	 * @param value
	 *            the new version
	 */
	public abstract void setVersion(String value);

	/**
	 * Gets the URI of the referenced object.
	 * 
	 * @return the URI
	 */
	public abstract String getUri();

	/**
	 * Sets the uri.
	 * 
	 * @param value
	 *            the new uri
	 */
	public abstract void setUri(String value);

	/**
	 * Gets the URI of the referenced object set by the providing database node.
	 * This URI, if present, is guaranteed to be absolute.
	 * 
	 * @return the href
	 */
	public abstract String getHref();

}
