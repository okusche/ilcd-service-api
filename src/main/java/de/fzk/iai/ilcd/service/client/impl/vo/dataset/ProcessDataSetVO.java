/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl.vo.dataset;

import java.util.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.Constants;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.*;
import de.fzk.iai.ilcd.service.model.process.IReferenceFlow;
import org.w3c.dom.Node;

import de.fzk.iai.ilcd.service.client.impl.vo.epd.ProcessSubType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.IProcessVO;
import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.common.ILString;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.CompletenessValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfProcessValue;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;
import de.fzk.iai.ilcd.service.model.process.IReview;

@XmlRootElement(name = "process", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "synonyms", "type", "quantitativeReference", "referenceFlows", "location", "timeInformation", "parameterized",
		"hasResults", "containsProductModel", "lciMethodInformation", "dataSources", "completenessProductModel", "complianceSystems",
		"reviews", "overallQuality", "useAdvice", "technologyDescription", "technicalPurpose", "accessInformation", "format", "ownerReference",
		"approvedBy", "registrationNumber", "registrationAuthority"  })
public class ProcessDataSetVO extends DataSetVO implements IProcessVO {

	@XmlAttribute(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected Boolean accessRestricted = null;

	@XmlAttribute(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v2/Process")
	protected Boolean metaDataOnly = null;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected TypeOfProcessValue type;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected Boolean parameterized = false;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected Boolean containsProductModel = false;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected String overallQuality = null;

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected List<LString> synonyms = new ArrayList<LString>();

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected String location;

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected List<LString> useAdvice = new ArrayList<LString>();

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected List<LString> technicalPurpose = new ArrayList<LString>();

	@XmlElement( type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected List<LString> technologyDescription = new ArrayList<LString>();

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", name = "time")
	protected TimeInformationType timeInformation;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected String format;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", name = "ownership")
	protected GlobalReferenceType ownerReference;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected GlobalReferenceType approvedBy;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected LCIMethodInformationType lciMethodInformation;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected CompletenessValue completenessProductModel;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected AccessInformationType accessInformation;

	@XmlElement(name = "complianceSystem", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", type = ComplianceSystemType.class)
	protected SortedSet<IComplianceSystem> complianceSystems;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected QuantitativeReferenceType quantitativeReference;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", type = ReviewType.class, name = "review")
	protected List<IReview> reviews;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected Boolean hasResults = false;
	
	@XmlElement(namespace = Constants.NS_SERVICE_API_PROCESS_V2, type = GlobalReferenceType.class, name = "dataSource")
	protected List<IGlobalReference> dataSources;

	@XmlElement(namespace = Constants.NS_SERVICE_API_PROCESS_V2, type = GlobalReferenceType.class)
	protected IGlobalReference registrationAuthority;

	@XmlElement(namespace = Constants.NS_SERVICE_API_PROCESS_V2)
	protected String registrationNumber;

	@XmlElement(namespace = Constants.NS_SERVICE_API_PROCESS_V3, type = ReferenceFlowType.class, name = "referenceFlow")
	protected List<IReferenceFlow> referenceFlows;

	public ProcessDataSetVO() {
	}

	public IMultiLangString getBaseName() {
		IMultiLangString baseName = new MultiLangString();

		List<ILString> lStrings = this.getName().getLStrings();
		for (ILString lString : lStrings) {
			String lang = lString.getLang();
			String value = lString.getValue();
			if (value != null) {
				String basePart = null;
				if (value.contains(";"))
					basePart = value.substring(0, value.indexOf(';'));
				else
					basePart = value;
				baseName.setValue(lang, basePart);
			}
		}
		return baseName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getLocation()
	 */
	public String getLocation() {
		return location;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getParameterized()
	 */
	public Boolean getParameterized() {
		return parameterized;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getDataSetFormat()
	 */
	public String getFormat() {
		return format;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getOwnershipOfDataSet()
	 */
	public GlobalReferenceType getOwnerReference() {
		return ownerReference;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getSynonyms()
	 */
	public IMultiLangString getSynonyms() {
		if (synonyms == null) {
			synonyms = new ArrayList<LString>();
		}
		return new MultiLangString(synonyms);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getTimeInformation()
	 */
	public TimeInformationType getTimeInformation() {
		return timeInformation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getType()
	 */
	public TypeOfProcessValue getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getUseAdvice()
	 */
	public IMultiLangString getUseAdvice() {
		if (useAdvice == null) {
			useAdvice = new ArrayList<LString>();
		}
		return new MultiLangString(useAdvice);
	}
	
	public ProcessSubType getSubType() {
		
		if (this.getOther() == null || this.getOther().getAny().isEmpty())
			return null;
		
		try {
			for (Object o : this.getOther().getAny()) {
				if (o instanceof Node) {
					Node e = (Node) o;
					if ("subType".equals(e.getLocalName()) && "http://www.iai.kit.edu/EPD/2013".equals(e.getNamespaceURI())) { 
						return ProcessSubType.fromValue(e.getTextContent());
					}
				} else if (o instanceof ProcessSubType)
					return (ProcessSubType) o;
			}
		} catch (Exception e) {
		}
		
		return null;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setLocation(java.lang .String)
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setParameterized(java .lang.Boolean)
	 */
	public void setParameterized(Boolean parameterized) {
		this.parameterized = parameterized;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setDataSetFormat(de.
	 * fzk.iai.ilcd.api.vo.types.common.GlobalReferenceType)
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setOwnershipOfDataSet
	 * (de.fzk.iai.ilcd.api.vo.types.common.GlobalReferenceType)
	 */
	public void setOwnerReference(GlobalReferenceType referenceToOwnershipOfDataSet) {
		this.ownerReference = (GlobalReferenceType) referenceToOwnershipOfDataSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setSynonyms(java.util .List)
	 */
	public void setSynonyms(List<LString> synonyms) {
		this.synonyms = synonyms;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setTime(de.fzk.iai.ilcd .api.vo.types.process.TimeInformationType)
	 */
	public void setTimeInformation(TimeInformationType value) {
		this.timeInformation = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setType(de.fzk.iai.ilcd .api.vo.types.process.TypeOfProcessValue)
	 */
	public void setType(TypeOfProcessValue type) {
		this.type = type;
	}

	/*
	 * 
	 */
	public void setSubType( ProcessSubType subType ) {
		
		for ( Object o : this.getOther().getAny() ) {
			if ( o instanceof ProcessSubType ) {
				if ( subType == null )
					this.getOther().getAny().remove( o );
				else {
					o = subType;
					return;
				}
			}
		}

		if (subType != null)
			this.getOther().getAny().add( subType );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setUseAdvice(java.util .List)
	 */
	public void setUseAdvice(List<LString> useAdviceForDataSet) {
		this.useAdvice = useAdviceForDataSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getCompletenessProductModel ()
	 */
	public CompletenessValue getCompletenessProductModel() {
		return completenessProductModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setCompletenessProductModel
	 * (de.fzk.iai.ilcd.api.vo.types.process.CompletenessValue)
	 */
	public void setCompletenessProductModel(CompletenessValue completenessProductModel) {
		this.completenessProductModel = completenessProductModel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getLicenseType()
	 */
	public AccessInformationType getAccessInformation() {
		return accessInformation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setLicenseType(de.fzk
	 * .iai.ilcd.api.vo.types.process.LicenseTypeValue)
	 */
	public void setAccessInformation(AccessInformationType accessInformation) {
		this.accessInformation = accessInformation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getComplianceSystem()
	 */
	public Set<IComplianceSystem> getComplianceSystems() {
		if (this.complianceSystems == null) {
			this.complianceSystems = new TreeSet<IComplianceSystem>();
		}
		return this.complianceSystems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setComplianceSystem(
	 * de.fzk.iai.ilcd.api.vo.types.common.GlobalReferenceType)
	 */
	protected void setComplianceSystems(Set<IComplianceSystem> complianceSystems) {
		if (complianceSystems != null)
			this.complianceSystems = new TreeSet<IComplianceSystem>(complianceSystems);
		else
			this.complianceSystems = null;
	}

	public void addComplianceSystem(IComplianceSystem complianceSystem) {
		if (this.complianceSystems == null) {
			this.complianceSystems = new TreeSet<IComplianceSystem>();
		}
		this.complianceSystems.add(complianceSystem);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getQuantitativeReference ()
	 */
	public QuantitativeReferenceType getQuantitativeReference() {
		if (this.quantitativeReference == null) {
			this.quantitativeReference = new QuantitativeReferenceType();
		}
		return quantitativeReference;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setQuantitativeReference
	 * (de.fzk.iai.ilcd.api.vo.types.process.QuantitativeReferenceType)
	 */
	public void setQuantitativeReference(QuantitativeReferenceType quantitativeReference) {
		this.quantitativeReference = quantitativeReference;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getReview()
	 */
	public List<IReview> getReviews() {
		if (reviews == null) {
			reviews = new ArrayList<IReview>();
		}
		return reviews;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setReview(de.fzk.iai .ilcd.api.vo.types.process.ReviewType)
	 */
	protected void setReviews(List<IReview> reviews) {
		this.reviews = reviews;
	}

	public void addReview(ReviewType review) {
		if (reviews == null) {
			reviews = new ArrayList<IReview>();
		}
		reviews.add(review);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#getLCIMethodInformation ()
	 */
	public LCIMethodInformationType getLCIMethodInformation() {
		return lciMethodInformation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IProcessVO#setLCIMethodInformation
	 * (de.fzk.iai.ilcd.api.vo.types.process.LCIMethodInformationType)
	 */
	public void setLCIMethodInformation(LCIMethodInformationType lciMethodInformation) {
		this.lciMethodInformation = lciMethodInformation;
	}

	public GlobalReferenceType getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(GlobalReferenceType approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Boolean getHasResults() {
		return hasResults;
	}

	public void setResults(Boolean hasResults) {
		this.hasResults = hasResults;
	}

	public Boolean getContainsProductModel() {
		return containsProductModel;
	}

	public void setContainsProductModel(Boolean containsProductModel) {
		this.containsProductModel = containsProductModel;
	}

	public IMultiLangString getTechnicalPurpose() {
		if (technicalPurpose == null) {
			technicalPurpose = new ArrayList<LString>();
		}
		return new MultiLangString(technicalPurpose);
	}

	protected void setTechnicalPurpose(List<LString> technicalPurpose) {
		this.technicalPurpose = technicalPurpose;
	}

	public void setTechnicalPurpose(LString purpose) {
		if (this.technicalPurpose == null) {
			this.technicalPurpose = new ArrayList<LString>();
		}
		purpose.insertWithoutDuplicates(this.technicalPurpose);
	}

	public void setTechnicalPurpose(String lang, String value) {
		LString lString = new LString(lang, value);
		this.setTechnicalPurpose(lString);
	}

	public void setTechnicalPurpose(String purpose) {
		LString lString = new LString(purpose);
		this.setTechnicalPurpose(lString);
	}

	public IMultiLangString getTechnologyDescription() {
		if (technologyDescription == null) {
			technologyDescription = new ArrayList<LString>();
		}
		return new MultiLangString(technologyDescription);
	}

	protected void setTechnologyDescription(List<LString> technologyDescription) {
		this.technologyDescription = technologyDescription;
	}

	public void setTechnologyDescription(LString desc) {
		if (this.technologyDescription == null) {
			this.technologyDescription = new ArrayList<LString>();
		}
		desc.insertWithoutDuplicates(this.technologyDescription);
	}

	public void setTechnologyDescription(String lang, String value) {
		LString lString = new LString(lang, value);
		this.setTechnologyDescription(lString);
	}

	public void setTechnologyDescription(String technologyDescription) {
		LString lString = new LString(technologyDescription);
		this.setTechnologyDescription(lString);
	}

	/**
	 * @return the overallQuality
	 */
	public String getOverallQuality() {
		return overallQuality;
	}

	/**
	 * @param overallQuality
	 *            the overallQuality to set
	 */
	public void setOverallQuality(String overallQuality) {
		this.overallQuality = overallQuality;
	}

	/**
	 * @return the accessRestricted
	 */
	public boolean isAccessRestricted() {
		return (accessRestricted == null ? false : accessRestricted);
	}

	/**
	 * @param accessRestricted
	 *            the accessRestricted to set
	 */
	public void setAccessRestricted(boolean accessRestricted) {
		this.accessRestricted = accessRestricted;
	}

	public List<IGlobalReference> getDataSources() {
		if (this.dataSources == null)
			this.dataSources = new ArrayList<IGlobalReference>();
		return this.dataSources;
	}

	public void setDataSources(List<IGlobalReference> dataSources) {
		this.dataSources = dataSources;
	}

	public IGlobalReference getRegistrationAuthority() {
		return registrationAuthority;
	}

	public void setRegistrationAuthority(IGlobalReference registrationAuthority) {
		this.registrationAuthority = registrationAuthority;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public List<IReferenceFlow> getReferenceFlows() {
		if (this.referenceFlows == null)
			this.referenceFlows = new ArrayList<>();
		return this.referenceFlows;
	}

	public Boolean getMetaDataOnly() {
		return metaDataOnly;
	}

	public void setMetaDataOnly(Boolean metaDataOnly) {
		this.metaDataOnly = metaDataOnly;
	}

}
