package de.fzk.iai.ilcd.service.client.impl.vo.networknode;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import de.fzk.iai.ilcd.service.model.IHeartbeatVO;
import java.time.Instant;
import java.util.Comparator;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(FIELD)
@XmlRootElement(name = "heartbeat", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
@XmlType(propOrder = {"time", "alive"})
public class HeartbeatVO implements IHeartbeatVO {

  public static Comparator<HeartbeatVO> LATEST_FIRST_COMPARATOR = Comparator.comparing(
      HeartbeatVO::getTime, Comparator.nullsFirst(Comparator.naturalOrder())).reversed();

  @XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
  // JAXB needs a type adapter here. It is given in the package-info.java file within this class's package
  private Instant time;

  @XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
  private Boolean alive;

  public HeartbeatVO() {
  }

  public HeartbeatVO(Instant time, Boolean alive) {
    this.time = time;
    this.alive = alive;
  }

  public Instant getTime() {
    return time;
  }

  public void setTime(Instant time) {
    this.time = time;
  }

  public Boolean isAlive() {
    return alive;
  }

  public void setAlive(Boolean alive) {
    this.alive = alive;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    HeartbeatVO that = (HeartbeatVO) o;
    return Objects.equals(getTime(), that.getTime()) && Objects.equals(isAlive(), that.isAlive());
  }

  @Override
  public int hashCode() {
    int result = Objects.hashCode(getTime());
    result = 31 * result + Objects.hashCode(isAlive());
    return result;
  }

  @Override
  public String toString() {
    return "HeartbeatVO{" + "time=" + time + ", alive=" + alive + '}';
  }
}
