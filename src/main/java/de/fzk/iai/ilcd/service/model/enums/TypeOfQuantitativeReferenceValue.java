/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model.enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TypeOfQuantitativeReferenceValue.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="TypeOfQuantitativeReferenceValue"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Reference flow(s)"/&gt;
 *     &lt;enumeration value="Functional unit"/&gt;
 *     &lt;enumeration value="Production period"/&gt;
 *     &lt;enumeration value="Other parameter"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TypeOfQuantitativeReferenceValues")
@XmlEnum
public enum TypeOfQuantitativeReferenceValue {

	/** Quantitative Reference is specified through reference flows. */
	@XmlEnumValue("Reference flow(s)")
	ELEMENTARY_FLOW("Reference flow(s)"),

	/**
	 * Quantitative reference is specified described by a functional unit.
	 * 
	 */
	@XmlEnumValue("Functional unit")
	PRODUCT_FLOW("Functional unit"),

	/** Quantitative reference is defined by a production period. */
	@XmlEnumValue("Production period")
	WASTE_FLOW("Production period"),

	/** Other definition of reference flow. */
	@XmlEnumValue("Other parameter")
	OTHER_FLOW("Other parameter");
	
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new type of quantitative reference value.
	 *
	 * @param v the value
	 */
	TypeOfQuantitativeReferenceValue(String v) {
		value = v;
	}

	/**
	 * Value.
	 *
	 * @return the string
	 */
	public String value() {
		return value;
	}

	/**
	 * From value.
	 *
	 * @param v the value
	 * @return the type of quantitative reference value
	 */
	public static TypeOfQuantitativeReferenceValue fromValue(String v) {
		for (TypeOfQuantitativeReferenceValue c : TypeOfQuantitativeReferenceValue.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
