/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.types.flow;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.CategoryType;
import de.fzk.iai.ilcd.service.model.common.IClass;
import de.fzk.iai.ilcd.service.model.common.IClassification;

/**
 * <p>
 * Java class for FlowCategorizationType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="FlowCategorizationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="category" type="{http://lca.jrc.it/ILCD/Common}CategoryType" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://lca.jrc.it/ILCD/Common}other" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" default="ILCD" /&gt;
 *       &lt;attribute name="categories" type="{http://www.w3.org/2001/XMLSchema}anyURI" /&gt;
 *       &lt;anyAttribute processContents='lax' namespace='##other'/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlowCategorizationType", propOrder = { "categories" })
public class FlowCategorizationType implements IClassification {

	@XmlElement(type = CategoryType.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI", name = "category", required = true)
	protected List<CategoryType> categories;

	@XmlAttribute
	protected String name;

	@XmlAttribute(name = "categories")
	@XmlSchemaType(name = "anyURI")
	protected String categoriesFileRef;

	/**
	 * Convenience constructor
	 * 
	 * @param cat
	 */
	public FlowCategorizationType(CategoryType cat) {
		this.add(cat);
	}

	public FlowCategorizationType() {
	}

	/**
	 * Convenience method to add a Category without calling the getCategory()
	 * method
	 * 
	 * @param cat
	 */
	public void add(CategoryType cat) {
		this.getCategories().add(cat);
	}

	/**
	 * Remove the category that matches the level and value of the one passed as
	 * parameter
	 * 
	 * @param cat
	 */
	public void remove(CategoryType cat) {
		// int removalCandidateIndex = -1;

		this.getCategories().remove(cat);

		// why is this complicated iteration needed if theres is a
		// remove(object) method?
		// for (CategoryType catType : this.getCategory()) {
		// if (catType.getLevel() == cat.getLevel() &&
		// catType.getName().equals(cat.getName()))
		// removalCandidateIndex = this.getCategory().indexOf(catType);
		// }
		//
		// if (removalCandidateIndex != -1)
		// this.getCategory().remove(removalCandidateIndex);
	}

	/**
	 * Remove all categories whose level is equal or greater than the parameter
	 * 
	 * @param level
	 */
	public void removeAll(int level) {
		List<CategoryType> removalCandidates = new ArrayList<CategoryType>();

		for (CategoryType catType : this.getCategories()) {
			if (catType.getLevel() >= level)
				removalCandidates.add((CategoryType) catType);
		}

		if (!removalCandidates.isEmpty())
			this.getCategories().removeAll(removalCandidates);
	}

	/**
	 * Convenience method to get the number of present Category elements without
	 * calling the getCategory() method
	 * 
	 * @return the size
	 */
	public int size() {
		return this.categories.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.flow.IFlowCategorization#getCategory()
	 */
	public List<CategoryType> getCategories() {
		if (categories == null) {
			categories = new ArrayList<CategoryType>();
		}
		return this.categories;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.flow.IFlowCategorization#getName()
	 */
	public String getName() {
		if (name == null) {
			return "ILCD";
		} else {
			return name;
		}
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.flow.IFlowCategorization#getCategories()
	 */
	public String getCategoriesFileRef() {
		return categoriesFileRef;
	}

	/**
	 * Sets the value of the categories property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCategoriesFileRef(String value) {
		this.categoriesFileRef = value;
	}

	public String getCategory(int level) {
		if (this.categories != null)
			for (CategoryType cat : this.categories) {
				if (cat.getLevel() == level)
					return cat.getName();
			}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<IClass> getIClassList() {
		// Note: this works because every CategoryType object implements the
		// IClass interface
		// That's a hack because Java Generics are not reifying types
		return (List<IClass>) (List) this.categories;
	}

	public String getClassHierarchyAsString() {
		StringBuilder buffer = new StringBuilder();

		int numberOfClasses = categories.size();
		int i = 0;
		for (CategoryType catType : categories) {
			buffer.append(catType.getName());
			if (i++ < numberOfClasses - 1) {
				buffer.append(" / ");
			}
		}

		return buffer.toString();
	}
}
