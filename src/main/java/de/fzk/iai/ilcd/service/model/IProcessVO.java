/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model;

import java.util.List;

import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.CompletenessValue;
import de.fzk.iai.ilcd.service.model.process.IQuantitativeReference;
import de.fzk.iai.ilcd.service.model.process.IReview;

/**
 * The Interface IProcessVO.
 */
public interface IProcessVO extends IDataSetVO, IProcessListVO {

	/**
	 * Gets the base name.
	 * 
	 * @return IMultiLangString containing only the base name part of the
	 *         process name
	 */
	public abstract IMultiLangString getBaseName();

	/**
	 * Gets the format.
	 * 
	 * @return the dataSetFormat
	 */
	public abstract String getFormat();

	/**
	 * Synonyms / alternative names / brands of the good, service, or process.
	 * Separated by semicolon.Gets the value of the synonyms property.
	 * 
	 * @return the synonyms
	 */
	public abstract IMultiLangString getSynonyms();

	/**
	 * Gets the value of the useAdvice property.
	 * 
	 * @return the use advice
	 */
	public abstract IMultiLangString getUseAdvice();

	/**
	 * Gets the technical purpose.
	 * 
	 * @return description of the technical purpose of this process data set
	 */
	public abstract IMultiLangString getTechnicalPurpose();

	/**
	 * Gets the technology description.
	 * 
	 * @return technology description of this process data set
	 */
	public abstract IMultiLangString getTechnologyDescription();

	/**
	 * Gets the completeness product model.
	 * 
	 * @return the completenessProductModel
	 */
	public abstract CompletenessValue getCompletenessProductModel();

	/**
	 * Gets the quantitative reference.
	 * 
	 * @return the quantitativeReference
	 */
	public abstract IQuantitativeReference getQuantitativeReference();

	/**
	 * Gets the reviews.
	 * 
	 * @return the review
	 */
	public abstract List<IReview> getReviews();

	/**
	 * Gets the reference to data set approval organisation.
	 * 
	 * @return reference to data set approval organisation
	 */
	public abstract IGlobalReference getApprovedBy();

}
