package de.fzk.iai.ilcd.service.client.impl;

import java.io.InputStream;

/**
 * This class wraps a file name and an InputStream object.
 * 
 */
public class FileParam {

	protected String fileName;

	protected InputStream inputStream;

	/**
	 * 
	 * @param fileName
	 *            the file name
	 * @param inputStream
	 *            the InputStream
	 */
	public FileParam( String fileName, InputStream inputStream ) {
		this.fileName = fileName;
		this.inputStream = inputStream;
	}

	/**
	 * Get the file name
	 * 
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Set the file name
	 * 
	 * @param fileName
	 *            the file name
	 */
	public void setFileName( String fileName ) {
		this.fileName = fileName;
	}

	/**
	 * get the InputStream
	 * 
	 * @return the InputStream
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * Set the InputStream
	 * 
	 * @param inputStream
	 */
	public void setInputStream( InputStream inputStream ) {
		this.inputStream = inputStream;
	}

}
