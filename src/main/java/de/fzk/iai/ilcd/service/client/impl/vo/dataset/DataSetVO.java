/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for
 * Applied Computer Science (IAI).
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl.vo.dataset;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;

import de.fzk.iai.ilcd.service.client.impl.vo.Constants;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassificationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.Other;
import de.fzk.iai.ilcd.service.model.IContactVO;
import de.fzk.iai.ilcd.service.model.IFlowPropertyVO;
import de.fzk.iai.ilcd.service.model.IFlowVO;
import de.fzk.iai.ilcd.service.model.ILCIAMethodVO;
import de.fzk.iai.ilcd.service.model.IProcessVO;
import de.fzk.iai.ilcd.service.model.ISourceVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupVO;
import de.fzk.iai.ilcd.service.model.common.IClass;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;

@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( propOrder = { "uuidAsString", "permanentUri", "dataSetVersion", "name", "shortName", "classification", "generalComment", "other", "duplicates" } )
public abstract class DataSetVO implements de.fzk.iai.ilcd.service.model.IDataSetVO {

	@XmlAttribute( namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI" )
	protected String sourceId;

	@XmlAttribute( namespace = "http://www.w3.org/1999/xlink" )
	@XmlSchemaType( name = "anyURI" )
	protected String href;

	@XmlElement( name = "uuid" )
	protected String uuidAsString;

	@XmlElement( type = LString.class )
	protected List<LString> name = new ArrayList<LString>();

	@XmlElement( type = LString.class )
	protected List<LString> shortName;

	protected String permanentUri;

	protected List<ClassificationType> classification;

	@XmlElement( type = LString.class )
	protected List<LString> generalComment = new ArrayList<LString>();

	@XmlElement( name = "other" )
	protected Other other;

	protected String dataSetVersion;

	@XmlAnyElement
	@XmlElementWrapper(name="duplicates")
	@XmlElementRef
	protected List<DataSetVO> duplicates = null;

	@XmlTransient
	protected String rootDataStockId = null;

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getUuid()
	 */
	public String getUuidAsString() {
		return uuidAsString;
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setUuid(java.lang.String)
	 */
	public void setUuidAsString( String uuid ) {
		this.uuidAsString = uuid;
	}

	public void setUuid( String uuid ) {
		this.setUuidAsString( uuid );
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getName()
	 */
	public IMultiLangString getName() {
		if ( name == null ) {
			name = new ArrayList<LString>();
		}
		return new MultiLangString( name );
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setName(java.util.List)
	 */
	protected void setName( List<LString> name ) {
		this.name = name;
	}

	public void addName( LString lName ) {
		if ( name == null )
			name = new ArrayList<LString>();
		lName.insertWithoutDuplicates( name );
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setName(java.lang.String)
	 */
	public void setName( String lang, String name ) {
		LString lString = new LString( lang, name );
		this.addName( lString );
	}

	public void setName( String name ) {
		LString lString = new LString( name );
		this.addName( lString );
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getDefaultName()
	 */
	public String getDefaultName() {
		return this.getName().getValue();
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getShortName()
	 */
	public IMultiLangString getShortName() {
		if ( shortName == null ) {
			shortName = new ArrayList<LString>();
		}
		return new MultiLangString( shortName );
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setShortName(java.util.List)
	 */
	protected void setShortName( List<LString> shortName ) {
		this.shortName = shortName;
	}

	public void addShortName( LString name ) {
		if ( shortName == null )
			shortName = new ArrayList<LString>();
		name.insertWithoutDuplicates( shortName );
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setShortName(java.lang.String)
	 */
	public void setShortName( String shortName ) {
		LString lString = new LString( shortName );
		this.addName( lString );
	}

	public void setShortName( String lang, String shortName ) {
		LString lSTring = new LString( lang, shortName );
		this.addName( lSTring );
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getPermanentUri()
	 */
	public String getPermanentUri() {
		return permanentUri;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setPermanentUri(java.lang.String
	 * )
	 */
	public void setPermanentUri( String permanentUri ) {
		this.permanentUri = permanentUri;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append( this.name );
		buf.append( " (" );
		buf.append( this.uuidAsString );
		buf.append( ", " );
		buf.append( this.permanentUri );
		buf.append( ")" );
		return buf.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setGeneralComment(java.util
	 * .List)
	 */
	protected void setGeneralComment( List<LString> generalComment ) {
		this.generalComment = generalComment;
	}

	public void addGeneralComment( LString comment ) {
		if ( generalComment == null )
			generalComment = new ArrayList<LString>();
		comment.insertWithoutDuplicates( generalComment );
	}

	public void setGeneralComment( String lang, String value ) {
		LString comment = new LString( lang, value );
		this.addGeneralComment( comment );
	}

	public void setGeneralComment( String value ) {
		LString comment = new LString( value );
		this.addGeneralComment( comment );
	}

	protected List<LString> getGeneralComment() {
		return generalComment;
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getGeneralComment()
	 */
	public IMultiLangString getDescription() {
		if ( generalComment == null ) {
			generalComment = new ArrayList<LString>();
		}
		return new MultiLangString( generalComment );
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getClassification()
	 */
	public ClassificationType getClassification( String classificationSystem ) {
		for ( ClassificationType classification : getClassifications() ) {
			if ( classification.getName().equals( classificationSystem ) )
				return classification;
		}
		return null;
	}

	public ClassificationType getClassification() {
		if ( !getClassifications().isEmpty() )
			return this.classification.get( 0 );
		else
			return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setClassification(de.fzk.iai
	 * .ilcd.api.vo.types.common.ClassificationType)
	 */
	public void setClassification( ClassificationType classification ) {
		for ( ClassificationType classif : getClassifications() ) {
			if ( classif.getName().equals( classification.getName() ) ) {
				this.classification.remove( classif );
				this.classification.add( classification );
				return;
			}
		}
		getClassifications().add( classification );
	}

	public List<ClassificationType> getClassifications() {
		if ( this.classification == null )
			this.classification = new ArrayList<ClassificationType>();
		return classification;
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getDataSetVersion()
	 */
	public String getDataSetVersion() {
		return dataSetVersion;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setDataSetVersion(java.lang
	 * .String)
	 */
	public void setDataSetVersion( String value ) {
		this.dataSetVersion = value;
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#getShortName()
	 */
	// public String getShortName() {
	// return shortName;
	// }

	/*
	 * (non-Javadoc)
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setShortName(java.lang.String)
	 */
	// public void setShortName(String shortName) {
	// this.shortName = shortName;
	// }

	/**
	 * @deprecated
	 */
	public String getTopCategory() {
		try {
			return this.getClassification().getClasses().get( 0 ).getName();
		}
		catch ( NullPointerException e ) {
			return null;
		}
	}

	/**
	 * @deprecated
	 */
	public String getSubCategory() {
		try {
			return this.getClassification().getClasses().get( 1 ).getName();
		}
		catch ( NullPointerException e ) {
			return null;
		}
		catch ( IndexOutOfBoundsException e ) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IDataSetVO#setCategory(int,
	 * java.lang.String)
	 */
	/**
	 * @deprecated
	 */
	public void setCategory( int level, String category ) {

		ClassificationType classification;

		if ( this.classification.isEmpty() ) {
			classification = new ClassificationType();
			this.classification.add( classification );
		}
		else {
			classification = getClassification();
		}

		ClassType clazz = null;
		if ( classification.size() == 0 ) {
			clazz = new ClassType();
			classification.add( clazz );
		}
		else {
			for ( IClass c : classification.getClasses() ) {
				if ( c.getLevel() != null && c.getLevel() == level )
					clazz = (ClassType) c;
			}
			if ( clazz == null ) {
				clazz = new ClassType();
				classification.add( clazz );
			}
		}

		clazz.setValue( category );
		clazz.setLevel( level );
	}

	/**
	 * @deprecated
	 */
	public void setTopCategory( String topCategory ) {
		setCategory( 0, topCategory );
	}

	/**
	 * @deprecated
	 */
	public void setSubCategory( String subCategory ) {
		setCategory( 1, subCategory );
	}

	/**
	 * Returns the name of the schema for this dataset type (using the values
	 * defined in {@link de.fzk.iai.ilcd.service.client.impl.vo.Constants}).
	 * 
	 * @return the schema name
	 */
	public String getSchemaName() {
		if ( this instanceof ProcessDataSetVO )
			return Constants.SERVICE_API_PROCESS_SCHEMA_NAME;
		else if ( this instanceof FlowDataSetVO )
			return Constants.SERVICE_API_FLOW_SCHEMA_NAME;
		else if ( this instanceof FlowPropertyDataSetVO )
			return Constants.SERVICE_API_FLOW_PROPERTY_SCHEMA_NAME;
		else if ( this instanceof UnitGroupDataSetVO )
			return Constants.SERVICE_API_UNIT_GROUP_SCHEMA_NAME;
		else if ( this instanceof SourceDataSetVO )
			return Constants.SERVICE_API_SOURCE_SCHEMA_NAME;
		else if ( this instanceof ContactDataSetVO )
			return Constants.SERVICE_API_CONTACT_SCHEMA_NAME;
		else if ( this instanceof LCIAMethodDataSetVO )
			return Constants.SERVICE_API_LCIAMETHOD_SCHEMA_NAME;
		else if ( this instanceof LifeCycleModelDataSetVO )
			return Constants.SERVICE_API_LIFECYCLEMODEL_SCHEMA_NAME;
		else
			return null;
	}

	/**
	 * Returns the namespace URI for this dataset type (using the values defined
	 * in {@link de.fzk.iai.ilcd.service.client.impl.vo.Constants}).
	 * 
	 * @return the schema name
	 */
	public String getNameSpaceURI() {
		if ( this instanceof IProcessVO )
			return Constants.NS_SERVICE_API_PROCESS;
		else if ( this instanceof IFlowVO )
			return Constants.NS_SERVICE_API_FLOW;
		else if ( this instanceof IFlowPropertyVO )
			return Constants.NS_SERVICE_API_FLOWPROPERTY;
		else if ( this instanceof IUnitGroupVO )
			return Constants.NS_SERVICE_API_UNITGROUP;
		else if ( this instanceof ISourceVO )
			return Constants.NS_SERVICE_API_SOURCE;
		else if ( this instanceof IContactVO )
			return Constants.NS_SERVICE_API_CONTACT;
		else if ( this instanceof ILCIAMethodVO )
			return Constants.NS_SERVICE_API_LCIAMETHOD;
		else if ( this instanceof ILCIAMethodVO )
			return Constants.NS_SERVICE_API_LIFECYCLEMODEL;
		else
			return null;
	}

	public String getDataSetType() {
		return (this.getClass().getSimpleName().substring( 0, this.getClass().getSimpleName().indexOf( "DataSet" ) ).toUpperCase());
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href
	 *            the href to set
	 */
	public void setHref( String href ) {
		this.href = href;
	}

	/**
	 * @return the sourceNode
	 */
	public String getSourceId() {
		return sourceId;
	}

	/**
	 * @param sourceId
	 *            the sourceNode to set
	 */
	public void setSourceId( String sourceId ) {
		this.sourceId = sourceId;
	}

	/**
	 * Gets the identifier of the originating root data stock.
	 *
	 * @return the source id
	 */
	public String getRootDataStockId() {
		return this.rootDataStockId;
	}

	/**
	 * Sets identifier of the originating root data stock.
	 *
	 * @param rootDataStockId
	 *            the new root data stock id
	 */
	public  void setRootDataStockId(String rootDataStockId) {
		this.rootDataStockId = rootDataStockId;
	}

	public Other getOther() {
		if ( this.other == null ) {
			this.other = new Other();
		}
		return this.other;
	}

	public void beforeMarshal( Marshaller m ) {
		if ( other != null && other.getAny().isEmpty() ) {
			other = null;
		}
	}
	
	public List<String> getLanguages() {
		List<String> languages = new ArrayList<String>();
		for (LString l : this.name)
			languages.add(l.getLang());
		return languages;
	}

	public List<DataSetVO> getDuplicates() {
		if ( duplicates == null )
			duplicates = new ArrayList<DataSetVO>();
		return duplicates;
	}

	public void setDuplicates(List<DataSetVO> duplicates) {
		this.duplicates = duplicates;
	}

}
