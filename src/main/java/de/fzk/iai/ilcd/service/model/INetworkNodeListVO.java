package de.fzk.iai.ilcd.service.model;

import de.fzk.iai.ilcd.service.client.impl.vo.networknode.NetworkNodeStatusVO;
import java.util.List;

public interface INetworkNodeListVO {

  List<NetworkNodeStatusVO> getNetworkNodes();

}
