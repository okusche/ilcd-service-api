/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model;

import java.util.List;

import de.fzk.iai.ilcd.service.model.enums.AreaOfProtectionValue;
import de.fzk.iai.ilcd.service.model.enums.LCIAImpactCategoryValue;

/**
 * The Interface ILCIAMethodVO.
 */
public interface ILCIAMethodVO extends IDataSetVO, ILCIAMethodListVO {

	/**
	 * Gets the methodology.
	 * 
	 * @return the methodology
	 */
	public abstract List<String> getMethodology();

	/**
	 * Gets the impact category.
	 * 
	 * @return the impact category
	 * 
	 */
	public abstract List<LCIAImpactCategoryValue> getImpactCategory();

	/**
	 * Gets the area of protection.
	 * 
	 * @return the area of protection
	 */
	public abstract List<AreaOfProtectionValue> getAreaOfProtection();

	/**
	 * Gets the impact indicator.
	 * 
	 * @return the impact indicator
	 */
	public abstract String getImpactIndicator();

//	/**
//	 * Gets the reference quantity.
//	 * 
//	 * @return the reference quantity
//	 */
//	public abstract IGlobalReference getReferenceQuantity();

}
