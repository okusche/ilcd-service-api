package de.fzk.iai.ilcd.service.client.impl.vo.dataset;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.api.app.common.GlobalReference;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ComplianceSystemType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.ReviewType;
import de.fzk.iai.ilcd.service.model.ILifeCycleModelVO;
import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;
import de.fzk.iai.ilcd.service.model.process.IReview;

@XmlRootElement(name = "lifecyclemodel", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "resultingProcess", "diagrams", "techProcesses", "reviews", "complianceSystems", "owner", "externalDocs" })
public class LifeCycleModelDataSetVO extends DataSetVO implements ILifeCycleModelVO {
	
	@XmlElement(name = "complianceSystem", type = ComplianceSystemType.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel")
	protected SortedSet<IComplianceSystem> complianceSystems;
	
	@XmlElement(name = "review", type = ReviewType.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel")
	protected List<IReview> reviews;
	
	@XmlElement(name = "owner", type = GlobalReference.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel")
	protected List<IGlobalReference> owner;
	
	@XmlElement(name = "diagram", type = GlobalReference.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel")
	protected List<IGlobalReference> diagrams;
	
	@XmlElement(name = "externalDoc", type = GlobalReference.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel")
	protected List<IGlobalReference> externalDocs;
	
	@XmlElement(name = "techProcess", type = GlobalReference.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel")
	protected List<IGlobalReference> techProcesses;
	
	@XmlElement(name = "resultingProcess", type = GlobalReference.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel")
	protected List<IGlobalReference> resultingProcess;
	
	public Set<IComplianceSystem> getComplianceSystems() {
		if (this.complianceSystems == null) {
			this.complianceSystems = new TreeSet<IComplianceSystem>();
		}
		return this.complianceSystems;
	}

	public IMultiLangString getBaseName() {
		// TODO Auto-generated method stub
		return null;
	}

	public IMultiLangString getTreatmentStandardsRoutes() {
		// TODO Auto-generated method stub
		return null;
	}

	public IMultiLangString getMixAndLocationTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	public IMultiLangString getFunctionalUnitFlowProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<IReview> getReviews() {
		if (reviews == null) {
			reviews = new ArrayList<IReview>();
		}
		return reviews;
	}
	
	public void addReview(ReviewType r) {
		if (reviews == null) {
			reviews = new ArrayList<IReview>();
		}
		reviews.add(r);
	}
	
	public List<IGlobalReference> getReferenceToResultingProcess() {
		if (this.resultingProcess == null)
			this.resultingProcess = new ArrayList<IGlobalReference>();
		return this.resultingProcess;
	}

	public List<IGlobalReference> getReferenceToExternalDocumentation() {
		if (this.externalDocs == null)
			this.externalDocs = new ArrayList<IGlobalReference>();
		return this.externalDocs;
	}

	public List<IGlobalReference> getReferenceToProcess() {
		if (this.techProcesses == null)
			this.techProcesses = new ArrayList<IGlobalReference>();
		return this.techProcesses;
	}

	public List<IGlobalReference> getReferenceToDiagram() {
		if (this.diagrams == null)
			this.diagrams = new ArrayList<IGlobalReference>();
		return this.diagrams;
	}

	public List<IGlobalReference> getReferenceToOwnershipOfDataSet() {
		if (this.owner == null)
			this.owner = new ArrayList<IGlobalReference>();
		return this.owner;
	}

	public List<IGlobalReference> getOwners() {
		return owner;
	}

	public void setOwners(List<IGlobalReference> owners) {
		this.owner = owners;
	}

	public List<IGlobalReference> getDiagrams() {
		return diagrams;
	}

	public void setDiagrams(List<IGlobalReference> diagrams) {
		this.diagrams = diagrams;
	}

	public List<IGlobalReference> getExternalDocs() {
		return externalDocs;
	}

	public void setExternalDocs(List<IGlobalReference> externalDocs) {
		this.externalDocs = externalDocs;
	}

	public List<IGlobalReference> getTechProcesses() {
		return techProcesses;
	}

	public void setTechProcesses(List<IGlobalReference> techProcesses) {
		this.techProcesses = techProcesses;
	}

	public List<IGlobalReference> getResultingProcess() {
		return resultingProcess;
	}

	public void setResultingProcess(List<IGlobalReference> resultingProcess) {
		this.resultingProcess = resultingProcess;
	}

	public void setComplianceSystems(SortedSet<IComplianceSystem> complianceSystems) {
		if (complianceSystems != null)
			this.complianceSystems = new TreeSet<IComplianceSystem>(complianceSystems);
		else
			this.complianceSystems = null;
	}
	
	public void addComplianceSystem(IComplianceSystem complianceSystem) {
		if (this.complianceSystems == null) {
			this.complianceSystems = new TreeSet<IComplianceSystem>();
		}
		this.complianceSystems.add(complianceSystem);
	}

	public void setReviews(List<IReview> reviews) {
		this.reviews = reviews;
	}

}
