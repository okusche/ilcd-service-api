/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model.process;

import java.util.List;
import java.util.Set;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.TypeOfReviewValue;

// TODO: Auto-generated Javadoc
/**
 * The Interface IReview.
 */
public interface IReview {

	/**
	 * Gets the value of the scope property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the scope property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getScopes().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link IScope }
	 *
	 * @return the scopes
	 */
	public abstract Set<IScope> getScopes();

	/**
	 * Data quality indicators serve to provide the reviewed key information on
	 * the data set in a defined, computer-readable (and hence searchable) form.
	 * This serves to support LCA practitioners to identify/select the highest
	 * quality and most appropriate data sets.
	 * 
	 * @return possible object is {@link IDataQualityIndicator }
	 * 
	 */
	public abstract Set<IDataQualityIndicator> getDataQualityIndicators();

	/**
	 * Summary of the review. All the following items should be explicitly
	 * addressed: Representativeness, completeness, and precision of Inputs and
	 * Outputs for the process in its documented location, technology and time
	 * i.e. both completeness of technical model (product, waste, and elementary
	 * flows) and completeness of coverage of the relevant problem fields
	 * (environmental, human health, resource use) for this specific good,
	 * service, or process. Plausibility of data. Correctness and
	 * appropriateness of the data set documentation. Appropriateness of system
	 * boundaries, cut-off rules, LCI modelling choices such as e.g. allocation,
	 * consistency of included processes and of LCI methodology. If the data set
	 * comprises pre-calculated LCIA results, the correspondence of the Input
	 * and Output elementary flows (including their geographical validity) with
	 * the applied LCIA method(s) should be addressed by the reviewer. An
	 * overall quality statement on the data set may be included here.Gets the
	 * value of the reviewDetails property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the reviewDetails property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getReviewDetails().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link LString }
	 *
	 * @return the review details
	 */
	public abstract IMultiLangString getReviewDetails();

	/**
	 * Gets the value of the referenceToNameOfReviewerAndInstitution property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the referenceToNameOfReviewerAndInstitution
	 * property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getReferenceToNameOfReviewerAndInstitution().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 *
	 * @return the references to reviewers
	 * {@link IGlobalReference }
	 */
	public abstract List<IGlobalReference> getReferencesToReviewers();

	/**
	 * Gets the value of the otherReviewDetails property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the otherReviewDetails property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getOtherReviewDetails().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link LString }
	 *
	 * @return the other review details
	 */
	public abstract IMultiLangString getOtherReviewDetails();

	/**
	 * Gets the value of the referenceToCompleteReviewReport property.
	 * 
	 * @return possible object is {@link IGlobalReference }
	 * 
	 */
	// at the moment not used in the ServiceAPI
	// public abstract IGlobalReference getReferenceToCompleteReviewReport();

	/**
	 * Gets the value of the type property.
	 * 
	 * @return possible object is {@link TypeOfReviewValue }
	 * 
	 */
	public abstract TypeOfReviewValue getType();

}
