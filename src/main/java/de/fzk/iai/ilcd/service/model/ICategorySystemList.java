package de.fzk.iai.ilcd.service.model;

import java.util.List;

public interface ICategorySystemList extends IList {

	public List<ICategorySystem> getCategorySystems();

}
