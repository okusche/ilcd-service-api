/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.Logger;

import de.fzk.iai.ilcd.service.client.impl.AbstractServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ContactDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowPropertyDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.LCIAMethodDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.LifeCycleModelDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.SourceDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.UnitGroupDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.epd.ProcessSubType;
import de.fzk.iai.ilcd.service.model.IDataSetVO;

/**
 * The Class DatasetVODAO.
 * 
 */
public class DatasetVODAO extends AbstractServiceDAO {

	/** The log. */
	@SuppressWarnings("unused")
	private final Logger log = org.apache.logging.log4j.LogManager.getLogger(this.getClass());

	/**
	 * Instantiates a new dataset vodao.
	 */
	public DatasetVODAO() {
	}

	/**
	 * Marshal.
	 * 
	 * @param obj
	 *            the obj
	 * @param stream
	 *            the stream
	 * @throws JAXBException
	 */
	public void marshal(Object obj, OutputStream stream) throws JAXBException {
		JAXBContext context;
		context = createContext();

		String schemaLocation = null;
		if (renderSchemaLocation) {
			if (obj instanceof IDataSetVO) {
				schemaLocation = ((DataSetVO) obj).getNameSpaceURI() + " ../schemas/"
						+ ((DataSetVO) obj).getSchemaName();
			} else if (obj instanceof DataSetList) {
				schemaLocation = "http://www.ilcd-network.org/ILCD/ServiceAPI ../schemas/ILCD_Service_API.xsd";
			}
		}
		
		super.marshal(obj, stream, context, schemaLocation);

	}

	/**
	 * Unmarshal.
	 * 
	 * @param stream
	 *            the stream
	 * @return the object
	 * @throws JAXBException
	 */
	public Object unmarshal(InputStream stream) throws JAXBException {

		JAXBContext context = createContext();

		return super.unmarshal(stream, context);

	}
	
	private JAXBContext createContext() throws JAXBException {
		return JAXBContext.newInstance( ContactDataSetVO.class, DataSetList.class, DataSetVO.class, FlowDataSetVO.class, FlowPropertyDataSetVO.class, LCIAMethodDataSetVO.class, ProcessDataSetVO.class, SourceDataSetVO.class, UnitGroupDataSetVO.class, ProcessSubType.class, LifeCycleModelDataSetVO.class );
	}
}
