/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.Logger;

import de.fzk.iai.ilcd.api.util.TraceInputStream;
import de.fzk.iai.ilcd.service.client.impl.vo.NamespacePrefixMapperImpl;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.NMultiLangStringXMLAdapter;

public class AbstractServiceDAO {

	private final Logger log = org.apache.logging.log4j.LogManager.getLogger(this.getClass());

	protected boolean renderSchemaLocation = false;

	public AbstractServiceDAO() {
		super();
	}

	/**
	 * Marshal.
	 * 
	 * @param obj
	 *            the obj
	 * @param stream
	 *            the stream
	 * @throws JAXBException
	 */
	public void marshal(Object obj, OutputStream stream, JAXBContext context, String schemaLocation)
			throws JAXBException {
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NamespacePrefixMapperImpl(obj));
		if (renderSchemaLocation)
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schemaLocation);
		m.setAdapter( NMultiLangStringXMLAdapter.class, new NMultiLangStringXMLAdapter("en") );
		m.marshal(obj, stream);

	}

	/**
	 * Unmarshal.
	 * 
	 * @param stream
	 *            the stream
	 * @return the object
	 * @throws JAXBException
	 */
	public Object unmarshal(InputStream stream, JAXBContext context) throws JAXBException {

		TraceInputStream trac = new TraceInputStream(stream, System.out);
		trac.setTrace(log.isTraceEnabled());

		Unmarshaller unmarshaller = context.createUnmarshaller();

		if (log.isDebugEnabled()) {
			// DO NOT enable on production systems!
			// System.setProperty("jaxb.debug", "true");
			unmarshaller.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
		}

		unmarshaller.setProperty("com.sun.xml.bind.ObjectFactory", new ObjectFactory());

		Object result = unmarshaller.unmarshal(trac);

		if (result instanceof JAXBElement<?>)
			return ((JAXBElement<?>) result).getValue();
		else
			return result;

	}

	/**
	 * @return the renderSchemaLocation
	 */
	public boolean isRenderSchemaLocation() {
		return renderSchemaLocation;
	}

	/**
	 * @param renderSchemaLocation
	 *            the renderSchemaLocation to set
	 */
	public void setRenderSchemaLocation(boolean renderSchemaLocation) {
		this.renderSchemaLocation = renderSchemaLocation;
	}

}
