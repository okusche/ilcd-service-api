/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for
 * Applied Computer Science (IAI).
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.model.IStringList;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "string" })
@XmlRootElement(name = "stringList", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
public class StringList extends SimpleList implements IStringList {

	@XmlElement(name = "string")
	protected List<String> string;

	@XmlTransient
	private TreeSet<String> hs = new TreeSet<String>();

	public List<String> getStrings() {
		return string;
	}

	public void setString(List<String> strings) {
		this.hs.clear();
		this.hs.addAll(strings);
		this.flush();
	}

	public void addString(String string) {
		this.hs.add(string);
	}

	public void flush() {
		this.string = new ArrayList<String>(hs);
	}

	/**
	 * @deprecated
	 */
	public void dedupe() { // keep it as a placeholder
		// No longer required
	}

	@Override
	public List<?> getPayload() {
		return this.getStrings();
	}

}
