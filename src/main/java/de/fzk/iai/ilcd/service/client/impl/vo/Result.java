/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo;

import java.util.List;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.model.IDataSetListVO;

/**
 * Wrapper class for the result of data set list responses
 * @param <T> Type of list value object
 */
public class Result<T extends IDataSetListVO> {

	/**
	 * The start index
	 */
	private final int startIndex;
	
	/**
	 * The page size
	 */
	private final int pageSize;

	/**
	 * The total size of available items
	 */
	private final int totalSize;
	
	/**
	 * The actual list of items for the {@link #startIndex} and {@link #pageSize}
	 */
	private final List<T> dataSets;

	/**
	 * Create a result for a data set list
	 * @param startIndex the start index
	 * @param pageSize the page size
	 * @param totalSize the total size of available items
	 * @param dataSets list of items for this result
	 */
	public Result(int startIndex, int pageSize, int totalSize, List<T> dataSets) {
		super();
		this.startIndex = startIndex;
		this.pageSize = pageSize;
		this.totalSize = totalSize;
		this.dataSets = dataSets;
	}
	
	/**
	 * Create a result list from a {@link DataSetList} object
	 * @param dsl the data set list object to use
	 */
	@SuppressWarnings("unchecked")
	public Result(DataSetList dsl) {
		super();
		this.startIndex = dsl.getStartIndex();
		this.pageSize = dsl.getPageSize();
		this.totalSize = dsl.getTotalSize();
		this.dataSets = (List<T>) dsl.getDataSet();
	}

	/**
	 * Get the start index of the result
	 * @return start index of the result
	 */
	public int getStartIndex() {
		return this.startIndex;
	}

	/**
	 * Get the page size of the result
	 * @return page size of the result
	 */
	public int getPageSize() {
		return this.pageSize;
	}

	/**
	 * Get the total size of the result list
	 * @return total size of the result list
	 */
	public int getTotalSize() {
		return this.totalSize;
	}

	/**
	 * Get the items of the current result page
	 * @return items of the current result page
	 */
	public List<T> getDataSets() {
		return this.dataSets;
	}
	
	
	
	
	
}
