/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model;

import java.util.List;

import de.fzk.iai.ilcd.service.client.impl.vo.epd.ProcessSubType;
import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.enums.TypeOfProcessValue;
import de.fzk.iai.ilcd.service.model.process.IAccessInformation;
import de.fzk.iai.ilcd.service.model.process.ILCIMethodInformation;
import de.fzk.iai.ilcd.service.model.process.IReferenceFlow;
import de.fzk.iai.ilcd.service.model.process.ITimeInformation;

/**
 * The Interface IProcessListVO.
 */
public interface IProcessListVO extends IDataSetListVO, IDeclaresCompliance {

	public static final String URL_SUFFIX = "processes";

	/**
	 * Gets the location.
	 * 
	 * @return the location
	 */
	public abstract String getLocation();

	/**
	 * Checks if is parameterized.
	 * 
	 * @return the parameterized
	 */
	public abstract Boolean getParameterized();

	/**
	 * Gets the value of the time property.
	 * 
	 * @return possible object is {@link ITimeInformation }
	 * 
	 */
	public abstract ITimeInformation getTimeInformation();

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public abstract TypeOfProcessValue getType();

	/**
	 * Checks for results.
	 * 
	 * @return this process data set has also LCIA results
	 */
	public abstract Boolean getHasResults();

	/**
	 * Gets the access information.
	 * 
	 * @return possible object is {@link IAccessInformation }
	 */
	public abstract IAccessInformation getAccessInformation();

	/**
	 * Gets the LCI method information.
	 * 
	 * @return possible object is {@link ILCIMethodInformation }
	 */
	public abstract ILCIMethodInformation getLCIMethodInformation();

	/**
	 * Gets the overall quality description.
	 * 
	 * @return the overallQualityValue
	 */
	public abstract String getOverallQuality();
	
	/**
	 * Returns true if authentication is required to read this dataset.
	 * 
	 * @return true if authentication is required
	 */
	public abstract boolean isAccessRestricted();

	/**
	 * Sets whether authentication is required to read this dataset.
	 * 
	 * @param restricted
	 *            true if access is restricted
	 */
	public abstract void setAccessRestricted(boolean restricted);

	/**
	 * Checks whether the dataset contains an attached product model.
	 * 
	 * @return this process contains an attached product model
	 */
	public abstract Boolean getContainsProductModel();

	/**
	 * Gets the owner reference.
	 * 
	 * @return the ownership
	 */
	public abstract IGlobalReference getOwnerReference();

	/**
	 * Gets the list of datasources
	 * 
	 * @return the datasources
	 */
	public abstract List<IGlobalReference> getDataSources();

	/**
	 * Gets the subtype
	 * 
	 * (extension)
	 * @return the subtype
	 */
	public abstract ProcessSubType getSubType();

	/**
	 * Gets the registration number
	 * 
	 * @return the reg. number
	 */
	public abstract String getRegistrationNumber();
	
	/**
	 * Gets the registration authority
	 * 
	 * @return the reg. authority
	 */
	public abstract IGlobalReference getRegistrationAuthority();

	/**
	 * Gets the list of reference flows
	 *
	 * @return the list of reference flows
	 */
	public abstract List<IReferenceFlow> getReferenceFlows();

	/**
	 * Gets the meta data only flag
	 * 
	 * @return the meta data only flag
	 */
	public abstract Boolean getMetaDataOnly();

}
