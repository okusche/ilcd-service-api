/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model.process;

import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.LicenseTypeValue;

/**
 * The Interface IAccessInformation.
 *
 * @author clemens.duepmeier
 */
public interface IAccessInformation {

	/**
	 * Checks if a copyright is declared.
	 *
	 * @return true, if a copyright is declared
	 */
	public abstract boolean isCopyright();

	/**
	 * Gets the license type.
	 *
	 * @return the license type
	 */
	public abstract LicenseTypeValue getLicenseType();

	/**
	 * Gets the use restrictions.
	 *
	 * @return the use restrictions
	 */
	public abstract IMultiLangString getUseRestrictions();
}
