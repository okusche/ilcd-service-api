package de.fzk.iai.ilcd.service.client.impl.adapter;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

public class InstantAdapter {

  private static Instant fromString(String instantString) {
    return instantString == null ? null : Instant.parse(instantString);
  }

  private static String toString(Instant instant) {
    return instant == null ? null : instant.toString();
  }

  public static class XmlAdapter extends
      javax.xml.bind.annotation.adapters.XmlAdapter<String, Instant> {

    @Override
    public Instant unmarshal(String v) {
      return (v != null) ? Instant.parse(v) : null;
    }

    @Override
    public String marshal(Instant v) {
      return (v != null) ? DateTimeFormatter.ISO_INSTANT.format(v) : null;
    }
  }
}
