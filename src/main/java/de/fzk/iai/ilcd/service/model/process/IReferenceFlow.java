/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model.process;

import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;

// TODO: Auto-generated Javadoc
/**
 * The Interface IReferenceFlow.
 */
public interface IReferenceFlow {

	/**
	 * Gets the flow name.
	 *
	 * @return the name
	 */
	public abstract IMultiLangString getFlowName();

	/**
	 * Gets the flow property name.
	 *
	 * @return the flow property name
	 */
	public abstract IMultiLangString getFlowPropertyName();

	/**
	 * Gets the unit.
	 *
	 * @return the unit
	 */
	public abstract String getUnit();

	/**
	 * Gets the mean value.
	 *
	 * @return the mean value
	 */
	public Double getMeanValue();

	/**
	 * Gets the reference.
	 *
	 * @return the reference
	 */
	public abstract IGlobalReference getReference();

}
