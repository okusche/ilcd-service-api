/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.types.common;

import java.util.ArrayList;
import java.util.List;

import de.fzk.iai.ilcd.service.model.common.ILString;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import java.util.Objects;

public class MultiLangString implements IMultiLangString {

	protected List<LString> lStrings;

	public MultiLangString() {
		lStrings = new ArrayList<LString>();
	}

	public MultiLangString(List<LString> lStrings) {
		this.lStrings = lStrings;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public List<ILString> getLStrings() {
		// we use type erasure because we know that LSTring elements implement
		// ILString
		return (List<ILString>) (List) lStrings;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.common.IMultiLangString#getValue()
	 */
	public String getValue() {
		return getValue(IMultiLangString.DEFAULT_LANGUAGE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.common.IMultiLangString#getValue(java.lang
	 * .String)
	 */
	public String getValue(String lang) {
		for (LString item : lStrings) {
			if (item.getLang().equals(lang))
				return item.getValue();
		}
		return null;
	}

	public void addLString(LString lString) {
		if (this.lStrings == null)
			lStrings = new ArrayList<LString>();
		lString.insertWithoutDuplicates(lStrings);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.common.IMultiLangString#setValue(java.lang
	 * .String)
	 */
	public void setValue(String value) {
		setValue(IMultiLangString.DEFAULT_LANGUAGE, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.common.IMultiLangString#setValue(java.lang
	 * .String, java.lang.String)
	 */
	public void setValue(String lang, String value) {
		LString lString = new LString(lang, value);
		this.addLString(lString);
	}

	public String getDefaultValue() {
		return getValue();
	}

	public String getValueWithFallback(String lang) {
		String result = getValue(lang);
		if (result != null)
			return result;

		result = getValue();
		if (result != null)
			return result;
		
		for (LString ls : this.lStrings) {
			return ls.getValue();
		}
		
		return getValue();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		MultiLangString that = (MultiLangString) o;
		return Objects.equals(lStrings, that.lStrings);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(lStrings);
	}
}
