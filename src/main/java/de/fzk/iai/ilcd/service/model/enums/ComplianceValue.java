/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2010.03.01 at 01:55:00 PM MEZ
//

package de.fzk.iai.ilcd.service.model.enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ComplianceValues.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="ComplianceValues"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Fully compliant"/&gt;
 *     &lt;enumeration value="Not compliant"/&gt;
 *     &lt;enumeration value="Not defined"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ComplianceValues")
@XmlEnum
public enum ComplianceValue {

	/**
	 * Meets all requirements of this compliance aspect as defined in the
	 * respective "Compliance system".
	 * 
	 */
	@XmlEnumValue("Fully compliant")
	FULLY_COMPLIANT("Fully compliant"),

	/**
	 * Does not meet all requirements of this compliance aspect, as defined for
	 * the respective "Compliance system".
	 * 
	 */
	@XmlEnumValue("Not compliant")
	NOT_COMPLIANT("Not compliant"),

	/**
	 * For this compliance aspect the named "Compliance system" has not defined
	 * compliance requirements.
	 * 
	 */
	@XmlEnumValue("Not defined")
	NOT_DEFINED("Not defined");
	
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new compliance value.
	 *
	 * @param v the value
	 */
	ComplianceValue(String v) {
		value = v;
	}

	/**
	 * Value.
	 *
	 * @return the string
	 */
	public String value() {
		return value;
	}

	/**
	 * From value.
	 *
	 * @param v the value
	 * @return the compliance value
	 */
	public static ComplianceValue fromValue(String v) {
		for (ComplianceValue c : ComplianceValue.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
