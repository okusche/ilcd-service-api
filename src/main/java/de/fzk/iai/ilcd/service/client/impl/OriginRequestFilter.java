package de.fzk.iai.ilcd.service.client.impl;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;

public class OriginRequestFilter implements ClientRequestFilter {

	private String origin;

	public OriginRequestFilter(String origin) {
		this.origin = origin;
	}

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		if (origin != null) {
			requestContext.getHeaders().add("X-Origin", origin);
		}
	}
}
