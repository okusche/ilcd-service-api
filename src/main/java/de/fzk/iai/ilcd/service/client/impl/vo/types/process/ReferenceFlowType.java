/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.types.process;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.process.IReferenceFlow;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceFlowType", propOrder = { "flowName", "flowPropertyName", "unit", "meanValue", "reference" })
public class ReferenceFlowType implements IReferenceFlow {

	@XmlElement(type = LString.class, name = "name")
	protected List<LString> flowName;

	@XmlElement(type = LString.class, name = "flowProperty")
	protected List<LString> flowPropertyName;

	protected String unit;

	protected Double meanValue;

	// There is no href for this element: the href attribute is set in the
	// reference itself
	// @XmlAttribute(namespace = "http://www.w3.org/1999/xlink")
	// @XmlSchemaType(name = "anyURI")
	// protected String href;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected GlobalReferenceType reference;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.process.IReferenceFlow#getName()
	 */
	public MultiLangString getFlowName() {
		if (flowName == null) {
			flowName = new ArrayList<LString>();
		}
		return new MultiLangString(flowName);
	}

	public void setFlowName(String lang, String value) {
		LString lString = new LString(lang, value);
		this.addFlowName(lString);
	}

	public void setFlowName(String name) {
		LString lString = new LString(MultiLangString.DEFAULT_LANGUAGE, name);
		this.addFlowName(lString);
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void addFlowName(LString name) {
		if (this.flowName == null) {
			this.flowName = new ArrayList<LString>();
		}
		name.insertWithoutDuplicates(flowName);
	}

	protected void setFlowName(List<LString> names) {
		this.flowName = names;
	}

	public MultiLangString getFlowPropertyName() {
		if (flowPropertyName == null) {
			flowPropertyName = new ArrayList<LString>();
		}
		return new MultiLangString(flowPropertyName);
	}

	public void setFlowPropertyName(String lang, String value) {
		LString lString = new LString(lang, value);
		this.addFlowPropertyName(lString);
	}

	public void setFlowPropertyName(String name) {
		LString lString = new LString(MultiLangString.DEFAULT_LANGUAGE, name);
		this.addFlowPropertyName(lString);
	}

	protected void setFlowPropertyName(List<LString> name) {
		this.flowPropertyName = name;
	}

	public void addFlowPropertyName(LString name) {
		if (this.flowPropertyName == null) {
			this.flowPropertyName = new ArrayList<LString>();
		}
		name.insertWithoutDuplicates(this.flowPropertyName);
	}

	public Double getMeanValue() {
		return meanValue;
	}

	public void setMeanValue(Double meanValue) {
		this.meanValue = meanValue;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

//	/**
//	 * @return the href
//	 */
	// public String getHref() {
	// return href;
	// }

//	/**
//	 * @param href
//	 *            the href to set
//	 */
	// public void setHref(String href) {
	// this.href = href;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.process.IReferenceFlow#getReference()
	 */
	public GlobalReferenceType getReference() {
		return reference;
	}

	/**
	 * @param reference
	 *            the reference to set
	 */
	public void setReference(GlobalReferenceType reference) {
		this.reference = reference;
	}

}
