package de.fzk.iai.ilcd.service.client.impl.vo.networknode;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import de.fzk.iai.ilcd.service.model.INetworkNodeListVO;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "networkNodeList", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
@XmlType(propOrder = {"networkNodes"})
@XmlAccessorType(FIELD)
public class NetworkNodeStatusListVO implements INetworkNodeListVO {

    @XmlElementWrapper(name = "networkNodes", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
    @XmlElement(name = "networkNode", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
    private List<NetworkNodeStatusVO> networkNodes;

    @SuppressWarnings("unused")
    public NetworkNodeStatusListVO() {
    }

    public NetworkNodeStatusListVO(List<NetworkNodeStatusVO> networkNodes) {
        this.networkNodes = networkNodes;
    }

    public List<NetworkNodeStatusVO> getNetworkNodes() {
        return networkNodes;
    }

    @SuppressWarnings("unused")
    public void setNetworkNodes(List<NetworkNodeStatusVO> networkNodes) {
        this.networkNodes = networkNodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NetworkNodeStatusListVO that = (NetworkNodeStatusListVO) o;
        return Objects.equals(getNetworkNodes(), that.getNetworkNodes());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getNetworkNodes());
    }

    @Override
    public String toString() {
        return "NetworkNodeStatusListVO{" + "networkNodes=" + networkNodes + '}';
    }
}
