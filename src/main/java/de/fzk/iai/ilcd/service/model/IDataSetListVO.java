/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model;

import java.util.List;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetVO;
import de.fzk.iai.ilcd.service.model.common.IClassification;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;

/**
 * Base interface for DataSetList.
 * 
 */
public interface IDataSetListVO {

	/**
	 * Gets the URI of the data set.
	 * 
	 * This is important for lists of data sets that contain items from foreign
	 * database nodes.
	 * 
	 * @return the URI
	 */
	public abstract String getHref();

	/**
	 * Gets a string representation of the data set's UUID.
	 * 
	 * @return the UUID
	 */
	public abstract String getUuidAsString();

	/**
	 * Gets the name of the data set.
	 * 
	 * @return the name
	 */
	public abstract IMultiLangString getName();

	/**
	 * Gets the data sets permanentURI.
	 * 
	 * @return the permanentUri
	 */
	public abstract String getPermanentUri();

	/**
	 * Gets the data set's classficiation.
	 * 
	 * @return the classification
	 */
	public abstract IClassification getClassification();

	public abstract IClassification getClassification(String classificationSystem);

	public abstract List<DataSetVO> getDuplicates();

	public abstract void setDuplicates(List<DataSetVO> list);

	/**
	 * Gets the the dataSetVersion.
	 * 
	 * @return the version String
	 * 
	 */
	public abstract String getDataSetVersion();

	/**
	 * Sets the the href.
	 * 
	 * @param href
	 *            the new href
	 */
	public abstract void setHref(String href);

	/**
	 * Gets the the sourceId, which is the identifier of the originating
	 * database node.
	 * 
	 * @return the source id
	 */
	public abstract String getSourceId();

	/**
	 * Sets the sourceId, which is the identifier of the originating database
	 * node.
	 * 
	 * @param sourceId
	 *            the new source id
	 */
	public abstract void setSourceId(String sourceId);

	/**
	 * Gets the default name.
	 * 
	 * @return the default name
	 */
	public abstract String getDefaultName();

	/**
	 * Gets the default name.
	 * 
	 * @return the default name
	 */
	public abstract List<String> getLanguages();

	public abstract List<? extends IClassification> getClassifications();

	/**
	 * Gets the identifier of the originating root data stock.
	 *
	 * @return the source id
	 */
	public abstract String getRootDataStockId();

}
