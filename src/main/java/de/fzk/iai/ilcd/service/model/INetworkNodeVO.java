package de.fzk.iai.ilcd.service.model;

import java.time.Instant;
import java.util.List;

public interface INetworkNodeVO {

  INodeInfo getINodeInfo();

  List<IHeartbeatVO> getIHeartbeats();

  Instant getLastSeen();

  Boolean isAlive();

}
