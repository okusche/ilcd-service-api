/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.01 at 01:55:00 PM MEZ 
//

package de.fzk.iai.ilcd.service.model.enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TypeOfProcessValue.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="TypeOfProcessValue"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Unit process, single operation"/&gt;
 *     &lt;enumeration value="Unit process, black box"/&gt;
 *     &lt;enumeration value="LCI result"/&gt;
 *     &lt;enumeration value="Partly terminated system"/&gt;
 *     &lt;enumeration value="Avoided product system"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TypeOfProcessValues")
@XmlEnum
public enum TypeOfProcessValue {

	/**
	 * Unit operation type unit process that can not be further subdivided.
	 * Covers multi-functional processes of unit operation type.
	 * 
	 */
	@XmlEnumValue("Unit process, single operation")
	UNIT_PROCESS_SINGLE_OPERATION("Unit process, single operation"),

	/**
	 * Process-chain or plant level unit process. This covers horizontally
	 * averaged unit processes across different sites. Covers also those
	 * multi-functional unit processes, where the different co-products undergo
	 * different processing steps within the black box, hence causing
	 * allocation-problems for this data set.
	 * 
	 */
	@XmlEnumValue("Unit process, black box")
	UNIT_PROCESS_BLACK_BOX("Unit process, black box"),

	/**
	 * Aggregated data set of the complete or partial life cycle of a product
	 * system that next to the elementary flows (and possibly not relevant
	 * amounts of waste flows and radioactive wastes) lists in the input/output
	 * list exclusively the product(s) of the process as reference flow(s), but
	 * no other goods or services. E.g. cradle-to-gate and cradle-to-grave data
	 * sets. Check also the definition of "Partly terminated system".
	 * 
	 */
	@XmlEnumValue("LCI result")
	LCI_RESULT("LCI result"),

	/**
	 * Aggregated data set with however at least one product flow in the
	 * input/output list that needs further modelling, in addition to the
	 * reference flow(s). E.g. a process of an injection moulding machine with
	 * one open "Electricity" input product flow that requires the LCA
	 * practitioner to saturate with an Electricity production LCI data set
	 * (e.g. of the country where the machine is operated). Note that also
	 * aggregated process data sets that include relevant amounts of waste flows
	 * for which the waste management has not been modelled yet are
	 * "partly terminated system" data sets.
	 * 
	 */
	@XmlEnumValue("Partly terminated system")
	PARTLY_TERMINATED_SYSTEM("Partly terminated system"),

	/**
	 * Data set with all flows set to negative values OR all inputs be made to
	 * outputs and vice versa; i.e. a negative/inverted inventory (can be unit
	 * process, LCI result, or other type). Used in system
	 * expansion/substitution for consequential modelling.
	 * 
	 */
	@XmlEnumValue("Avoided product system")
	AVOIDED_PRODUCT_SYSTEM("Avoided product system"),
	
	/**
	 * EPD data set
	 */
	@XmlEnumValue("EPD")
	EPD("EPD");
	
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new type of process value.
	 *
	 * @param v the value
	 */
	TypeOfProcessValue(String v) {
		value = v;
	}

	/**
	 * Value.
	 *
	 * @return the string
	 */
	public String value() {
		return value;
	}

	/**
	 * From value.
	 *
	 * @param v the value
	 * @return the type of process value
	 */
	public static TypeOfProcessValue fromValue(String v) {
		for (TypeOfProcessValue c : TypeOfProcessValue.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
