package de.fzk.iai.ilcd.service.client.impl.vo.networknode;

import static de.fzk.iai.ilcd.service.client.impl.vo.networknode.HeartbeatVO.LATEST_FIRST_COMPARATOR;
import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.NodeInfo;
import de.fzk.iai.ilcd.service.model.IHeartbeatVO;
import de.fzk.iai.ilcd.service.model.INetworkNodeVO;
import de.fzk.iai.ilcd.service.model.INodeInfo;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(FIELD)
@XmlRootElement(name = "networkNode", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
@XmlType(propOrder = {"nodeInfo", "heartbeats", "alive", "lastSeen"})
public class NetworkNodeStatusVO implements INetworkNodeVO {

  @XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
  private NodeInfo nodeInfo;

  @XmlElementWrapper(name = "heartbeats", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
  @XmlElement(name = "heartbeat", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
  private List<HeartbeatVO> heartbeats;

  @XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
  private Boolean alive;

  @XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NetworkNode")
  private Instant lastSeen;

  public NetworkNodeStatusVO() {
  }

  public NetworkNodeStatusVO(NodeInfo nodeInfo, List<HeartbeatVO> heartbeats) {
    setNodeInfo(nodeInfo);
    setHeartbeats(heartbeats);
    initDerivedFields();
  }

  @SuppressWarnings("unused")
  public NetworkNodeStatusVO(NodeInfo nodeInfo, List<HeartbeatVO> heartbeats, Boolean alive,
      Instant lastSeen) {
    setNodeInfo(nodeInfo);
    setHeartbeats(this.heartbeats = heartbeats);
    setAlive(this.alive = alive);
    setLastSeen(lastSeen);
  }

  private void initDerivedFields() {
    if (this.heartbeats != null && !this.heartbeats.isEmpty()) {
      if (this.alive == null) {
        List<HeartbeatVO> beats = this.heartbeats.stream()
                                        .filter(Objects::nonNull)
                                        .sorted(LATEST_FIRST_COMPARATOR)
                                        .collect(Collectors.toList());
        if (!beats.isEmpty()) {
          setAlive(beats.get(0).isAlive());
        }
        setAlive(this.alive != null ? this.alive : this.heartbeats.get(0).isAlive());
      }

      if (this.lastSeen == null) {
        Instant lastSeen = null;
        List<HeartbeatVO> aliveBeats = this.heartbeats.stream()
                                                      .filter(Objects::nonNull)
                                                      .filter(HeartbeatVO::isAlive)
                                                      .sorted(LATEST_FIRST_COMPARATOR)
                                                      .collect(Collectors.toList());
        if (!aliveBeats.isEmpty()) {
          lastSeen = aliveBeats.get(0).getTime();
        }
        setLastSeen(lastSeen);
      }
    }
  }

  @Override
  public List<IHeartbeatVO> getIHeartbeats() {
    return this.heartbeats.stream().map(IHeartbeatVO.class::cast).collect(Collectors.toList());
  }

  public List<HeartbeatVO> getHeartbeats() {
    return this.heartbeats;
  }

  public void setHeartbeats(List<HeartbeatVO> heartbeats) {
    List<HeartbeatVO> sorted = null;
    if (heartbeats != null) {
      sorted = heartbeats.stream().filter(Objects::nonNull) // let's not allow null heartbeats
                         .sorted(LATEST_FIRST_COMPARATOR) // let's have it sorted always
                         .collect(Collectors.toList());
    }
    this.heartbeats = sorted;
  }

  @Override
  public INodeInfo getINodeInfo() {
    return this.nodeInfo;
  }

  public NodeInfo getNodeInfo() {
    return nodeInfo;
  }

  public void setNodeInfo(NodeInfo nodeInfo) {
    this.nodeInfo = nodeInfo;
  }

  @SuppressWarnings("unused")
  public Boolean getAlive() {
    return this.isAlive();
  }

  public void setAlive(Boolean alive) {
    this.alive = alive;
  }

  @Override
  public Boolean isAlive() {
    initDerivedFields();
    return this.alive;
  }

  @Override
  public Instant getLastSeen() {
    initDerivedFields();
    return this.lastSeen;
  }

  public void setLastSeen(Instant lastSeen) {
    this.lastSeen = lastSeen;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    NetworkNodeStatusVO that = (NetworkNodeStatusVO) o;
    return Objects.equals(getNodeInfo(), that.getNodeInfo()) && Objects.equals(getHeartbeats(),
        that.getHeartbeats());
  }

  @Override
  public int hashCode() {
    int result = Objects.hashCode(getNodeInfo());
    result = 31 * result + Objects.hashCode(getHeartbeats());
    return result;
  }

  @Override
  public String toString() {
    return "NetworkNodeStatusVO{" + "nodeInfo=" + nodeInfo + ", heartbeats=" + heartbeats
           + ", alive=" + alive + ", lastSeen=" + lastSeen + '}';
  }
}
