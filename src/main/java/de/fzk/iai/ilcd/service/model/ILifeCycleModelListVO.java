package de.fzk.iai.ilcd.service.model;

import java.util.List;

import de.fzk.iai.ilcd.service.model.common.IGlobalReference;

public interface ILifeCycleModelListVO extends IDataSetListVO, IDeclaresCompliance {

	public static final String URL_SUFFIX = "lifecyclemodels";
	
	public abstract List<IGlobalReference> getReferenceToResultingProcess();
	public abstract List<IGlobalReference> getReferenceToExternalDocumentation();
	public abstract List<IGlobalReference> getReferenceToProcess(); // foreach processInstance
	public abstract List<IGlobalReference> getReferenceToDiagram();
	public abstract List<IGlobalReference> getReferenceToOwnershipOfDataSet();
}