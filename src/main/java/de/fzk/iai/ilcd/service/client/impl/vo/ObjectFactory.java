/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl.vo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {
	private final static QName _Categories_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI", "categories");

	private final static QName _CategorySystems_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI", "categorySystems");

	private final static QName _AuthenticationInfo_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI", "authenticationInfo");

	private final static QName _DataStockList_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI", "dataStockList");


	public AuthenticationInfo authenticationInfo() {
		return new AuthenticationInfo();
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI", name = "categories")
	public JAXBElement<CategoryList> createCategorySystem(CategoryList value) {
		return new JAXBElement<CategoryList>(_Categories_QNAME, CategoryList.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI", name = "categorySystems")
	public JAXBElement<CategorySystemList> createCategorySystemsList(CategorySystemList value) {
		return new JAXBElement<CategorySystemList>(_CategorySystems_QNAME, CategorySystemList.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI", name = "integers")
	public JAXBElement<IntegerList> createIntegerList(IntegerList value) {
		return new JAXBElement<IntegerList>(_Categories_QNAME, IntegerList.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI", name = "strings")
	public JAXBElement<StringList> createIntegerList(StringList value) {
		return new JAXBElement<StringList>(_Categories_QNAME, StringList.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI", name = "authenticationInfo")
	public JAXBElement<AuthenticationInfo> createAuthenticationInfo(AuthenticationInfo value) {
		return new JAXBElement<AuthenticationInfo>(_AuthenticationInfo_QNAME, AuthenticationInfo.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI", name = "dataStockList")
	public JAXBElement<DataStockList> createDataStockList(DataStockList value) {
		return new JAXBElement<DataStockList>(_DataStockList_QNAME, DataStockList.class, null, value);
	}

}
