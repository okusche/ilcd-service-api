/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.fzk.iai.ilcd.service.client.impl.vo.types.process;

import java.util.Comparator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.collections4.comparators.NullComparator;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.model.enums.ComplianceValue;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;

/**
 * 
 * @author clemens.duepmeier
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplianceSystemType", propOrder = { "reference", "overallCompliance", "nomenclatureCompliance", "methodologicalCompliance",
		"reviewCompliance", "documentationCompliance", "qualityCompliance" })
public class ComplianceSystemType implements IComplianceSystem, Comparable<IComplianceSystem> {

	@SuppressWarnings("unchecked")
	public static final Comparator<String> NULL_SAFE_COMPARATOR = new NullComparator(String.CASE_INSENSITIVE_ORDER);

	@XmlAttribute
	protected String name;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected GlobalReferenceType reference = null;

	protected ComplianceValue overallCompliance;

	protected ComplianceValue nomenclatureCompliance;

	protected ComplianceValue methodologicalCompliance;

	protected ComplianceValue reviewCompliance;

	protected ComplianceValue documentationCompliance;

	protected ComplianceValue qualityCompliance;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ComplianceValue getDocumentationCompliance() {
		return documentationCompliance;
	}

	public void setDocumentationCompliance(ComplianceValue documentationCompliance) {
		this.documentationCompliance = documentationCompliance;
	}

	public ComplianceValue getMethodologicalCompliance() {
		return methodologicalCompliance;
	}

	public void setMethodologicalCompliance(ComplianceValue methodologicalCompliance) {
		this.methodologicalCompliance = methodologicalCompliance;
	}

	public ComplianceValue getNomenclatureCompliance() {
		return nomenclatureCompliance;
	}

	public void setNomenclatureCompliance(ComplianceValue nomenclatureCompliance) {
		this.nomenclatureCompliance = nomenclatureCompliance;
	}

	public ComplianceValue getOverallCompliance() {
		return overallCompliance;
	}

	public void setOverallCompliance(ComplianceValue overallCompliance) {
		this.overallCompliance = overallCompliance;
	}

	public ComplianceValue getQualityCompliance() {
		return qualityCompliance;
	}

	public void setQualityCompliance(ComplianceValue qualityCompliance) {
		this.qualityCompliance = qualityCompliance;
	}

	public ComplianceValue getReviewCompliance() {
		return reviewCompliance;
	}

	public void setReviewCompliance(ComplianceValue reviewCompliance) {
		this.reviewCompliance = reviewCompliance;
	}

	public GlobalReferenceType getReference() {
		return reference;
	}

	public void setReference(GlobalReferenceType reference) {
		this.reference = reference;
	}

	public int compareTo(IComplianceSystem other) {
		return NULL_SAFE_COMPARATOR.compare(this.name, other.getName());
	}
}
