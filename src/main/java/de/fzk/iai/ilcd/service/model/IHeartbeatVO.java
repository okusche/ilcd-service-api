package de.fzk.iai.ilcd.service.model;

import java.time.Instant;

public interface IHeartbeatVO {

  Instant getTime();

  Boolean isAlive();

}
