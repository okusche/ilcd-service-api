/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model.process;

import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.enums.ComplianceValue;

/**
 * The Interface IComplianceSystem.
 * 
 * @author clemens.duepmeier
 */
public interface IComplianceSystem extends Comparable<IComplianceSystem> {

	/**
	 * Gets the name of the compliance system.
	 * 
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * Gets the reference to the corresponding source data set.
	 * 
	 * @return the reference
	 */
	public abstract IGlobalReference getReference();

	/**
	 * Gets the the overall compliance of a data set according to this
	 * compliance system.
	 * 
	 * @return the overall compliance
	 */
	public abstract ComplianceValue getOverallCompliance();

	/**
	 * Gets the compliance in regards to a nomenclature specification.
	 * 
	 * @return the nomenclature compliance
	 */
	public abstract ComplianceValue getNomenclatureCompliance();

	/**
	 * Gets the compliance in regards to methodology.
	 * 
	 * @return the methodological compliance
	 */
	public abstract ComplianceValue getMethodologicalCompliance();

	/**
	 * Gets the compliance in regards to review.
	 * 
	 * @return the review compliance
	 */
	public abstract ComplianceValue getReviewCompliance();

	/**
	 * Gets the compliance in regards to documentation.
	 * 
	 * @return the documentation compliance
	 */
	public abstract ComplianceValue getDocumentationCompliance();

	/**
	 * Gets the compliance in regards to quality criteria.
	 * 
	 * @return the quality compliance
	 */
	public abstract ComplianceValue getQualityCompliance();
	
	/**
	 * For satisfying Comparable interface
	 * 
	 * @return the comparison result
	 */
	public int compareTo(IComplianceSystem other);

}
