/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 *
 * This file is part of the Java Service API for ILCD.
 *
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl;

import de.fzk.iai.ilcd.service.client.impl.vo.networknode.NetworkNodeStatusListVO;
import de.fzk.iai.ilcd.service.client.impl.vo.networknode.NetworkNodeStatusVO;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.Logger;

import de.fzk.iai.ilcd.service.client.impl.vo.AuthenticationInfo;
import de.fzk.iai.ilcd.service.client.impl.vo.CategoryList;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockList;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockVO;
import de.fzk.iai.ilcd.service.client.impl.vo.IntegerList;
import de.fzk.iai.ilcd.service.client.impl.vo.StringList;
import de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.NodeInfo;
import de.fzk.iai.ilcd.service.model.ICategoryList;
import de.fzk.iai.ilcd.service.model.IDataStockListVO;
import de.fzk.iai.ilcd.service.model.IDataStockVO;
import de.fzk.iai.ilcd.service.model.IIntegerList;

/**
 * The Class ServiceDAO.
 */
public class ServiceDAO extends AbstractServiceDAO {

  final Logger log = org.apache.logging.log4j.LogManager.getLogger(this.getClass());

  private JAXBContext context;

  /**
   * Instantiates a new service dao.
   */
  public ServiceDAO() {
    try {
      this.context = createContext();
    } catch (JAXBException e) {
      log.error("could not create JAXB context", e);
    }
  }

  public void marshal(Object obj, OutputStream stream) throws JAXBException {

    String schemaLocation = null;
    if (renderSchemaLocation) {
      if (obj instanceof NodeInfo) {
        schemaLocation = "http://www.ilcd-network.org/ILCD/ServiceAPI ../schemas/ILCD_Service_API_NodeInfo.xsd";
      } else if (obj instanceof AuthenticationInfo) {
        schemaLocation = "http://www.ilcd-network.org/ILCD/ServiceAPI ../schemas/ILCD_Service_API_AuthInfo.xsd";
      } else if (obj instanceof IDataStockListVO || obj instanceof IDataStockVO) {
        schemaLocation = "http://www.ilcd-network.org/ILCD/ServiceAPI ../schemas/ILCD_Service_API_DataStocks.xsd";
      } else if (obj instanceof ICategoryList) {
        schemaLocation = "http://www.ilcd-network.org/ILCD/ServiceAPI ../schemas/ILCD_Service_API_Categories.xsd";
      } else if (obj instanceof IIntegerList) {
        schemaLocation = "http://www.ilcd-network.org/ILCD/ServiceAPI ../schemas/ILCD_Service_API_Integers.xsd";
      }
    }
    super.marshal(obj, stream, this.context, schemaLocation);
  }

  /**
   * Unmarshal.
   *
   * @param stream the stream
   * @return the object
   */
  public Object unmarshal(InputStream stream) {

    try {
      return super.unmarshal(stream, this.context);
    } catch (JAXBException e) {
      log.error("cannot unmarshal dataset", e);
      return null;
    }
  }

  private JAXBContext createContext() throws JAXBException {
    return JAXBContext.newInstance(NodeInfo.class, AuthenticationInfo.class, DataStockVO.class,
        DataStockList.class, CategoryList.class, IntegerList.class, StringList.class,
        NetworkNodeStatusVO.class, NetworkNodeStatusListVO.class);
  }

}
