/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.types.process;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.LicenseTypeValue;
import de.fzk.iai.ilcd.service.model.process.IAccessInformation;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessInformationType", propOrder = { "copyright", "licenseType", "useRestrictions" })
public class AccessInformationType implements IAccessInformation {

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected Boolean copyright;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected LicenseTypeValue licenseType;

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected List<LString> useRestrictions;

	public boolean isCopyright() {
		return copyright;
	}

	public void setCopyright(boolean copyright) {
		this.copyright = copyright;
	}

	public LicenseTypeValue getLicenseType() {
		return this.licenseType;
	}

	public void setLicenseType(LicenseTypeValue licenseType) {
		this.licenseType = licenseType;
	}

	public IMultiLangString getUseRestrictions() {
		if (useRestrictions == null)
			useRestrictions = new ArrayList<LString>();
		return new MultiLangString(useRestrictions);
	}

	protected void setUseRestrictions(List<LString> useRestrictions) {
		this.useRestrictions = useRestrictions;
	}

	public void addUseRestrictions(LString useRestrictions) {
		if (this.useRestrictions == null)
			this.useRestrictions = new ArrayList<LString>();
		useRestrictions.insertWithoutDuplicates(this.useRestrictions);
	}

	public void setUseRestrictions(String lang, String restrictions) {
		LString lString = new LString(lang, restrictions);
		this.addUseRestrictions(lString);
	}

	public void setUseRestrictions(String useRestrictionsDefault) {
		LString lString = new LString(useRestrictionsDefault);
		this.addUseRestrictions(lString);

	}

}
