/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.model.IAuthenticationInfo;

@XmlRootElement(name = "authInfo")
@XmlType(propOrder = { "authenticated", "userName", "roles" , "dataStockRoles" })
public class AuthenticationInfo implements IAuthenticationInfo {

	protected boolean authenticated = false;

	protected String userName;

	@XmlElement(name = "role")
	protected List<String> roles;

	@XmlElement(name = "dataStock")
	protected List<DataStockVO> dataStockRoles;

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public List<String> getRoles() {
		if (roles == null)
			roles = new ArrayList<String>();
		return roles;
	}

	public List<DataStockVO> getDataStockRoles() {
		if (dataStockRoles == null)
			dataStockRoles = new ArrayList<DataStockVO>();
		return dataStockRoles;
	}
}
