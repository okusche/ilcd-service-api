@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(type = Instant.class, value = InstantAdapter.XmlAdapter.class)
})
package de.fzk.iai.ilcd.service.model;

import de.fzk.iai.ilcd.service.client.impl.adapter.InstantAdapter;
import java.time.Instant;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
