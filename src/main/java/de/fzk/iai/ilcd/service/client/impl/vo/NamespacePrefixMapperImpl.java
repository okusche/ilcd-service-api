/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo;

import org.apache.logging.log4j.Logger;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.NodeInfo;

public class NamespacePrefixMapperImpl extends NamespacePrefixMapper {
	protected static final Logger log = org.apache.logging.log4j.LogManager.getLogger(NamespacePrefixMapperImpl.class);

	private String defaultNameSpaceURI = null;

	public NamespacePrefixMapperImpl(Object obj) {

		if (obj instanceof DataSetVO)
			this.defaultNameSpaceURI = ((DataSetVO) obj).getNameSpaceURI();
		else if (obj instanceof DataSetList || obj instanceof NodeInfo || obj instanceof AuthenticationInfo)
			this.defaultNameSpaceURI = Constants.NS_SERVICE_API;
	}

	public NamespacePrefixMapperImpl() {
	}

	/**
	 * Returns a preferred prefix for the given namespace URI.
	 * 
	 * This method is intended to be overrided by a derived class.
	 * 
	 * @param namespaceUri
	 *            The namespace URI for which the prefix needs to be found. Never be null. "" is used to denote the
	 *            default namespace.
	 * @param suggestion
	 *            When the content tree has a suggestion for the prefix to the given namespaceUri, that suggestion is
	 *            passed as a parameter. Typicall this value comes from the QName.getPrefix to show the preference of
	 *            the content tree. This parameter may be null, and this parameter may represent an already occupied
	 *            prefix.
	 * @param requirePrefix
	 *            If this method is expected to return non-empty prefix. When this flag is true, it means that the given
	 *            namespace URI cannot be set as the default namespace.
	 * 
	 * @return null if there's no prefered prefix for the namespace URI. In this case, the system will generate a prefix
	 *         for you.
	 * 
	 *         Otherwise the system will try to use the returned prefix, but generally there's no guarantee if the
	 *         prefix will be actually used or not.
	 * 
	 *         return "" to map this namespace URI to the default namespace. Again, there's no guarantee that this
	 *         preference will be honored.
	 * 
	 *         If this method returns "" when requirePrefix=true, the return value will be ignored and the system will
	 *         generate one.
	 */
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {

		log.trace("mapping namespaceUri " + namespaceUri + ", suggestion is " + suggestion + "(default: "
				+ this.defaultNameSpaceURI + ")");

		String result = null;

		if (namespaceUri.equals(this.defaultNameSpaceURI) && !requirePrefix)
			return "";

		if (Constants.NS_SCHEMA_INSTANCE.equals(namespaceUri))
			result = "xsi";

		else if (namespaceUri.equals("http://www.w3.org/1999/xlink"))
			result = "xlink";

		else if (Constants.NS_SERVICE_API.equals(namespaceUri))
			result = "sapi";

		else if (Constants.NS_SERVICE_API_PROCESS.equals(namespaceUri))
			result = "p";

		else if (Constants.NS_SERVICE_API_PROCESS_V2.equals(namespaceUri))
			result = "p2";

		else if (Constants.NS_SERVICE_API_PROCESS_V3.equals(namespaceUri))
			result = "p3";

		else if (Constants.NS_SERVICE_API_LCIAMETHOD.equals(namespaceUri))
			result = "l";

		else if (Constants.NS_SERVICE_API_FLOW.equals(namespaceUri))
			result = "f";

		else if (Constants.NS_SERVICE_API_FLOWPROPERTY.equals(namespaceUri))
			result = "fp";

		else if (Constants.NS_SERVICE_API_UNITGROUP.equals(namespaceUri))
			result = "u";

		else if (Constants.NS_SERVICE_API_SOURCE.equals(namespaceUri)) 
			result = "s";
		
		else if (Constants.NS_SERVICE_API_CONTACT.equals(namespaceUri))
			result = "c";

		else if (Constants.NS_SERVICE_API_LIFECYCLEMODEL.equals(namespaceUri))
			result = "lcm";

		else if (Constants.NS_SERVICE_API_DATASTOCK.equals(namespaceUri))
			result = "ds";

		else if (Constants.NS_SERVICE_API_NODEINFO.equals(namespaceUri))
			result = "ni";

		else if (Constants.NS_EPD_2013.equals(namespaceUri))
			result = "epd";
		
		if (result != null) {
			log.trace("returning \"" + result + "\"");
			return result;
		} else {
			log.trace("result is null, returning " + suggestion);
			return suggestion;
		}
	}

	/**
	 * Returns a list of namespace URIs that should be declared at the root element.
	 */
	public String[] getPreDeclaredNamespaceUris() {
		// return new String[] { Constants.NS_SCHEMA_INSTANCE };
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sun.xml.bind.marshaller.NamespacePrefixMapper#getContextualNamespaceDecls ()
	 */
	@Override
	public String[] getContextualNamespaceDecls() {
		return super.getContextualNamespaceDecls();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seecom.sun.xml.bind.marshaller.NamespacePrefixMapper# getPreDeclaredNamespaceUris2()
	 */
	@Override
	public String[] getPreDeclaredNamespaceUris2() {
		return super.getPreDeclaredNamespaceUris2();
	}
}
