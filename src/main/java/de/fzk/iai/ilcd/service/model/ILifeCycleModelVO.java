package de.fzk.iai.ilcd.service.model;

import java.util.List;

import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.process.IReview;

public interface ILifeCycleModelVO extends IDataSetVO, ILifeCycleModelListVO {

	/**
	 * Gets the base name.
	 * 
	 * General descriptive name of the life cycle model and/or its main good(s),
	 * service(s) and/or functions delivered.
	 * 
	 * 
	 * @return IMultiLangString containing only the base name part of the
	 *         lifecyclemodel name
	 */
	public abstract IMultiLangString getBaseName();

	/**
	 * Gets the treatmentStandardsRoutes.
	 * 
	 * Specifying information on the good, service, or function delivered by the
	 * life cycle model in technical term(s): treatment received, standard
	 * fulfilled, product quality, use information, production route name, educt
	 * name, primary / secondary etc. Separated by commata.
	 * 
	 * 
	 * 
	 * @return IMultiLangString containing treatmentStandardsRoutes
	 */
	public abstract IMultiLangString getTreatmentStandardsRoutes();

	/**
	 * Gets the mixAndLocationTypes.
	 * 
	 * Specifying information on the good, service, or function, whether being a
	 * production mix or consumption mix, location type of availability (such as
	 * e.g. "to consumer" or "at plant"). Separated by commata. May include
	 * information of excluded life cycle stages, if any.
	 * 
	 * 
	 * @return IMultiLangString containing mixAndLocationTypes
	 */
	public abstract IMultiLangString getMixAndLocationTypes();

	/**
	 * Gets the getFunctionalUnitFlowProperties.
	 * 
	 * Further, quantitative specifying information on the good, service or function
	 * in technical term(s): qualifying constituent(s)-content and / or
	 * energy-content per unit etc. as appropriate. Separated by commata. (Note:
	 * non-qualifying flow properties, CAS No, Synonyms, Chemical formulas etc. are
	 * to be documented exclusively in the "Flow data set" of the reference flow of
	 * this life cycle model.)
	 * 
	 * 
	 * @return IMultiLangString containing only the base name part of the
	 *         lifecyclemodel name
	 */
	public abstract IMultiLangString getFunctionalUnitFlowProperties();

	/**
	 * Gets the reviews.
	 * 
	 * Review information on this life cycle model data set
	 * 
	 * @return list of reviews
	 */
	public abstract List<IReview> getReviews();

}
