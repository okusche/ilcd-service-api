/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo;

/**
 *
 */
public class Constants {

	public static final String NS_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";

	/**
	 * Service API namespace URIs
	 */
	public static final String NS_SERVICE_API_PROCESS = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process";

	public static final String NS_SERVICE_API_PROCESS_V2 = "http://www.ilcd-network.org/ILCD/ServiceAPI/v2/Process";

	public static final String NS_SERVICE_API_PROCESS_V3 = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/Process";

	public static final String NS_SERVICE_API_LCIAMETHOD = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod";

	public static final String NS_SERVICE_API_FLOW = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow";

	public static final String NS_SERVICE_API_FLOWPROPERTY = "http://www.ilcd-network.org/ILCD/ServiceAPI/FlowProperty";

	public static final String NS_SERVICE_API_UNITGROUP = "http://www.ilcd-network.org/ILCD/ServiceAPI/UnitGroup";

	public static final String NS_SERVICE_API_SOURCE = "http://www.ilcd-network.org/ILCD/ServiceAPI/Source";

	public static final String NS_SERVICE_API_CONTACT = "http://www.ilcd-network.org/ILCD/ServiceAPI/Contact";

	public static final String NS_SERVICE_API_LIFECYCLEMODEL = "http://www.ilcd-network.org/ILCD/ServiceAPI/v3/LifeCycleModel";

	public static final String NS_SERVICE_API_NODEINFO = "http://www.ilcd-network.org/ILCD/ServiceAPI/NodeInfo";

	public static final String NS_SERVICE_API_DATASTOCK = "http://www.ilcd-network.org/ILCD/ServiceAPI/DataStock";

	public static final String NS_SERVICE_API = "http://www.ilcd-network.org/ILCD/ServiceAPI";

	public static final String NS_EPD_2013 = "http://www.iai.kit.edu/EPD/2013";
	
	/**
	 * Service API schema names
	 */
	public static final String SERVICE_API_PROCESS_SCHEMA_NAME = "ILCD_Service_API_Process.xsd";

	public static final String SERVICE_API_LCIAMETHOD_SCHEMA_NAME = "ILCD_Service_API_LCIAMethod.xsd";

	public static final String SERVICE_API_FLOW_SCHEMA_NAME = "ILCD_Service_API_Flow.xsd";

	public static final String SERVICE_API_FLOW_PROPERTY_SCHEMA_NAME = "ILCD_Service_API_FlowProperty.xsd";

	public static final String SERVICE_API_UNIT_GROUP_SCHEMA_NAME = "ILCD_Service_API_UnitGroup.xsd";

	public static final String SERVICE_API_SOURCE_SCHEMA_NAME = "ILCD_Service_API_Source.xsd";

	public static final String SERVICE_API_CONTACT_SCHEMA_NAME = "ILCD_Service_API_Contact.xsd";

	public static final String SERVICE_API_LIFECYCLEMODEL_SCHEMA_NAME = "ILCD_Service_API_LifeCycleModel.xsd";

	public static final String SERVICE_API_AUTH_INFO_SCHEMA_NAME = "ILCD_Service_API_AuthenticationInfo.xsd";

	public static final String SERVICE_API_NODE_INFO_SCHEMA_NAME = "ILCD_Service_API_NodeInfo.xsd";

	public static final String SERVICE_API_DATASTOCKS_SCHEMA_NAME = "ILCD_Service_API_DataStocks.xsd";

}
