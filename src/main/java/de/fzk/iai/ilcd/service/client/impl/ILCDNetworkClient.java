/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for
 * Applied Computer Science (IAI).
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.List;

import javax.ws.rs.client.*;
import javax.ws.rs.core.*;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import de.fzk.iai.ilcd.service.model.*;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.Response;


import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.flow.FlowDataSet;
import de.fzk.iai.ilcd.api.app.flowproperty.FlowPropertyDataSet;
import de.fzk.iai.ilcd.api.app.lciamethod.LCIAMethodDataSet;
import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.api.app.unitgroup.UnitGroupDataSet;
import de.fzk.iai.ilcd.api.binding.helper.DatasetDAO;
import de.fzk.iai.ilcd.api.dataset.DataSet;
import de.fzk.iai.ilcd.service.client.AccessDeniedException;
import de.fzk.iai.ilcd.service.client.DatasetNotFoundException;
import de.fzk.iai.ilcd.service.client.FailedAuthenticationException;
import de.fzk.iai.ilcd.service.client.FailedConnectionException;
import de.fzk.iai.ilcd.service.client.ILCDServiceClient;
import de.fzk.iai.ilcd.service.client.ILCDServiceClientException;
import de.fzk.iai.ilcd.service.client.NotPermittedException;
import de.fzk.iai.ilcd.service.client.impl.vo.AuthenticationInfo;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockList;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockVO;
import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.IntegerList;
import de.fzk.iai.ilcd.service.client.impl.vo.Result;
import de.fzk.iai.ilcd.service.client.impl.vo.StringList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.NodeInfo;
import de.fzk.iai.ilcd.service.model.enums.TypeOfFlowValue;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/***
 * This is a client implementation for the ILCD Service API. It provides methods to read and write data sets or lists of
 * data sets from and to a database
 * service.
 * 
 * <b>Common usage scenarios</b>
 * 
 * <b>Create client</b> First, create a client, either without authentication
 * 
 * <pre>
 * 
 * 
 * 
 * 
 * 
 * ILCDNetworkClient client = new ILCDNetworkClient( &quot;http://www.acme.org/ILCD/resource/&quot; );
 * </pre>
 * 
 * or with authentication
 * 
 * <pre>
 * 
 * 
 * 
 * 
 * 
 * ILCDNetworkClient client = new ILCDNetworkClient( &quot;http://www.acme.org/ILCD/resource/&quot;, &quot;myusername&quot;, &quot;mypa22w0rd&quot; );
 * </pre>
 * 
 * <b>Retrieve a list of all datasets of a certain type (in this example, process)</b>
 * 
 * <pre>
 * 
 * 
 * 
 * 
 * 
 * Result&lt;IProcessListVO&gt; results = client.getDataSets( IProcessListVO.class );
 * </pre>
 * 
 * <b>Retrieve a dataset (value object) with the given UUID from the connected node</b>
 * 
 * <pre>
 * 
 * 
 * 
 * 
 * 
 * IProcessVO processVO = connection.getDataSetVO( IProcessVO.class, &quot;00000000-0000-0000-0000-000000000000&quot; );
 * </pre>
 * 
 * <b>Retrieve a dataset (full dataset) with the given UUID from the connected node.</b>
 * 
 * <pre>
 * 
 * 
 * 
 * 
 * 
 * ProcessDataSet result = connection.getDataSetVO( ProcessDataSet.class, &quot;00000000-0000-0000-0000-000000000000&quot; );
 * </pre>
 * 
 * 
 * <b>Retrieve a dataset (full dataset) with the given URL</b>
 * 
 * <pre>
 * 
 * 
 * 
 * 
 * 
 * Dataset process = getFullDataset( &quot;http://www.acme.org/ILCD/resource/processes/00000000-0000-0000-0000-000000000000&quot; );
 * </pre>
 * 
 * 
 * <b>Retrieve a dataset (value object) with the given URL</b>
 * 
 * <pre>
 * 
 * 
 * 
 * 
 * 
 * IDataSetVO process = get( &quot;http://www.acme.org/ILCD/resource/processes/00000000-0000-0000-0000-000000000000&quot; );
 * </pre>
 * 
 * 
 * <b>Performs a query with search parameters</b>
 * 
 * <pre>
 * MultivaluedMap&lt;String, String&gt; queryParams = new MultivaluedMapImpl();
 * queryParams.add( &quot;name&quot;, &quot;electricity&quot; );
 * queryParams.add( &quot;location&quot;, &quot;SK&quot; );
 * 
 * Result&lt;IProcessVO&gt; results = client.query( IProcessVO.class, queryParams );
 * </pre>
 * 
 * optional: execute this query across all known network nodes
 * 
 * <pre>
 * 
 * 
 * 
 * 
 * 
 * Result&lt;IProcessVO&gt; results = client.query( IProcessVO.class, queryParams, true );
 * </pre>
 * 
 * 
 * <b>Store a dataset on the service</b>
 * 
 * <pre>
 * // the variable "process" holds a populated de.fzk.iai.ilcd.api.app.process.ProcessDataSet
 * ProcessDataSet process = ...; 
 * client.putDataSet(process);
 * </pre>
 * 
 * 
 */
public class ILCDNetworkClient implements ILCDServiceClient<IDataSetListVO> {

	private final Logger log = org.apache.logging.log4j.LogManager.getLogger( this.getClass() );

	public static final String VERSION = "ILCD Service API " + ILCDNetworkClient.class.getPackage().getImplementationVersion();

	// usually this will be set to "?", for running the test cases on a plain
	// web service, use "+" instead
	private static final String PARAMETER_SEPARATOR = "?";

	private static final String PARAMETER_CONCATENATOR = "&";

	public static final String MODE_OVERVIEW = "view=overview";

	public static final String MODE_FULL = "format=xml&view=full";

	public static final String QUERY_COMMAND = "search";

	public static final String DISTRIBUTED_QUERY = "distributed";

	public static final String START_INDEX = "startIndex";

	public static final String PAGE_SIZE = "pageSize";

	public static final String NODE_INFO = "nodeinfo";

	public static final String AUTHENTICATION_STATUS = "authenticate/status";

	public static final String EXCHANGES = "exchanges";

	public static final String EXCHANGES_DIRECTION = "direction";

	public static final String EXCHANGES_DIRECTION_IN = "in";

	public static final String EXCHANGES_DIRECTION_OUT = "out";

	public static final String EXCHANGES_TYPE = "type";

	public static final String PRODUCERS = "producers";

	public static final String CONSUMERS = "consumers";
	
	public static final String SOURCE_WITH_BINARIES = "withBinaries";

	private String baseUrl;

	private String nodeName;

	public Client client;

	private DatasetDAO dao;

	private DatasetVODAO voDao;
	
	private ServiceDAO sDao;

	/***
	 * Default constructor
	 * 
	 */
	public ILCDNetworkClient() {
		log.info(VERSION);
		voDao = new DatasetVODAO();
		dao = new DatasetDAO();
		sDao = new ServiceDAO();
		CookieHandler.setDefault(new CookieManager());
	}

	/***
	 * Open a connection to a node with the given base URL
	 * 
	 * @param baseUrl
	 *            the base URL
	 * @throws FailedAuthenticationException
	 * @throws FailedConnectionException
	 */
	public ILCDNetworkClient( String baseUrl ) throws ILCDServiceClientException {
		this();
		open( baseUrl );
	}

	/***
	 * Open a connection to a node with the given base URL and authentication details
	 * 
	 * @param baseUrl
	 *            the base URL
	 */
	public ILCDNetworkClient( String baseUrl, String username, String password ) throws ILCDServiceClientException {
		this();
		open( baseUrl, username, password );
	}
	
	public void setOrigin(String origin) {
		this.client.register(new OriginRequestFilter(origin));
	}

	/***
	 * Open a connection to a node with the given base URL and authentication credentials
	 * 
	 * @param baseUrl
	 *            the base URL
	 * @param username
	 *            the username to authenticate with
	 * @param password
	 *            the password to authenticate with
	 * @throws FailedAuthenticationException
	 * @throws FailedConnectionException
	 */

	public void open(String baseUrl, String username, String password) throws ILCDServiceClientException {
		baseUrl = fixURL(baseUrl);
		try {
			URL url = new URL(baseUrl);
			URLConnection conn = url.openConnection();
			conn.connect();
		} catch (MalformedURLException e) {
			throw new FailedConnectionException("Cannot connect, malformed URL", e);
		} catch (UnknownHostException e) { // TODO: combine 2 exceptions below whenever compiler compliance > 1.5
			throw new FailedConnectionException("Host is down or taking too long to respond", e);
		} catch (IOException e) {
			throw new FailedConnectionException("Host is down or taking too long to respond", e);
		}
		if (log.isInfoEnabled())
			log.info("opening URL " + baseUrl);
		this.baseUrl = baseUrl;
		if (baseUrl == null || username == null)
			this.client = buildClient();
		else
			this.client = buildClient(baseUrl, username, password);
	}

	/***
	 * Open a connection to a node with the given base URL
	 * 
	 * @param baseUrl
	 *            the base URL
	 * @throws FailedAuthenticationException
	 * @throws FailedConnectionException
	 * @throws FailedAuthenticationException
	 */
	public void open( String baseUrl ) throws ILCDServiceClientException {
		open( baseUrl, null, null );
	}

	/***
	 * 
	 * Checks if a connection has been established
	 * 
	 * @return true if the client is already connected
	 */
	public boolean isOpen() {
		return (this.baseUrl != null && this.client != null);
	}

	/**
	 * Retrieve a data set value object (VO) from the service by its UUID.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param id
	 *            the UUID
	 * @return the data set value object
	 * 
	 */
	public <T extends IDataSetVO> T getDataSetVO( Class<T> clazz, String id ) throws ILCDServiceClientException, IOException {
		return getDataSetVO( clazz, id, null );
	}

	/**
	 * Retrieve a data set value object (VO) from the service by its UUID and version number.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param id
	 *            the UUID
	 * @param version
	 *            the version number
	 * @return the data set value object
	 * 
	 */
	@SuppressWarnings( "unchecked" )
	public <T extends IDataSetVO> T getDataSetVO( Class<T> clazz, String id, String version ) throws ILCDServiceClientException, IOException {
		URL url = buildGETURL( clazz, id, version, MODE_OVERVIEW );
		if (log.isDebugEnabled())
			log.debug( "reading from " + url.toString() );
		T result = null;
		try {
			result = (T) voDao.unmarshal( url.openStream() );
		}
		catch ( FileNotFoundException e ) {
			throw new DatasetNotFoundException( e );
		}
		catch ( JAXBException e ) {
			log.error( "error unmarshalling", e );
		}
		return result;
	}

	/**
	 * Retrieve a data set value object (VO) as InputStream from the service by its UUID.
	 * 
	 * @param <T>
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param id
	 *            the UUID
	 * @return the InputStream
	 * 
	 */
	public <T extends IDataSetVO> InputStream getDataSetVOAsStream( Class<T> clazz, String id ) throws ILCDServiceClientException, IOException {
		return getDataSetVOAsStream( clazz, id, null );
	}

	/**
	 * Retrieve a data set value object (VO) as InputStream from the service by its UUID and version number.
	 * 
	 * @param <T>
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param id
	 *            the UUID
	 * @return the InputStream
	 * 
	 */
	public <T extends IDataSetVO> InputStream getDataSetVOAsStream( Class<T> clazz, String id, String version ) throws ILCDServiceClientException, IOException {
		URL url = buildGETURL( clazz, id, version, MODE_OVERVIEW );
		if (log.isDebugEnabled())
			log.debug( "reading from " + url.toString() );
		try {
			return url.openStream();
		}
		catch ( FileNotFoundException e ) {
			throw new DatasetNotFoundException( e );
		}
	}

	/**
	 * Retrieve a data set (full dataset) from the service by its UUID.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param id
	 *            the UUID
	 * @return the data set
	 * @throws AccessDeniedException 
	 * 
	 */
	public <T extends DataSet> T getDataSet( Class<T> clazz, String id ) throws ILCDServiceClientException, IOException {
		return getDataSet( clazz, id, null );
	}

	/**
	 * Retrieve a data set (full dataset) from the service by its UUID.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param id
	 *            the UUID
	 * @return the data set
	 * 
	 */
	@SuppressWarnings( "unchecked" )
	public <T extends DataSet> T getDataSet( Class<T> clazz, String id, String version ) throws ILCDServiceClientException, IOException {
		URL url = buildGETURL( clazz, id, version, MODE_FULL );
		if (log.isDebugEnabled())
			log.debug( "reading from " + url.toString() );
		T result = null;
		try {
			Response response = client.target( url.toString() ).request(MediaType.APPLICATION_XML_TYPE).get();
			if (response.getStatus() == 200) {
				InputStream is = response.readEntity(InputStream.class);
				result = (T) dao.openDataset( is );
			} else if (response.getStatus() == 401) {
				throw new AccessDeniedException(response.toString());
			} else if (response.getStatus() == 403) {
				throw new NotPermittedException(response.toString());
			} else if (response.getStatus() == 404) {
				throw new DatasetNotFoundException(response.toString());
			} else {
				log.error(response);
			}
		}
		catch ( DatasetNotFoundException e ) {
			 throw new DatasetNotFoundException( e );
		}
		catch ( Exception e ) {
			log.error( e );
		}

		return result;
	}

	/**
	 * Retrieve a data set (full dataset) as InputStream from the service by its UUID.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param id
	 *            the UUID
	 * @return the InputStream
	 * 
	 */
	public <T extends DataSet> InputStream getDataSetAsStream( Class<T> clazz, String id ) throws ILCDServiceClientException, IOException {
		return getDataSetAsStream( clazz, id, null );
	}

	/**
	 * Retrieve a data set (full dataset) as InputStream from the service by its UUID.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param id
	 *            the UUID
	 * @return the InputStream
	 * 
	 */
	public <T extends DataSet> InputStream getDataSetAsStream( Class<T> clazz, String id, String version ) throws ILCDServiceClientException, IOException {
		URL url = buildGETURL( clazz, id, version, MODE_FULL );
		if (log.isDebugEnabled())
			log.debug( "reading from " + url.toString() );
		try {
			return url.openStream();
		}
		catch ( FileNotFoundException e ) {
			throw new DatasetNotFoundException( e );
		}
	}

	/**
	 * Retrieve a list of all data set value objects (VO) from the service for the specified data set type.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @return the list of data set value objects
	 * 
	 */
	public <T extends IDataSetListVO> Result<T> getDataSets( Class<T> clazz ) throws IOException {
		return getDataSets( clazz, null, null );
	}

	/**
	 * Retrieve a list of all data set value objects (VO) from the service for the specified data set type.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @return the list of data set value objects
	 * 
	 */
	public <T extends IDataSetListVO> Result<T> getDataSets( Class<T> clazz, Integer startIndex, Integer pageSize ) throws IOException {
		URL url = buildGETURL( clazz, startIndex, pageSize );
		if (log.isDebugEnabled())
			log.debug( "reading from " + url.toString() );
		DataSetList list = null;
		try {
			list = (DataSetList) voDao.unmarshal( url.openStream() );
		}
		catch ( JAXBException e ) {
			log.error( "error unmarshalling", e );
			return null;
		}
		this.nodeName = list.getSourceId();
		if (log.isDebugEnabled())
			log.debug( "node name is " + this.nodeName );

		return new Result<T>( list );
	}
	
	/**
	 * Retrieve a list of all data stocks.
	 * 
	 * @return the list of data stocks
	 * 
	 */
	public DataStockList getDataStocks( ) throws IOException {
		
		WebTarget webResource = this.client.target( baseUrl + "datastocks" );

		Object result = webResource.request( MediaType.APPLICATION_XML ).get(DataStockList.class);

		JAXBElement<DataStockList> e = (JAXBElement<DataStockList>) result;

		DataStockList list = e.getValue();

		if (log.isDebugEnabled())
			log.debug( "retrieved " + list.getDataStocks().size() + " data stocks");
		
		return list;
	}

	/**
	 * Retrieve a list of all data set value objects (VO) as InputStream from the service for the specified data set
	 * type.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @return the InputStream
	 * 
	 */
	public <T extends IDataSetListVO> InputStream getDataSetsAsStream( Class<T> clazz ) throws IOException {
		return getDataSetsAsStream( clazz, null, null );
	}

	/**
	 * Retrieve a list of all data set value objects (VO) as InputStream from the service for the specified data set
	 * type.
	 * 
	 * @param clazz
	 *            the desired data set type
	 * @param startIndex
	 *            the start index of the result set (defaults to 0 if null)
	 * @param pageSize
	 *            the page size of the result set returned by the service
	 * @return the InputStream
	 * 
	 */
	public <T extends IDataSetListVO> InputStream getDataSetsAsStream( Class<T> clazz, Integer startIndex, Integer pageSize ) throws IOException {
		URL url = buildGETURL( clazz, startIndex, pageSize );
		if (log.isDebugEnabled())
			log.debug( "reading from " + url.toString() );
		return url.openStream();
	}

	/**
	 * Retrieve a data set (full dataset) using an absolute URL (which may point to any service).
	 * 
	 * @param href
	 *            the absolute URL
	 * @return the data set
	 * 
	 */
	public DataSet getFullDataset( String href ) throws ILCDServiceClientException, IOException {
		URL url = new URL( href + "?" + MODE_FULL );
		if (log.isDebugEnabled())
			log.debug( "reading from " + url.toString() );
		DataSet result = null;
		try {
			result = (DataSet) dao.openDataset( url );
		}
		catch ( FileNotFoundException e ) {
			throw new DatasetNotFoundException( e );
		}
		return result;
	}

	/**
	 * Retrieve a data set (full dataset) as InputStream using an absolute URL (which may point to any service).
	 * 
	 * @param href
	 *            the absolute URL
	 * @return the InputStream
	 * 
	 */
	public InputStream getFullDatasetAsStream( String href ) throws ILCDServiceClientException, IOException {
		URL url = new URL( href + "?" + MODE_FULL );
		if (log.isDebugEnabled())
			log.debug( "reading from " + url.toString() );
		try {
			return url.openStream();
		}
		catch ( FileNotFoundException e ) {
			throw new DatasetNotFoundException( e );
		}
	}

	public <T extends DataSet> boolean existsDataSet( Class<T> clazz, String uuid, String version ) {
		URI uri;
		
		try {
			uri = buildGETURL(clazz, uuid, version, null).toURI();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		if (log.isDebugEnabled())
			log.debug( "reading from " + uri.toString() );

		WebTarget webResource = this.client.target( uri );

		Response response = webResource.request().head();
		if ( response.getStatus() == Response.Status.OK.getStatusCode() ) {
			return true;
		}
		else if ( response.getStatus() == Response.Status.NOT_FOUND.getStatusCode() ) {
			return false;
		}
		else {
			if (log.isDebugEnabled())
				log.debug( "Unexpected status code: " + Integer.toString( response.getStatus() ) + ". Interpreting as if dataset is not available." );
			return false;
		}
	}

	
	public Result<IFlowListVO> getExchanges( String uuid, String direction, TypeOfFlowValue type ) {
		DataSetList list = null;

		try {
			StringBuffer urlBuf = new StringBuffer( this.baseUrl );

			urlBuf.append( getURLSuffix( IProcessListVO.class ) );
			urlBuf.append( "/" );
			urlBuf.append( uuid );
			urlBuf.append( "/" );
			urlBuf.append( EXCHANGES );
			if ( direction != null || type != null ) {
				urlBuf.append( "?" );
				if ( direction != null ) {
					urlBuf.append( EXCHANGES_DIRECTION );
					urlBuf.append( "=" );
					urlBuf.append( direction );
				}
				if ( type != null ) {
					urlBuf.append( PARAMETER_CONCATENATOR );
					urlBuf.append( EXCHANGES_TYPE );
					urlBuf.append( "=" );
					urlBuf.append( type.name() );
				}
			}
			String url = urlBuf.toString();
			log.debug( "reading from " + url.toString() );
			list = (DataSetList) voDao.unmarshal( new URL( url ).openStream() );
		}
		catch ( MalformedURLException e1 ) {
			e1.printStackTrace();
		}
		catch ( JAXBException e ) {
			log.error( "error unmarshalling", e );
			return null;
		}
		catch ( IOException e ) {
			e.printStackTrace();
		}

		return new Result<IFlowListVO>( list );

	}

	/**
	 * Get consumers or producers or a flow
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @param startIndex
	 *            the start index of the result set (defaults to 0 if null)
	 * @param pageSize
	 *            the page size of the result set returned by the service
	 * @return loaded processes
	 */
	public Result<IProcessListVO> getConsumers( String uuid, Integer startIndex, Integer pageSize ) {
		return this.getConsumersOrProducers( uuid, ILCDNetworkClient.CONSUMERS, startIndex, pageSize );
	}

	/**
	 * Get consumers or producers or a flow
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @param startIndex
	 *            the start index of the result set (defaults to 0 if null)
	 * @param pageSize
	 *            the page size of the result set returned by the service
	 * @return loaded processes
	 */
	public Result<IProcessListVO> getProducers( String uuid, Integer startIndex, Integer pageSize ) {
		return this.getConsumersOrProducers( uuid, ILCDNetworkClient.PRODUCERS, startIndex, pageSize );
	}

	/**
	 * Get consumers or producers or a flow
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @param type
	 *            type of exchange {@link #CONSUMERS} or {@link #PRODUCERS}
	 * @param startIndex
	 *            the start index of the result set (defaults to 0 if null)
	 * @param pageSize
	 *            the page size of the result set returned by the service
	 * @return loaded processes
	 * @see #getConsumers(String, Integer, Integer)
	 * @see #getProducers(String, Integer, Integer)
	 */
	private Result<IProcessListVO> getConsumersOrProducers( String uuid, String type, Integer startIndex, Integer pageSize ) {
		String url = this.getConsumersOrProducersURL( uuid, type );

		log.debug( "reading from " + url );

		WebTarget webResource = this.client.target( url );

		if ( startIndex != null )
			webResource = webResource.queryParam( START_INDEX, startIndex.toString() );
		if ( pageSize != null )
			webResource = webResource.queryParam( PAGE_SIZE, pageSize.toString() );

		DataSetList list = null;
		try {
			list = (DataSetList) this.voDao.unmarshal( webResource.request().get( InputStream.class ) );
		}
		catch ( JAXBException e ) {
			log.error( "error unmarshalling", e );
			return null;
		}

		return new Result<IProcessListVO>( list );

	}

	/**
	 * Determine if one or more producer/s (process/es with provided flow was output exchange) are available
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @return <code>true</code> if producer/s are available, else <code>false</code>
	 */
	public boolean isProducerAvailable( String uuid ) {
		return this.getConsumerOrProducerAvailability( uuid, ILCDNetworkClient.PRODUCERS );
	}

	/**
	 * Determine if one or more consumer/s (process/es with provided flow was input exchange) are available
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @return <code>true</code> if consumer/s are available, else <code>false</code>
	 */
	public boolean isConsumerAvailable( String uuid ) {
		return this.getConsumerOrProducerAvailability( uuid, ILCDNetworkClient.CONSUMERS );
	}

	/**
	 * Get consumers or producers or a flow
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @param type
	 *            type of exchange {@link #CONSUMERS} or {@link #PRODUCERS}
	 * @return loaded processes
	 * @see #getConsumers(String, Integer, Integer)
	 * @see #getProducers(String, Integer, Integer)
	 */
	private boolean getConsumerOrProducerAvailability( String uuid, String type ) {
		String url = this.getConsumersOrProducersURL( uuid, type );

		log.debug( "reading from " + url );

		WebTarget webResource = this.client.target( url );

		Response response = webResource.request().head();
		if ( response.getStatus() == Response.Status.OK.getStatusCode() ) {
			return true;
		}
		else if ( response.getStatus() == Response.Status.NO_CONTENT.getStatusCode() ) {
			return false;
		}
		else {
			log.warn( "Unexpected status code: " + Integer.toString( response.getStatus() ) + ". Interpreting as if no consumer/producer available" );
			return false;
		}

	}

	/**
	 * Get the URL for consumers or producers
	 * 
	 * @param uuid
	 *            UUID of flow
	 * @param type
	 *            type of exchange {@link #CONSUMERS} or {@link #PRODUCERS}
	 * @return created URL
	 */
	private String getConsumersOrProducersURL( String uuid, String type ) {
		StringBuffer urlBuf = new StringBuffer( this.baseUrl );

		urlBuf.append( getURLSuffix( IFlowListVO.class ) );
		urlBuf.append( "/" );
		urlBuf.append( uuid );
		urlBuf.append( "/" );
		urlBuf.append( type );

		return urlBuf.toString();
	}
	
	/**
	 * Retrieve a list of all reference years.
	 * 
	 * @return the list of reference years
	 * 
	 */
	public IIntegerList getReferenceYears() throws IOException {
		
		WebTarget webResource = this.client.target( baseUrl + "processes/referenceyears" );

		if (log.isDebugEnabled())
			log.debug("GETting " + webResource.getUri().toString());
		
		IIntegerList result = webResource.request( MediaType.APPLICATION_XML ).get(IntegerList.class);

		if (log.isDebugEnabled())
			log.debug( "retrieved " + result.getIntegers().size() + " referenceyears");
		
		return result;
	}

	/**
	 * Retrieve a list of all valid until years.
	 * 
	 * @return the list of valid until years
	 * 
	 */
	public IIntegerList getValidUntilYears() throws IOException {
		
		WebTarget webResource = this.client.target( baseUrl + "processes/validuntilyears" );

		if (log.isDebugEnabled())
			log.debug("GETting " + webResource.getUri().toString());
		
		IIntegerList result = webResource.request( MediaType.APPLICATION_XML ).get(IntegerList.class);

		if (log.isDebugEnabled())
			log.debug( "retrieved " + result.getIntegers().size() + " validuntilyears");
		
		return result;
	}

	/**
	 * Retrieve a list of all locations.
	 * 
	 * @return the list of locations
	 * 
	 */
	public IStringList getLocations() throws IOException {
		
		WebTarget webResource = this.client.target( baseUrl + "processes/locations" );

		if (log.isDebugEnabled())
			log.debug("GETting " + webResource.getUri().toString());
		
		StringList result = webResource.request( MediaType.APPLICATION_XML ).get(StringList.class);

		if (log.isDebugEnabled())
			log.debug( "retrieved " + result.getStrings().size() + " locations");
		
		return result;
	}

	/**
	 * Retrieve a list of all languages.
	 *
	 * @return the list of languages
	 *
	 */
	public IStringList getLanguages() throws IOException {

		WebTarget webResource = this.client.target( baseUrl + "processes/languages" );

		if (log.isDebugEnabled())
			log.debug("GETting " + webResource.getUri().toString());

		StringList result = webResource.request( MediaType.APPLICATION_XML ).get(StringList.class);

		if (log.isDebugEnabled())
			log.debug( "retrieved " + result.getStrings().size() + " languages");

		return result;
	}

	/**
	 * Retrieve a list of all registration authorities.
	 *
	 * @return the list of registration authorities
	 *
	 */
	public Result<IContactListVO> getRegistrationAuthorities() throws IOException {

		String url = baseUrl + "processes/registrationAuthorities";

		if (log.isDebugEnabled())
			log.debug("GETting " + url);

		WebTarget webResource = this.client.target( baseUrl + "processes/registrationAuthorities" );

		if (log.isDebugEnabled())
			log.debug("GETting " + webResource.getUri().toString());

		Object r = webResource.request( MediaType.APPLICATION_XML ).get(DataSetList.class);

		JAXBElement<DataSetList> e = (JAXBElement<DataSetList>) r;

		DataSetList list = e.getValue();

		if (log.isDebugEnabled())
			log.debug( "retrieved " + list.getDataSet().size() + " registrationAuthorities");

		Result<IContactListVO> result = new Result<>(list);

		return result;
	}

	// /**
	// * Retrieve a list of data set value objects from the service.
	// *
	// * @param href
	// * the absolute URL
	// * @return the list of data set value objects
	// */
	// public IDataSetListVO get(String href) throws IOException {
	// URL url = new URL(href);
	// log.debug("reading from " + url.toString());
	// IDataSetListVO result = (IDataSetListVO)
	// voDao.unmarshal(url.openStream());
	// return result;
	// }

	/**
	 * Retrieve a list of data set value objects matching the specified criteria.
	 * 
	 * @param <T>
	 *            the type of data set elements held in this collection
	 * @param clazz
	 *            The class of the desired data set type
	 * @param queryParams
	 *            the query parameters as name-value pairs
	 * @return the query result
	 */
	public <T extends IDataSetListVO> Result<T> query( Class<T> clazz, MultivaluedMap<String, String> queryParams ) {
		return query( clazz, queryParams, false );
	}

	/**
	 * Retrieve a list of data set value objects matching the specified criteria.
	 * 
	 * @param <T>
	 *            the type of data set elements held in this collection
	 * @param clazz
	 *            The class of the desired data set type
	 * @param queryParams
	 *            the query parameters as name-value pairs
	 * @param startIndex
	 *            the start index of the result set (defaults to 0 if null)
	 * @param pageSize
	 *            the page size of the result set returned by the service
	 * @return the query result
	 */
	public <T extends IDataSetListVO> Result<T> query( Class<T> clazz, MultivaluedMap<String, String> queryParams, Integer startIndex, Integer pageSize ) {
		return query( clazz, queryParams, false, startIndex, pageSize );
	}

	/**
	 * Retrieve a list of data set value objects matching the specified criteria.
	 * 
	 * @param <T>
	 *            the type of data set elements held in this collection
	 * @param clazz
	 *            The class of the desired data set type
	 * @param queryParams
	 *            the query parameters as name-value pairs
	 * @param distributed
	 *            if set to true, a distributed query will be executed on all other nodes known to the service
	 * @return the query result
	 */
	public <T extends IDataSetListVO> Result<T> query( Class<T> clazz, MultivaluedMap<String, String> queryParams, boolean distributed ) {
		return query( clazz, queryParams, distributed, null, null );
	}

	/**
	 * Retrieve a list of data set value objects matching the specified criteria.
	 * 
	 * @param <T>
	 *            the type of data set elements held in this collection
	 * @param clazz
	 *            The class of the desired data set type
	 * @param queryParams
	 *            the query parameters as name-value pairs
	 * @param distributed
	 *            if set to true, a distributed query will be executed on all other nodes known to the service
	 * @param startIndex
	 *            the start index of the result set (defaults to 0 if null)
	 * @param pageSize
	 *            the page size of the result set returned by the service
	 * @return the query result
	 */
	@SuppressWarnings( "unchecked" )
	public <T extends IDataSetListVO> Result<T> query( Class<T> clazz, MultivaluedMap<String, String> queryParams, boolean distributed, Integer startIndex,
			Integer pageSize ) {

		String url = buildGETURL( clazz ).toString();

		log.debug( "reading from " + url );

		WebTarget webResource = client.target( url );

		webResource = webResource.queryParam( QUERY_COMMAND, "true" );

		if ( distributed )
			webResource = webResource.queryParam( DISTRIBUTED_QUERY, "true" );
		if ( startIndex != null )
			webResource = webResource.queryParam( START_INDEX, startIndex.toString() );
		if ( pageSize != null )
			webResource = webResource.queryParam( PAGE_SIZE, pageSize.toString() );

		log.debug( webResource.getUri().toString() );

		for (String key : queryParams.keySet()) {
			webResource = webResource.queryParam(key, queryParams.getFirst(key));
		}

		Response response = webResource.request().get();

		GenericType<JAXBElement<DataSetList>> genericType = new GenericType<JAXBElement<DataSetList>>() {};
		JAXBElement<DataSetList> e = response.readEntity(genericType);


		DataSetList list = e.getValue();
		this.nodeName = list.getSourceId();
		log.info( "returning " + list.getDataSet().size() + " results from node " + this.nodeName );

		return new Result<T>( list );
	}

	/**
	 * Retrieve a list of data set value objects matching the specified criteria as InputStream.
	 * 
	 * @param <T>
	 *            the type of data set elements held in this collection
	 * @param clazz
	 *            The class of the desired data set type
	 * @param queryParams
	 *            the query parameters as name-value pairs
	 * @return the InputStream
	 */
	public <T extends IDataSetListVO> InputStream queryAsStream( Class<T> clazz, MultivaluedMap<String, String> queryParams ) {
		return queryAsStream( clazz, queryParams, false );
	}

	/**
	 * Retrieve a list of data set value objects matching the specified criteria as InputStream.
	 * 
	 * @param <T>
	 *            the type of data set elements held in this collection
	 * @param clazz
	 *            The class of the desired data set type
	 * @param queryParams
	 *            the query parameters as name-value pairs
	 * @param startIndex
	 *            the start index of the result set (defaults to 0 if null)
	 * @param pageSize
	 *            the page size of the result set returned by the service
	 * @return the InputStream
	 */
	public <T extends IDataSetListVO> InputStream queryAsStream( Class<T> clazz, MultivaluedMap<String, String> queryParams, Integer startIndex,
			Integer pageSize ) {
		return queryAsStream( clazz, queryParams, false, startIndex, pageSize );
	}

	/**
	 * Retrieve a list of data set value objects matching the specified criteria as InputStream.
	 * 
	 * @param <T>
	 *            the type of data set elements held in this collection
	 * @param clazz
	 *            The class of the desired data set type
	 * @param queryParams
	 *            the query parameters as name-value pairs
	 * @param distributed
	 *            if set to true, a distributed query will be executed on all other nodes known to the service
	 * @return the InputStream
	 */
	public <T extends IDataSetListVO> InputStream queryAsStream( Class<T> clazz, MultivaluedMap<String, String> queryParams, boolean distributed ) {
		return queryAsStream( clazz, queryParams, distributed, null, null );
	}

	/**
	 * Retrieve a list of data set value objects matching the specified criteria as InputStream.
	 * 
	 * @param <T>
	 *            the type of data set elements held in this collection
	 * @param clazz
	 *            The class of the desired data set type
	 * @param queryParams
	 *            the query parameters as name-value pairs
	 * @param distributed
	 *            if set to true, a distributed query will be executed on all other nodes known to the service
	 * @param startIndex
	 *            the start index of the result set (defaults to 0 if null)
	 * @param pageSize
	 *            the page size of the result set returned by the service
	 * @return the InputStream
	 */
	public <T extends IDataSetListVO> InputStream queryAsStream( Class<T> clazz, MultivaluedMap<String, String> queryParams, boolean distributed,
			Integer startIndex, Integer pageSize ) {
		String url = buildGETURL( clazz ).toString();

		log.debug( "reading from " + url );

		WebTarget webResource = client.target( url );

		webResource = webResource.queryParam( QUERY_COMMAND, "true" );

		if ( distributed )
			webResource = webResource.queryParam( DISTRIBUTED_QUERY, "true" );
		if ( startIndex != null )
			webResource = webResource.queryParam( START_INDEX, startIndex.toString() );
		if ( pageSize != null )
			webResource = webResource.queryParam( PAGE_SIZE, pageSize.toString() );

		for (String key : queryParams.keySet()) {
			webResource = webResource.queryParam(key, queryParams.getFirst(key));
		}

		return webResource.request().get( InputStream.class);
	}
	
	private URL buildURL( String suffix ) {
		try {
			return new URL( this.baseUrl + suffix );
		}
		catch ( MalformedURLException e ) {
			e.printStackTrace();
		}
		return null;
	}

	private URL buildGETURL( Class<?> clazz ) {
		try {
			return new URL( this.baseUrl + getURLSuffix( clazz ) );
		}
		catch ( MalformedURLException e ) {
			e.printStackTrace();
		}
		return null;
	}

	private URL buildGETURL( Class<?> clazz, Integer startIndex, Integer pageSize ) {
		try {
			StringBuffer buf = new StringBuffer();
			boolean params = false;
			buf.append( this.baseUrl );
			buf.append( getURLSuffix( clazz ) );
			if ( startIndex != null || pageSize != null ) {
				buf.append( "?" );
				if ( startIndex != null ) {
					buf.append( START_INDEX + "=" + startIndex );
					params = true;
				}
				if ( pageSize != null ) {
					if ( params != true ) {
						buf.append( PARAMETER_CONCATENATOR );
					}
					buf.append( PAGE_SIZE + "=" + pageSize );
				}
			}
			return new URL( buf.toString() );
		}
		catch ( MalformedURLException e ) {
			e.printStackTrace();
		}
		return null;
	}

	private URL buildGETURL( Class<?> clazz, String id, String mode ) {
		return buildGETURL( clazz, id, null, mode );
	}

	private URL buildGETURL( Class<?> clazz, String id ) {
		return buildGETURL( clazz, id, null, null );
	}

	private URL buildGETURL( Class<?> clazz, String id, String version, String mode ) {
		try {
			StringBuffer buf = new StringBuffer(this.baseUrl);
			buf.append(getURLSuffix( clazz ));
			buf.append("/");
			buf.append(id);
			if (version != null || mode != null)
				buf.append(PARAMETER_SEPARATOR);
			if (version != null ) {
				buf.append(PARAMETER_CONCATENATOR);
				buf.append("version=");
				buf.append(version);
			}
			if (mode != null) {
				buf.append(PARAMETER_CONCATENATOR);
				buf.append(mode);
			}
			return new URL( buf.toString() );
		}
		catch ( MalformedURLException e ) {
			log.warn( e );
		}
		return null;
	}

	protected String getURLSuffix( Class<?> clazz ) {
		if ( clazz.equals( ProcessDataSet.class ) )
			return DatasetTypes.PROCESSES.getValue();
		else if ( clazz.equals( FlowDataSet.class ) )
			return DatasetTypes.FLOWS.getValue();
		else if ( clazz.equals( FlowPropertyDataSet.class ) )
			return DatasetTypes.FLOWPROPERTIES.getValue();
		else if ( clazz.equals( UnitGroupDataSet.class ) )
			return DatasetTypes.UNITGROUPS.getValue();
		else if ( clazz.equals( LCIAMethodDataSet.class ) )
			return DatasetTypes.LCIAMETHODS.getValue();
		else if ( clazz.equals( SourceDataSet.class ) )
			return DatasetTypes.SOURCES.getValue();
		else if ( clazz.equals( ContactDataSet.class ) )
			return DatasetTypes.CONTACTS.getValue();
		else
			try {
				return (String) clazz.getField( "URL_SUFFIX" ).get( new String() );
			}
			catch ( Exception e ) {
				log.warn( e );
				return null;
			}
	}

	/***
	 * if argument does not contain a trailing slash, add it
	 * 
	 * @param url
	 * @return
	 */
	private String fixURL( String url ) {
		return (url.endsWith( "/" ) ? url : url + "/");
	}

	private Client buildClient() {
		client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();

		if ( log.isTraceEnabled() )
			client.register(new LoggingFeature());
		return client;
	}

	private Client buildClient( String baseUrl, String username, String password ) throws ILCDServiceClientException {

		log.debug( "building HTTP client for " + baseUrl + (username == null ? "" : ", authenticating as user " + username) );

		final String METHOD = "GET";
		// final String METHOD = "POST";

		// use the Apache implementation, enable cookies
		ClientConfig config = new ClientConfig();
		config.property(ApacheClientProperties.DISABLE_COOKIES, false);

		ClientBuilder clientBuilder = ClientBuilder.newBuilder().withConfig(config);

		clientBuilder.register(MultiPartFeature.class);

		// for debugging
		if ( log.isTraceEnabled() )
			clientBuilder.register( new LoggingFeature() );

		Client client = clientBuilder.build();

		String loginEndpoint = baseUrl + "authenticate/login";
		WebTarget webResource = null;
		Response response = null;

		if ( METHOD.equals( "GET" ) ) {
			response = client.target(loginEndpoint)
					.queryParam("userName", username)
					.queryParam("password", password)
					.request()
					.get(Response.class);
		}
		else if ( METHOD.equals( "POST" ) ) {
			Form formData = new Form();
			formData.param("userName", username);
			formData.param("password", password);

			response = client.target(loginEndpoint)
					.request()
					.post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
		} else {
		}

		log.info( response.getStatus() + " " + response.getEntity().toString() );

		if ( response.getStatus() != 200 )
			throw new FailedAuthenticationException( "Authentication at " + baseUrl + " failed with HTTP status code " + response.getStatus() );

		return client;
	}
	
	/**
	 * Store a data set on the service.
	 * 
	 * @param dataset
	 *            the dataset
	 * @return the service's response, wrapped into an ILCDClientResponse object
	 * 
	 */
	public <T extends DataSet> ILCDClientResponse putDataSet( T dataset ) {
		return putDataSet( dataset, null );
	}

	/**
	 * Store a data set on the service in the specified root data stock.
	 * 
	 * @param dataset
	 *            the dataset
	 * @param dataStock
	 *            the root datastock UUID
	 * @return the service's response, wrapped into an ILCDClientResponse object
	 * 
	 */
	public <T extends DataSet> ILCDClientResponse putDataSet( T dataset, String dataStock ) {

		String urlSuffix = getURLSuffix( dataset.getClass() );

		log.debug( "POSTing to " + baseUrl + urlSuffix );

		WebTarget webResource = this.client.target( baseUrl + urlSuffix );

		FormDataMultiPart multiPart = new FormDataMultiPart();

		JAXBElement<DataSet> e = this.dao.getDatasetHelper().createJAXBElement( dataset );

		FormDataBodyPart bodyPart = new FormDataBodyPart( "file", e, MediaType.APPLICATION_XML_TYPE );
		multiPart.bodyPart( bodyPart );
		if ( dataStock != null && !dataStock.isEmpty() )
			multiPart.field( "stock", dataStock );

		Response clientResponse = webResource.request().post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE), Response.class );

		MediaType responseType = clientResponse.getMediaType();

		ILCDClientResponse response = null;

		log.info("status is {}", clientResponse.getStatus());

		if ( responseType.toString().equals( MediaType.APPLICATION_XML ) ) {
			DataStockVO stock =  clientResponse.readEntity(DataStockVO.class);
			log.info( stock.getUuid() );
		} else {
			response = new ILCDClientResponse( clientResponse );
			log.info( response.getClientResponse().getStatus() + " " + response.getBody() );
		}

		return response;
	}

	/**
	 * Store a data set wrapped in an InputStream on the service.
	 * 
	 * @param inputStream
	 *            the InputStream
	 * @return the service's response, wrapped into an ILCDClientResponse object
	 * 
	 */
	public <T extends DataSet> ILCDClientResponse putDataSetAsStream( Class<T> clazz, InputStream inputStream ) {
		return putDataSetAsStream( clazz, inputStream, null );
	}

	/**
	 * Store a data set wrapped in an InputStream on the service.
	 * 
	 * @param inputStream
	 *            the InputStream
	 * @return the service's response, wrapped into an ILCDClientResponse object
	 * 
	 */
	public <T extends DataSet> ILCDClientResponse putDataSetAsStream( Class<T> clazz, InputStream inputStream, String dataStock ) {

		String urlSuffix = getURLSuffix( clazz );

		log.debug( "POSTing to " + baseUrl + urlSuffix );

		WebTarget webResource = this.client.target( baseUrl + urlSuffix );

		Response clientResponse = webResource.request().header( "stock", dataStock ).post( Entity.entity(inputStream, MediaType.APPLICATION_XML), Response.class );

		ILCDClientResponse response = new ILCDClientResponse( clientResponse );

		log.info( response.getClientResponse().getStatus() + " " + response.getBody() );

		return response;

	}

	/**
	 * Store a source data set with associated binary file on the service in the specified root data stock.
	 * 
	 * @param dataset
	 *            the dataset
	 * @param dataStock
	 *            the root datastock UUID
	 * @param digitalFiles
	 *            a List of FileParam objects, wrapping the binary files' file names and InputStreams 
	 * @return the service's response, wrapped into an ILCDClientResponse object
	 * 
	 */
	public ILCDClientResponse putSourceDataSet( SourceDataSet dataset, String dataStock, List<FileParam> digitalFiles) {

		String urlSuffix = getURLSuffix( dataset.getClass() ) + "/" + ILCDNetworkClient.SOURCE_WITH_BINARIES;

		log.debug( "POSTing to " + baseUrl + urlSuffix );

		WebTarget webResource = this.client.target( baseUrl + urlSuffix );

		FormDataMultiPart multiPart = new FormDataMultiPart();

		JAXBElement<DataSet> e = this.dao.getDatasetHelper().createJAXBElement( dataset );

		FormDataBodyPart bodyPart = new FormDataBodyPart( "file", e, MediaType.APPLICATION_XML_TYPE );
		multiPart.bodyPart( bodyPart );

		for (FileParam file : digitalFiles) {
    		FormDataBodyPart extDocBodyPart = new FormDataBodyPart( file.getFileName(), file.getInputStream(), MediaType.APPLICATION_OCTET_STREAM_TYPE );
    		multiPart.bodyPart( extDocBodyPart );
		}
		
		if ( dataStock != null && !dataStock.isEmpty() )
			multiPart.field( "stock", dataStock );

		Response clientResponse = webResource.request( MediaType.MULTIPART_FORM_DATA_TYPE ).post( Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE), Response.class );

		MediaType responseType = clientResponse.getMediaType();

		if ( responseType.toString().equals( MediaType.APPLICATION_XML ) ) {
			DataStockVO stock = clientResponse.readEntity( DataStockVO.class );
			log.info( stock.getUuid() );
		}
		else {
			ILCDClientResponse response = new ILCDClientResponse( clientResponse );
			log.info( response.getClientResponse().getStatus() + " " + response.getBody() );
		}

		return null;

	}

	
	/**
	 * Store a source data set with associated binary file wrapped in an InputStream on the service.
	 * 
	 * @param inputStream
	 *            the source dataset InputStream
	 * @param dataStock
	 *            the root datastock UUID
	 * @param digitalFiles
	 *            a List of FileParam objects, wrapping the binary files' file names and InputStreams 
	 *            
	 * @return the service's response, wrapped into an ILCDClientResponse object
	 * 
	 */
	public ILCDClientResponse putSourceDataSetAsStream( InputStream inputStream, String dataStock,  List<FileParam> digitalFiles  ) {

		String urlSuffix = getURLSuffix( SourceDataSet.class ) +  "/" + ILCDNetworkClient.SOURCE_WITH_BINARIES;

		log.debug( "POSTing to " + baseUrl + urlSuffix );

		WebTarget webResource = this.client.target( baseUrl + urlSuffix );

		FormDataMultiPart multiPart = new FormDataMultiPart();

		FormDataBodyPart bodyPart = new FormDataBodyPart( "file", inputStream, MediaType.APPLICATION_XML_TYPE );
		multiPart.bodyPart( bodyPart );

		for (FileParam file : digitalFiles) {
    		FormDataBodyPart extDocBodyPart = new FormDataBodyPart( file.getFileName(), file.getInputStream(), MediaType.APPLICATION_XML_TYPE );
    		multiPart.bodyPart( extDocBodyPart );
		}

		Response clientResponse = webResource.request().header( "stock", dataStock ).post( Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA_TYPE), Response.class );

		ILCDClientResponse response = new ILCDClientResponse( clientResponse );

		log.info( response.getClientResponse().getStatus() + " " + response.getBody() );

		return response;

	}

	public INodeInfo getNodeInfo() {

		String url = this.baseUrl + NODE_INFO;

		log.debug( "reading from " + url );

		WebTarget webResource = client.target( url );

		NodeInfo i = webResource.request().get( NodeInfo.class );

		return i;
	}

	public IAuthenticationInfo getAuthenticationStatus() {

		String url = this.baseUrl + AUTHENTICATION_STATUS;

		log.debug( "reading from " + url );

		WebTarget webResource = client.target( url );

		AuthenticationInfo i = webResource.request().get( AuthenticationInfo.class );

		return i;
	}

	/**
	 * @return the nodeName
	 */
	public String getNodeName() {
		return nodeName;
	}

	/**
	 * @return the dao
	 */
	protected DatasetDAO getDao() {
		return dao;
	}

	/**
	 * @param dao
	 *            the dao to set
	 */
	protected void setDao( DatasetDAO dao ) {
		this.dao = dao;
	}

	/**
	 * @return the voDao
	 */
	protected DatasetVODAO getVoDao() {
		return voDao;
	}

	/**
	 * @param voDao
	 *            the voDao to set
	 */
	protected void setVoDao( DatasetVODAO voDao ) {
		this.voDao = voDao;
	}

	
	public String getBaseUrl() {
		return baseUrl;
	}

}
